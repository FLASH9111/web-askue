<?php 
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: index.php
=====================================================
 Входная точка
*****************************************************
*/
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);
error_reporting(E_ALL);

header('Content-Type: text/html; charset=utf-8');

define ('READFILE', true);
define('ROOT',$_SERVER["DOCUMENT_ROOT"]); 

$url=''; 
session_start();

include_once (ROOT."/engine/core/mainload.php");
include_once (ROOT."/engine/core/init.php");
?>