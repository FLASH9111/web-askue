<?php 
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: engine/core/view_index.php
=====================================================
 Структурирование главной страницы
*****************************************************
*/
if (! defined ('READFILE'))
{
    exit ("Error! Hacking attempt!");
}		
$this->config = new config; 	
$this->tpl->load_template_str($script);
$this->tpl->compile('script');
if (!isset($this->tpl->result['meta']))  {
	$title = $this->config->getParam('site_name');
	$description = $this->config->getParam('description');
	$keywords = $this->config->getParam('meta');
	$meta = '<title>'.$title.'</title>
	<meta name="description" content="'.$description.'" />
    <meta name="keywords" content="'.$keywords.'" />
    ';
	$this->tpl->load_template_str($meta);
	$this->tpl->compile('meta');
}
$this->tpl->set('{{content}}', $this->tpl->result['content']);
$this->tpl->set('{{sitename}}', $this->config->getParam('site_name'));
$this->tpl->set('{{sitedescription}}', $this->config->getParam('description'));
$this->tpl->set('{{script}}', $this->tpl->result['script']);
$this->tpl->set('{{meta}}', $this->tpl->result['meta']);
$db = new medoo;
$this->tpl->load_template('main.tpl'); //загружаем каркасный файл
$this->tpl->compile('main'); //собираем шаблон
echo $this->tpl->result['main'];
$this->tpl->global_clear();
?>