<?php 
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: engine/core/view_admin.php
=====================================================
 Структурирование главной страницы админ-панели
*****************************************************
*/
if (! defined ('READFILE'))
{
    exit ("Error! Hacking attempt!");
}
if (!isset($this->tpl->result['meta']))  {
   	$meta = '<title>Panel ASKUE</title>';
  	$this->tpl->load_template_str($meta);
   	$this->tpl->compile('meta');
}
$this->tpl->set('{{content}}', $this->tpl->result['content']);
$this->tpl->set('{{meta}}', $this->tpl->result['meta']);
$this->tpl->load_template('main.tpl'); 
$this->tpl->compile('main'); 
echo $this->tpl->result['main'];
$this->tpl->global_clear();
?>