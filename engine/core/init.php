<?php 
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: engine/core/init.php
=====================================================
 Загрузка модулей и плагинов к ним
*****************************************************
*/
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!" );
}

/*Инициализация входной точки*/
if(isset($_GET['url']) and $_GET['url'] != '')
	{
    	if(!preg_match("/^[a-za-яA-ZА-Я0-9%=._\/-]+$/",$_GET['url']))
    	{
      	not_page();
    	}	
		$url = clean_var($_GET['url']);
	} elseif(isset($_GET['blog']))
	{
    	if(!preg_match("/^[a-za-яA-ZА-Я0-9%=._\/-]+$/",$_GET['blog']))
    	{
      	not_page();
    	}	
		$blog = clean_var($_GET['blog']);
		$db = new medoo;
		$table_post = 'post';
		$post = $db->get($table_post, ["alt_name", 'id'], ["AND" => [
				"id" => $blog,
				"approve" => 1,
			]]);
if($post != '') { 
	$url = $post['alt_name'];
}
	} 
	else {
		$url = '';
	}
$auth = false;
$url = rtrim($url, '/');
$url = explode('/', $url);
if(isset($_SESSION['user']['adminlog']) && ($_SESSION['user']['adminlog'] == 'admin' || $_SESSION['user']['adminlog'] == 'moderator'))
	$auth = true;
if ((!isset($_SESSION['user']['adminlog']) or $auth != true) and stristr($_SERVER['REQUEST_URI'], 'panel') !=false) { 
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && $url[0] != 'login' && $url[0] != 'forgot' && $url[0] != 'recovery') {
   		echo 'Ваша сессия устарела!'; 
   		exit;
   	}
	$object = new login($url);
}
else $unit = new unit('start', $url);

?>