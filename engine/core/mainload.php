<?php /*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: engine/core/mainload.php
=====================================================
 Загрузка основных элементов системы
*****************************************************
*/
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!" );
}

/*Подключение файла настроек*/
//include_once (ROOT."/config.php");

/*Подключение основных функций*/
$fileName = ROOT."/engine/lib/func_general.php";
if(file_exists($fileName)) {
     include $fileName;
}

/*Автоматическая загрузка основных классов*/
function autoload ($className) {
	$fileName = ROOT."/engine/classes/class_".$className . '.php';
  		if(file_exists($fileName)) {
     		include_once $fileName;
		} 
		else $fileName = ROOT."/modules/".$className."/class_".$className . '.php';
  		if(file_exists($fileName)) {
     		include_once $fileName; 
     	} 
     	else {
		  	if (isset($url))     
     			$controller = new error($url);//display ('Нет класса '.$className);
   		}
}
spl_autoload_register('autoload');

$fileName = ROOT."/config.php";
if(file_exists($fileName)) {
     //require_once $fileName;
}

$config = new configuration;

define('LANG',$config->lang); 

unset($config);

?>