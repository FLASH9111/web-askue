<?php
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: func_general.php
=====================================================
 Основные функции
*****************************************************
*/
/*Вывод сообщения об ошибке*/
function display($error) {
	echo $error;
	$tpl->set('{content}', $error);
	//exit;
}
function not_page() {
	header('HTTP/1.1 404 Not Found');
	header('Status: 404 Not Found');	
	include (ROOT."/templates/404.tpl"); 
	exit;		
}
function stripslashes_array($array) {
  return is_array($array) ?
    array_map('stripslashes_array', $array) : stripslashes($array);
}
/*Очистка переменной от вредоносных частей*/
function clean_var($cleanvar) {
	$cleanvar = trim(htmlspecialchars(strip_tags($cleanvar))); 
	return $cleanvar;
}
function array_diff_dim($array1, $array2) {
	$array = array();
	if (!empty($array1) and !empty($array2)) {
		foreach ($array1 as $value1) {
			foreach ($array2 as $value2) {
				if ($value1 == $value2) $flag = true;	
			}
			if ($flag != true) $array [] = $value1;	
			$flag = false;	
		}
		return $array;
	}
	else return false;
}
function dimarr($array) {
	switch(count($array)) {
		case 0:
			return false;
			break;
		case 1:
			return @ $array[0];
			break;
		default:
			return $array;
			break;
	}
}
/*Подготовка переменной типа URL*/
function clean_var_url($value) {
	$value = translit($value);
	$value = strtolower($value);
	$value = iconv("windows-1251", "utf-8", $value);
	$value = str_replace(".",'',$value);
	$value = trim($value, '-');
	$value = str_replace("_",'-',$value);
	$value = str_replace("---",'-',$value);
	$value = trim(str_replace("--",'-',$value));
	if ($value=='-') $value='';
	return $value;
}
/*Транслитерация для URL*/
function translit($str) {

  $tr = array(
        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Д"=>"d",
        "Е"=>"e","Ё"=>"yo","Ж"=>"j","З"=>"z","И"=>"i",
        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"c","Ч"=>"ch",
        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=> "_",  "/"=> "_"
  );
  $str = strtr($str,$tr);
  $str = preg_replace('/[^A-Za-z0-9._\-]/', '', $str);
  return $str;
}
/*Сортировка массива по параметру priority*/
function cmp($a, $b) {
	if (((int) $a["priority"]) < ((int) ($b["priority"]))) return -1;
	if (((int) $a["priority"]) > ((int) ($b["priority"]))) return 1;
	if (((int) $a["priority"]) == ((int) ($b["priority"]))) return 0;
}
function str_size($str,$length){ 
	$str = iconv("UTF-8","windows-1251", $str); 
	$str = substr($str, 0, $length); 
	$str = iconv("windows-1251", "UTF-8", $str); 
	$str .= "..."; return $str;
}
function str_size_word($str, $size){ 
  return mb_substr($str,0,mb_strrpos(mb_substr($str,0,$size,'utf-8'),' ',"utf-8"),'utf-8');
} 
function hash_formula($pas){ 
  return md5($pas).md5("SVCE");
}
function check_http_status($url) {
  		$user_agent = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)';
  		$ch = curl_init();
  		curl_setopt($ch, CURLOPT_URL, $url);
  		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  		curl_setopt($ch, CURLOPT_VERBOSE, false);
  		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
  		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  		curl_setopt($ch, CURLOPT_SSLVERSION, 3);
  		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
  		$page = curl_exec($ch);
  		$err = curl_error($ch);
  		if (!empty($err))
    		return $err;
  		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  		curl_close($ch);
  		return $httpcode;
} 

?>