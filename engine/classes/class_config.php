<?php /*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: engine/classes/class_config.php
=====================================================
 Возвращение значений настроек системы
*****************************************************
*/
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!" );
}
class config extends controller {
	
	protected $config = array ();

	function __construct() {
		parent:: __construct();
		$this->getPages();
		$this->getConfDB();
	}
	
	public function getPages() {
		if (stristr($_SERVER['REQUEST_URI'], 'panel') == false) { 
			$this->config['location']='index';
		} else {
			$this->config['location']='admin';
		}
	}
	
	public function getConfDB() {
		$table = 'config';
		$rowList = $this->db->get(
    		$table, 
    		array('site_name', 'description', 'meta', 'admin_theme', 'index_theme', 'mainmail', 'maxitem', 'server_root', 'active', 'description_site_off', 'short_url')
		);
		$this->config = $this->config + $rowList;
	}

	/*Получение параметра системы*/
	
	public function getParam($name) {
		return (isset($this->config[$name])) ? $this->config[$name] : NULL;
	}
	public function getParams() {
		return (isset($this->config)) ? $this->config : NULL;
	}
}
?>
