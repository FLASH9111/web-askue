<?php
/*
*****************************************************
 CMS SVCE 2.0 
=====================================================
 http://svce.ru/
=====================================================
 Gorshkov O.V.
=====================================================
 Copyright (c) 2013-2016 Zed Group
=====================================================
 Файл: engine/classes/class_base_bc.php
=====================================================
 Основые функции для многих компонентов
*****************************************************
*/
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!" );
}
class base_bc extends controller {
  	protected $config;
  	public $script;
   	public function __construct($args) {
   		parent::__construct($args);
   		
   		$this->config = new config; 	
   	}
	public function outfields($fields_id, $fields_pr, $compile) {
   		$table_afields = 'afields';
   		if (!isset($compile) or $compile == '') $compile = 'content';
   		if ($fields_id != '') {
			$post_afields = $this->db->get($table_afields, [
				"template",
				"date",
				"name",
				"text",
				'id'
			], [
				"id" => $fields_id
			]);
			if (isset($post_afields['template']) and $post_afields['template'] != '') {
				$this->tpl->load_template($post_afields['template'].'.tpl');	
				$this->tpl->set('[date]', $post_afields['date']);  
  		  		$this->tpl->set('[name]', $post_afields['name']); 			 
  		  		$this->tpl->set('[text]', $post_afields['text']); 				 
			}
			elseif (isset($post_afields['text'])) {
				$this->tpl->load_template_str($post_afields['text']);
			}	
			$this->tpl->compile($compile);	
			$this->tpl->set ("{{".$compile."}}", $this->tpl->result["$compile"]);
		}
	}
	public function smpr($smprxml = '0.6') {
		if ($smprxml == '0.0') $select = 'selected'; else $select=''; $smpr='<option '.$select.' value="0.0">0%</option>';
	 	if ($smprxml == '0.1') $select = 'selected'; else $select=''; $smpr .= '<option '.$select.' value="0.1">10%</option>';
	 	if ($smprxml == '0.2') $select = 'selected'; else $select=''; $smpr .= '<option '.$select.' value="0.2">20%</option>';
	 	if ($smprxml == '0.3') $select = 'selected'; else $select=''; $smpr .= '<option '.$select.' value="0.3">30%</option>';
	 	if ($smprxml == '0.4') $select = 'selected'; else $select=''; $smpr .= '<option '.$select.' value="0.4">40%</option>';
	 	if ($smprxml == '0.5') $select = 'selected'; else $select=''; $smpr .= '<option '.$select.' value="0.5">50%</option>';
	 	if ($smprxml == '0.6') $select = 'selected'; else $select=''; $smpr .= '<option '.$select.' value="0.6">60%</option>';
	 	if ($smprxml == '0.7') $select = 'selected'; else $select=''; $smpr .= '<option '.$select.' value="0.7">70%</option>';
	 	if ($smprxml == '0.8') $select = 'selected'; else $select=''; $smpr .= '<option '.$select.' value="0.8">80%</option>';
	 	if ($smprxml == '0.9') $select = 'selected'; else $select=''; $smpr .= '<option '.$select.' value="0.9">90%</option>';
	 	if ($smprxml == '1.0') $select = 'selected'; else $select=''; $smpr .= '<option '.$select.' value="1.0">100%</option>';
	 	return $smpr;
	}
	public function smtime($smupxml = 'weekly') {
	    if ($smupxml == 'always') $select = 'selected'; else $select=''; $smup='<option '.$select.' value="always">'.$this->getLang('Always').'</option>';
		if ($smupxml == 'hourly') $select = 'selected'; else $select=''; $smup  .= '<option '.$select.' value="hourly">'.$this->getLang('Hourly').'</option>';
	 	if ($smupxml == 'daily') $select = 'selected'; else $select=''; $smup  .= '<option '.$select.' value="daily">'.$this->getLang('Daily').'</option>';
	 	if ($smupxml == 'weekly') $select = 'selected'; else $select=''; $smup  .= '<option '.$select.' value="weekly">'.$this->getLang('Weekly').'</option>';
	 	if ($smupxml == 'monthly') $select = 'selected'; else $select=''; $smup  .= '<option '.$select.' value="monthly">'.$this->getLang('Monthly').'</option>';
	 	if ($smupxml == 'yearly') $select = 'selected'; else $select=''; $smup  .= '<option '.$select.' value="yearly">'.$this->getLang('Yearly').'</option>';
	 	if ($smupxml == 'never') $select = 'selected'; else $select=''; $smup  .= '<option '.$select.' value="never">'.$this->getLang('Never').'</option>';
	 	return $smup;
	}
	public function sm($smxml = 1) {
	  	if ($smxml == 1) $select = 'selected'; else $select=''; $sm='<option '.$select.' value="1">'.$this->getLang('Yes').'</option>';
    	if ($smxml == 0) $select = 'selected'; else $select=''; $sm .= '<option '.$select.' value="0">'.$this->getLang('No').'</option>';
	 	return $sm;
	}
	public function addfields($module, $fields, $id = null) {
		$delete = array();
		$insert = array();
		$value = null;
		switch($module) {
   			case 'category': 
   				$table = 'fieldcategory';
   				$column = 'category_id';
   				break;
   			case 'post': 
   				$table = 'fieldpost';
   				$column = 'post_id';
   				break;
   			case 'custom': 
   				$table = 'fieldcustom';
   				$column = 'custom_id';
   				break;
   			case 'static': 
   				$table = 'fieldstatic';
   				$column = 'static_id';
   				break;
   			default:
   				return false;
   		}
		if ($id == null) {   			
			if ($fields = dimarr($fields))
   			$res = $this->db->insert($table, $fields);
   		}
   		else {
   			$fieldsnow = $this->db->select($table, ['field_pr', 'field_id', $column], [$column => $id]);
   			if (dimarr($fields)) 
   				$delete = array_diff_dim($fieldsnow, $fields);
   			else $delete = $fieldsnow;
   			if (!empty($delete))
   			foreach ($delete as $value) {
				$res = $this->db->delete($table,  ['AND'=>['field_id' => $value['field_id'], $column => $id]]);		
			}
   			if (dimarr($fieldsnow)) 
   				$insert = array_diff_dim($fields, $fieldsnow); 
   			else $insert = $fields;
   			if ($insert = dimarr($insert)) {
   			   	$res = $this->db->insert($table, $insert);
   			   }
   		}
   		return;
	}
	public function addcats($module, $cats, $id = null) {
		$delete = array();
		$insert = array();
		$ins = array();
		$catsnow = array();
		$value = null;
		switch($module) {
   			case 'post': 
   				$table_id = 'post';
   				$table = 'catpost';
   				$column = 'post_id';
   				break;
   			case 'custom': 
   				$table_id = 'custom_page';
   				$table = 'catcustom';
   				$column = 'custom_id';
   				break;
   			default:
   				return false;
   		}

		if ($id == null) {   			
			if (!empty($cats))
   			   	foreach ($cats as $value) {
					$ins [] = ['category_id' => $value, $column => $this->db->max($table_id, "id")];	   			
   				}
   			if ($ins = dimarr($ins))
   			$res = $this->db->insert($table, $ins);
   		}
   		else {
   			$res = $this->db->select($table, ['category_id'], [$column => $id]);
   			if ($res != "")
   			foreach ($res as $value) {
				$catsnow [] = $value['category_id'];	   			
   			}
   			if (dimarr($cats)) 
   				$delete = array_diff($catsnow, $cats);
   			else $delete = $catsnow;
   			if (!empty($delete))
   			foreach ($delete as $value) {
				$res = $this->db->delete($table, ['AND'=>['category_id' => $value, $column => $id]]);
			}
   			if (dimarr($catsnow)) {
   				$insert = array_diff($cats, $catsnow);
   			} 
   			else $insert = $cats;
   			if (!empty($insert))
   			   	foreach ($insert as $value) {
					$ins [] = ['category_id' => $value, $column => $id];	   			
   				}
   			if ($ins = dimarr($ins)) 
   			   	$res = $this->db->insert($table, $ins);
   		}
   		return;
	}
}
?>