<?php
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: engine/classes/class_navigation.php
=====================================================
 Навигация на сайте
*****************************************************
*/ 
class navigation  {
	public function __construct() {
   	}
   	public function navarr($array, $num) {
    	return array_chunk($array, $num, true);
   	}
   	public function countpages($array, $num) {
   	if (is_numeric($array)) 
			return ceil(($array) / $num);
		else 
			return count(array_chunk($array, $num, true)); 		
	}
	public function generatetpl($array, $num, $page, $location, $modulename) {
		$k = null;
		$nav = '';
		$nextlink = '';
		$prevlink = '';
		if (is_numeric($array)) 
			$links = ceil((($array) / $num));
		else 
			$links = count(array_chunk($array, $num, true)); 
		if ($location == 'admin') 
			$url = '/panel/'.$modulename;	
		else 
			$url = '/'.$modulename;
		$url = str_replace('/_start', '', $url);
		if ($links > 1) {//Вывод ссылок на страницы
	  	if ($page != $links)
			$nextlink = "<li><a href='".$url."/".($page+1)."/'>></a></li>";
		if ($page != 1)
			$prevlink = "<li><a href='".$url."/".($page-1)."/'><</a></li>";
		if ($links < 11) {
			for ($i = 1; $i <= $links; $i++)
				if ($i != $page)
					$nav .= "<li><a href='".$url."/".$i."/'>".$i."</a></li>";
				else
					$nav .= '<li class="active"><span>'.$i.'</span></li>';
			$k = $k+1;
		}
    	else {
			if ($page > 6 and $page < ($links-4)) {
				$nav = "<li><a href='".$url."/".(1)."/'>".(1)."</a></li>";
				$nav .= '<li><a>...</a></li>';
				$nav .= "<li><a href='".$url."/".($page-4)."/'>".($page-4)."</a></li>"."<li><a href='".$url."/".($page-3)."/'>".($page-3)."</a></li>"."<li><a href='".$url."/".($page-2)."/'>".($page-2)."</a></li>"."<li><a href='".$url."/".($page-1)."/'>".($page-1)."</a></li>";
				$nav .=	'<li class="active"><span>'.$page.'</span></li>';
				$nav .= "<li><a href='".$url."/".($page+1)."/'>".($page+1)."</a></li>"."<li><a href='".$url."/".($page+2)."/'>".($page+2)."</a></li>"."<li><a href='".$url."/".($page+3)."/'>".($page+3)."</a></li>"."<li><a href='".$url."/".($page+4)."/'>".($page+4)."</a></li>";
				$nav .='<li><a>...</a></li>';
				$nav .= "<li><a href='".$url."/".$links."/'>".$links."</a></li>";
			}
			if ($page <= 6){
				for ($i = 1; $i <= $links; $i++)
					if ($k < 7) {
						if ($i != $page)
							$nav .= "<li><a href='".$url."/".$i."/'>".$i."</a></li>";
						else
							$nav .= '<li class="active"><span>'.$i.'</span></li>';
						$k = $k+1;
					}
  				$nav .='<li><a>...</a></li>';
				$nav .= "<li><a href='".$url."/".$links."/'>".$links."</a></li>";
			}
			if ($page >= ($links-4)){
				for ($i = 1; $i <= $links; $i++)
					if ($k<3) {
						if ($i != $page)
							$nav .= "<li><a href='".$url."/".$i."/'>".$i."</a></li>";
						else
							$nav .= '<li class="active"><span>'.$i.'</span></li>';
        				$k = $k+1;
					}
				$k = 0;
				$nav .= '<li><a>...</a></li>';
				$nav .= "<li><a href='".$url."/".($page-2)."/'>".($page-2)."</a></li>";
				$nav .= "<li><a href='".$url."/".($page-1)."/'>".($page-1)."</a></li>";
				for ($i = $page; $i <= $links; $i++)
					if ($k < 5) {
						if ($i != $page)
							$nav .= "<li><a href='".$url."/".$i."/'>".$i."</a></li>";
						else
							$nav .= '<li class="active"><span>'.$i.'</span></li>';
						$k = $k+1;
					}
			}
		}
	}
	//echo $prevlink.$nav.$nextlink;
	return $prevlink.$nav.$nextlink;
	}
}