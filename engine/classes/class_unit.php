<?php /*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: engine/classes/class_unit.php
=====================================================
 Подключение модулей и их параметров
*****************************************************
*/
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!" );
}

class unit extends controller{
	
	/*Параметры запуска методов
	$args[0] - start/getParam
	$args[1] - имя контроллера*/
	
	private $sel;	
	
	/*Имя контроллера*/
	public $className = '';
	
	public  $tpl; 
	
	/*Имя метода*/
	public $nameMethod = 'default';
	
	/*Параметры контроллера*/
	public $paramList = array();
	
	/*Параметры плагина*/
	public $paramListPlug = array();
	
	/*URL*/
	protected $url = array();
	
	/*Флаг включение хотя бы одного контроллера*/
	public $classFlag = FALSE; 
	
	protected $config;
	
	function __construct() {
	   $this->config = new config;
		$this->sel = func_get_args();
		if (isset($this->sel[1][0])) $this->nameClass = $this->sel[1][0];
		if (isset ($this->sel[1])) $this->url = $this->sel[1];	
		if (($this->config->getParam('location') === 'index') and ($this->config->getParam('active') === 'disable'))
			display ($this->config->getParam('description_site_off'));
		if (isset($this->sel[1][1])) {
			$this->nameMethod = $this->sel[1][1];
		}
		if (isset($this->sel[0])) switch ($this->sel[0]) {
			case 'start':
			if (empty($this->sel[1]) or !isset($this->sel[1])) break;//return false;
    		$this->start();
    		break;
		}
	}	
	
	public function starttpl() {
		$this->tpl = new template;
		if ($this->config->getParam('location') == 'admin') {
		   // $this->tpl->dir = ROOT.'/admin/templates/'.$this->config->getParam('admin_theme').'/';
		   	$this->tpl->dir = ROOT.'/vue/public/';
		}
		else $this->tpl->dir = ROOT.'/templates/'.$this->config->getParam('index_theme').'/';
	}
	
	
	public function view() {
		if ($this->config->getParam('location') == 'admin') 
			include_once (ROOT."/engine/core/view_vue.php");
		else include_once (ROOT."/engine/core/view_index.php");
	}
	public function custom_link() {
		$this->db = new medoo;
		$alt_name = $this->url[0];
    	$post = null;
   		$table_custom_page = 'custom_page';
  		$post = $this->db->select($table_custom_page, "alt_name", ["alt_name" => $alt_name]);
		if(dimarr($post))  {
  			array_unshift($this->url, "_custom");
    		$unit = new unit ('start', $this->url);
    	}
    	else not_page();  				
	}
	function short_url() {

		$this->db = new medoo;
		$post = null;
		$alt_name = $this->url[0];
		if(preg_match(" /.html\z/i",$this->url[0]))
    	{
			$table_post = 'post';
  			$post = $this->db->select($table_post, "alt_name", ["AND" => [
				"alt_name" => $alt_name,
				"approve" => 1,
			]]);
  			if(dimarr($post))  {
  				array_unshift($this->url, "blog");
    			$unit = new unit ('start', $this->url);
    		}
    		else {
    			$post = null;
   				$table_static = 'static';
  				$post = $this->db->select($table_static, "alt_name", ["alt_name" => $alt_name]);
		  		if($post != "")  {
  					array_unshift($this->url, "static");
    				$unit = new unit('start', $this->url);
    			}
    			else $this->custom_link();  				
  			}  		
  		}	
		else  {
    			$f = false;
    			if(isset($this->url[1]) and $this->url[1] != '') {
    				$special_url = $this->url[0].'/'.$this->url[1];
					$table_post = 'post';
  					$post = $this->db->select($table_post, ['alt_name', 'special_url'], ["AND" => [
					"special_url" => $special_url,
					"approve" => 1,
					]]);
					$f = true; 
				}

  				if(dimarr($post) and $f == true)  {
  					array_unshift($this->url, "blog");
    				$unit = new unit ('start', $this->url);
    			}
    			else  
    			{
    		$post = null;
   			$table_category = 'category';
  			$post = $this->db->select($table_category, "alt_name", ["alt_name" => $alt_name]);
		  	if($post != "" and $post != array())  {
  				array_unshift($this->url, "category");
    			$unit = new unit('start', $this->url);
    		} 		
    		else {
				$table_post = 'post';
  				$post = $this->db->select($table_post, 'alt_name', ["AND" => [
				"alt_name" => $alt_name,
					"approve" => 1,
				]]);
  				if(dimarr($post))  {
  					array_unshift($this->url, "blog");
    				$unit = new unit ('start', $this->url);
    			}
    		else {
    			$post = null;
   				$table_static = 'static';
  				$post = $this->db->select($table_static, "alt_name", ["alt_name" => $alt_name]);
		  		if($post != "")  {
  					array_unshift($this->url, "static");
    				$unit = new unit('start', $this->url);
    			}
    			else $this->custom_link();  				
  			}  		
  		} 				
  		}
  	}
	}

	
	/*Проверка на разрешение запуска модуля*/
	public function rights($module) {
		$db = new medoo;
		
		$table_users = 'users';
		
		if ($this->config->getParam('location') != 'admin') return true;
		
		if (isset($_SESSION['user']['name'])) 
			$name = $_SESSION['user']['name'];
		else return false;
		$pages = $db->select($table_users, "allowed", ["name" => $name]);

		$pages = (explode(",", $pages[0]));
		 
		if ($this->config->getParam('location') == 'admin') {
			if ($module == 'admin') return true;
			if (array_search('_all', $pages) !== false)	return array_search('_all', $pages);
			else return array_search($module, $pages);
		}
		else return true;
	}	
	
	/*Запуск модулей*/
	public function start() {

		$globPath = ROOT.'/modules/*/config.php';
		$iterator = new GlobIterator($globPath);
		$filelist = array();
		$this->starttpl();
	
		//Условия определения админки/не админки, дефолтного конструктора
		if (($this->url[0] == '_start') or ($this->url[0] == '_all')) {
		    not_page();
		}
		if ($this->url[0] == '') {
		    $this->url[0] = '_start';
		} 
		if (is_numeric($this->url[0])) {
			$this->url[1] = $this->url[0];
		    $this->url[0] = '_start';
		} 
		foreach($iterator as $entry) {

   			if (file_exists($entry)) { 
   				require ($entry); //Подгрузка настроек контроллера
   			} else { 
   				return false;
   			}
   			
   			$this->className = $entry->getPath();
   			$this->className = str_replace(ROOT.'/modules/', '', $this->className);
   			/*Страницы в config.php модуля задаются следующим образом:
   				'pages' => 'admin: page1, page2, page3; index: page1, page2, page3',
			   */
			if (is_array($this->getParam('pages'))) {
				try {
					$location = $this->config->getParam('location');
					if (isset($this->getParam('pages')[$location]))
						$pages = $this->getParam('pages')[$location];
					else continue 1;
				} catch (Exception $e) {
					continue 1;
				}
			}
			else {
				$pages = str_replace(' ','',$this->getParam('pages'));
				$pages = explode(";", $pages);  
				$location = null;
				if (@ $this->config->getParam('location') == explode(":", current($pages))[0])  {
					@ $location = $this->config->getParam('location');
					$pages = explode(":", current($pages));
				}
				if ($this->config->getParam('location') == explode(":", end($pages))[0]) {
					@ $location = $this->config->getParam('location');
					$pages = explode(":", current($pages));

				}
				array_shift ($pages);
				@ $pages = explode(",", $pages[0]);
			}
   			if ((array_search($this->url[0], $pages) !== false) and ($location != '') and ($this->getParam('active') == 'enable')) {//Проверка на соответсвие страницы и включение
				$startList[] = Array('name' => $this->className,    'priority' => $this->getParam('priority'));
   				$this->classFlag = true; 
   				continue;
   			} 
   			if ((array_search('_all', $pages) !== false) and ($location != '') and ($this->getParam('active') == 'enable')) {//Проверка на соответсвие страницы и включение
				$startList[] = Array('name' => $this->className,    'priority' => $this->getParam('priority'));
   			}		 		
		}
		
		/*Сортировка по приоритету*/
		if ($this->classFlag === true) { 
			usort($startList, 'cmp');
			/*Запуск контроллеров согласно приоритета*/
			foreach($startList as $key) {
				//echo $key['name'];
				if ($this->rights($key['name']) !== false)
				$controller = new $key['name'] ($this->tpl, $this->url); //Запуск контроллера
			}
		}
		else {
   			if (($this->config->getParam('short_url') == 'enable') and ($this->config->getParam('location') === 'index')) $this->short_url();
   			else $this->custom_link();
		}		
		$this->view(); //Вывод 
	}

	/*Получение параметра модуля*/
	
	public function getParam($name) {
		$fileName = ROOT.'/modules/'.$this->className.'/config.php'; //Подготовка пути файла
		if (file_exists($fileName)) require ($fileName);
		return (isset($this->paramList[$name])) ? $this->paramList[$name] : NULL;
	}	
	public function getModuleParam($module, $name) {
		$fileName = ROOT.'/modules/'.$module.'/config.php'; //Подготовка пути файла
		if (file_exists($fileName)) require ($fileName);
		return (isset($this->paramList[$name])) ? $this->paramList[$name] : NULL;
	}	
}
?>
