<?php
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: engine/classes/class_login.php
=====================================================
 Авторизация
*****************************************************
*/ 
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!" );
}

class login extends controller 
{

	private $config;

	function __construct($url=null) {
		$this->config = new config;
   		if ($url[0] == 'login') {
   			$this->check();
			}
			elseif ($url[0] == 'forgot') {
				include (ROOT.'/vue/public/forgot.tpl');
			}
			elseif ($url[0] == 'recovery') {
   			$this->recovery();
			}
			else include (ROOT.'/vue/public/login.tpl');
	}

	protected function ipaddress() 	{
  		$strRemoteIP = $_SERVER['REMOTE_ADDR']; 
  		if (!$strRemoteIP) { $strRemoteIP = urldecode(getenv('HTTP_CLIENTIP'));}
  		if (getenv('HTTP_X_FORWARDED_FOR')) { $strIP = getenv('HTTP_X_FORWARDED_FOR'); }
  		elseif (getenv('HTTP_X_FORWARDED')) { $strIP = getenv('HTTP_X_FORWARDED'); }
  		elseif (getenv('HTTP_FORWARDED_FOR')) { $strIP = getenv('HTTP_FORWARDED_FOR'); }
  		elseif (getenv('HTTP_FORWARDED')) { $strIP = getenv('HTTP_FORWARDED'); }
  		else { $strIP = $_SERVER['REMOTE_ADDR']; }
  		if ($strRemoteIP != $strIP) { $strIP = $strRemoteIP.", ".$strIP; }
  		return $strIP;
	}
	protected function generate_password($number)

 {

 $arr = array('a','b','c','d','e','f',

 'g','h','i','j','k','l',

 'm','n','o','p','r','s',

 't','u','v','x','y','z',

 'A','B','C','D','E','F',

 'G','H','I','J','K','L',

 'M','N','O','P','R','S',

 'T','U','V','X','Y','Z',

 '1','2','3','4','5','6',

 '7','8','9','0');

 // Генерируем пароль

 $pass = "";

 for($i = 0; $i < $number; $i++)

 {

 // Вычисляем случайный индекс массива

 $index = rand(0, count($arr) - 1);

 $pass .= $arr[$index];

 }

 return $pass;

 }
	protected function check() {
		if (isset($_POST)) {
			$db = new medoo;
			
			$table = 'users'; 
			
			$login = clean_var($_POST['user']);
					
			$password = hash_formula($_POST['password']);
			
        	$column = ['password', 'id', 'activate', 'name'];
        	
        	$where = ['email' => $login];
        	
        	$result = $db->get($table, $column, $where);

        	if ($result != "" and password_verify($password , $result['password'])) {
        		
        		if($result['activate'] == '1') {
					$_SESSION['user'] = array();
      				$_SESSION['user']['name'] = clean_var($result['name']); 
      				$_SESSION['user']['email'] = $login; 
      				$_SESSION['user']['id'] = (int)$result['id'];
      				$_SESSION['user']['adminlog'] = 'admin';
      				$_SESSION['user']['logSESS'] = $login;
					echo $this->getLang('Successfully entered!').'<meta http-equiv="refresh" content="1; url=/panel/">'; 
				} 
				else echo $this->getLang('Incorrect data!'); 
        	}
        	else echo $this->getLang('Incorrect data!');
        	
		}
	}
	protected function recovery() {
		if (isset($_POST)) {
			$db = new medoo;
				
			$table = 'users'; 
			
			$login = clean_var($_POST['user']);
			$newPasNS = $this->generate_password(8);
			$newPas = hash_formula($newPasNS);
   		$newPas = password_hash($newPas, PASSWORD_BCRYPT);			
        	
        	$where = ['email' => $login];
        	
        	$has = $db->has($table, $where);
        	if ($has != true) 
        		exit('This user is not found');
        	$res = $db->update($table, [
		
				"password" => $newPas,
				
			], $where);

        	
        	$to = $login; 			

			//$to = "gorshkovoleg97@yandex.ru";
			//$headers= "From: =?utf-8?b?'". base64_encode('Сообщение из личного кабинета сайта') ."?= <support@seosp.ru>\r\n";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= "Content-type: text/html; charset=utf-8 \r\n";
// дополнительные данные
			$headers .= "From: DSS <support@seosp.ru>\r\n"; // от кого

			$message = "
				<html>			
				<head>
   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <title>Confirmation of registration</title>
    </head>
    <body>	
				You have submitted a password recovery request.<br>
				New password: $newPasNS</html>";
			
				//$message = "Имя: $name\r\nТема: $them\r\nДата: $date_g\r\nТекст: $text";

			$subject = '';
			mail($to, $subject, $message, $headers);
			echo "New password sent to e-mail".'<meta http-equiv="refresh" content="2; url=/panel/">';
		}
	}
}
?>
