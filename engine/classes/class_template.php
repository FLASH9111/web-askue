<?php 
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: engine/classes/class_template.php
=====================================================
 Класс работы с шаблонами
*****************************************************
*/ 
class template { 

    public  $dir = '.'; 
    public  $template = null; 
    protected  $module = null; 
    public  $copy_template = null; 
    public  $data = array(); 
    public  $block_data = array(); 
    public  $result = array('info' => '', 'content' => ''); 
    public  $template_parse_time = 0; 

    protected function getLangTpl($name) { 
		$langList = array ();
		$fileName = ROOT.'/language/'.LANG.'/base.php'; 
		if (file_exists($fileName)) {
			require ($fileName);
			$langList = $this->langList;
		}
		$this->langList = array ();
		if (!empty($this->module)) {
			$fileName = ROOT.'/modules/'.$this->module.'/language/'.LANG.'.php'; 
			if (file_exists($fileName)) require ($fileName);
			$this->langList = array_merge($langList, $this->langList);
		}
		return (isset($this->langList[$name])) ? $this->langList[$name] : $name;
    }

    public function lng($module = null) { 
    	$this->module = $module;
    	$row = null;
		if (stristr( $this->copy_template, "[lng:" )) { 
        	//$this->template = preg_replace( "#\\[lng:['\"](.+?)['\"]\\]#ies","\$this->getLangTpl('\\1')", $this->template); 
        	$this->copy_template = preg_replace_callback(
    			"#\[lng:(.+?)\]#i",
    			function ($m) use ($row) {
        			return $this->getLangTpl($m[1]);
    			},
    			$this->copy_template
			);
        } 
		return true;
    }

//задаём параметры основных переменных подгрузки шаблона 

    public function set($name , $var) { 
        if (is_array($var) && count($var)) { 
            foreach ($var as $key => $key_var) { 
                $this->set($key , $key_var); 
            } } else $this->data[$name] = $var; 
    } 
    
//Вылавливаем значение после равно в квадратных скобках  

    public function catch_value($name) {
    	$name = str_replace(array(']','['),array('',''),$name);
		$reg = '/\\['.$name.'=(?<number>\\d+)\\]/i';
		if (preg_match($reg, $this->copy_template, $match)) {
			return $match['number'];
		}
		else return false;
    } 

//заменяем тег со значением 

    public function set_tags_value($name, $var) { 
   		$name = str_replace(array(']','['),array('',''),$name);
		$reg = '/\\['.$name.'=(?<number>\\d+)\\]/i';
		if (preg_match($reg, $this->copy_template, $match)) {
			$this->copy_template = preg_replace($reg,$var,$this->copy_template);
			return true;
		}
		else return false;
    }    
    public function set_isset($name) { 
    	if (strpos($this->copy_template, $name) !== false) {
    		return true;
		}
		return false;
    }   
//Вставляем перед тегом, сохраняя его

    public function set_tags_before($name, $var) { 
   		$name=str_replace(array(']','['),array('',''),$name);
		$reg = "/\[".$name."\]/s";

		if (preg_match($reg, $this->copy_template, $match)) {
			$this->copy_template = preg_replace($reg,$var.'['.$name.']',$this->copy_template);
			return true;
		}
		else return false;
    }

//заменяем тег на что-либо 

    public function set_replace_tags($name, $var) { 
   		$name=str_replace(array(']','['),array('',''),$name);
		$this->copy_template = preg_replace("/\[".$name."\].*?\[".$name."\]/s",$var,$this->copy_template);
		return true;
    }    
//получаем значение между тегами 

    public function set_value_tags($name) { 
   		$name = str_replace(array(']','['),array('',''),$name);
   		preg_match("/\[".$name."\](.*?)\[".$name."\]/s",$this->copy_template,$tamp_while);
		if (isset($tamp_while[1])) return $tamp_while[1]; else return false;
    }  
//обозначаем блоки 

    public function set_block($name , $var) { 
        if (is_array($var) && count($var)) { 
            foreach ($var as $key => $key_var) { 
                $this->set_block($key , $key_var); 
            } } else $this->block_data[$name] = $var; 
    } 

//Возвращаем каркасный шаблон из файла

    public function return_template($tpl_name) { 
    	$time_before = $this->get_real_time(); 
		$tplRoot = $this->dir . DIRECTORY_SEPARATOR;
		$tplRoot = preg_replace('|([/]+)|s', '/', $tplRoot);
        if ($tpl_name == '' || !file_exists($tplRoot.$tpl_name)) { echo ("Unable to load template ". $tpl_name." "); return false;} 
        $this->template = file_get_contents($tplRoot.$tpl_name); 
        if (stristr( $this->template, "{{include file=" ) ) { 
        	$reg = '/{{include file="(.*?)".*?}}/im';
        	$this->template = preg_replace_callback(
    			$reg,
    			function ($m) {
        			return $this->sub_load_template($m[1]);
    			},
    			$this->template
			);
        } 
        return $this->template; 
    } 

//производим загрузку каркасного шаблона из файла

    public function load_template($tpl_name) { 
    	$time_before = $this->get_real_time(); 
		$tplRoot = $this->dir . DIRECTORY_SEPARATOR;
		$tplRoot = preg_replace('|([/]+)|s', '/', $tplRoot);
        if ($tpl_name == '' || !file_exists($tplRoot.$tpl_name)) { echo ("Unable to load template ". $tpl_name. " "); return false;} 
        $this->template = file_get_contents($tplRoot.$tpl_name); 
        if (stristr( $this->template, "{{include file=" ) ) { 
        	$reg = '/{{include file="(.*?)".*?}}/im';
        	$this->template = preg_replace_callback(
    			$reg,
    			function ($m) {
        			return $this->sub_load_template($m[1]);
    			},
    			$this->template
			);
        } 
        $this->copy_template = $this->template; 
    	$this->template_parse_time += $this->get_real_time() - $time_before; 
    	return true; 
    } 

//производим загрузку каркасного шаблона из строки

    public function load_template_str($tpl_str) { 
    	$time_before = $this->get_real_time(); 
        if ($tpl_str == '') { echo ("Unable to load template"); return false;} 
        $this->template = $tpl_str; 
        if (stristr( $this->template, "{include file=" ) ) { 
            $this->template = preg_replace( "#\\{include file=['\"](.+?)['\"]\\}#ies","\$this->sub_load_template('\\1')", $this->template); 
        } 
        $this->copy_template = $this->template; 
    	$this->template_parse_time += $this->get_real_time() - $time_before; 
    	return true; 
    } 

// этой функцией загружаем "подшаблоны" 

    public function sub_load_template($tpl_name) { 
   		$tplRoot = $this->dir . DIRECTORY_SEPARATOR;
		$tplRoot = preg_replace('|([/]+)|s', '/', $tplRoot);
        if ($tpl_name == '' || !file_exists($tplRoot.$tpl_name)) { die ("Unable to load template ". $tpl_name." "); return false;} 
        $template = file_get_contents($tplRoot.$tpl_name); 
        return $template; 
    } 

// очистка переменных шаблона 
    public function _clear() { 
    $this->data = array(); 
    $this->block_data = array(); 
    $this->copy_template = $this->template; 
    } 

    public function clear() { 
    $this->data = array(); 
    $this->block_data = array(); 
    $this->copy_template = null; 
    $this->template = null; 
    } 
//полная очистка включая результаты сборки шаблона 
    public function global_clear() { 
    $this->data = array(); 
    $this->block_data = array(); 
    $this->result = array(); 
    $this->copy_template = null; 
    $this->template = null; 
    } 
//сборка шаблона в единое целое 
    public function compile($tpl) { 
    $time_before = $this->get_real_time(); 
    foreach ($this->data as $key_find => $key_replace) { 
                $find[] = $key_find; 
                $replace[] = $key_replace; 
            } 
    @ $result = str_replace($find, $replace, $this->copy_template); 
    if (count($this->block_data)) { 
        foreach ($this->block_data as $key_find => $key_replace) { 
                $find_preg[] = $key_find; 
                $replace_preg[] = $key_replace; 
                } 
    $result = preg_replace($find_preg, $replace_preg, $result); 
    } 
    if (isset($this->result[$tpl])) $this->result[$tpl] .= $result; else $this->result[$tpl] = $result; 
    //$this->_clear(); 
    $this->template_parse_time += $this->get_real_time() - $time_before; 
    } 
//счётчик времени выполнения запросов сборки 
    public function get_real_time() 
    { 
        list($seconds, $microSeconds) = explode(' ', microtime()); 
        return ((float)$seconds + (float)$microSeconds); 
    } 
} 

?>