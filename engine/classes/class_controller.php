<?php
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: engine/classes/class_controller.php
=====================================================
 Родительский класс контроллеров (модулей)
*****************************************************
*/ 
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!" );
}

class controller {
	
	public $args;
	
	public $db;
	
	public $tpl;
	
	protected $paramList;
	
	protected $dirClass;
	
   	public function __construct() {

		$this->db = new medoo;

		$this->args = func_get_args();
		
		if (isset ($this->args[0][0])) $this->tpl = $this->args[0][0];
		
		$reflect =  new ReflectionClass($this);
     	$this->dirClass = basename(dirname($reflect->getFileName()));
   	}
   
   /*Параметр модуля*/
	public function getParam($name) {
		$fileName = ROOT.'/modules/'.$this->dirClass.'/config.php'; 
		if (file_exists($fileName)) require ($fileName);
		return (isset($this->paramList[$name])) ? $this->paramList[$name] : NULL;
	}
    /*Параметры модулей*/
    public function getParams() {
		$fileName = ROOT.'/modules/'.$this->dirClass.'/config.php'; 
		if (file_exists($fileName)) require ($fileName);
		return (isset($this->paramList)) ? $this->paramList : NULL;
	}
	/*Значение ассоциативного массива*/
	public function getArr($array = array (), $index) {
		return (isset($array[$index])) ? $array[$index] : NULL;
	}
	/*Перевод*/
	public function getLang($name) {
		$langList = array ();
		$fileName = ROOT.'/language/'.LANG.'/base.php';
		if (file_exists($fileName)) {
			require ($fileName);
			$langList = $this->langList;
		}
		$this->langList = array ();
		//echo $this->dirClass;

	//	$a = new \ReflectionClass('ZN\Database\InternalDB');

//var_dump($a->getFileName());

		$fileName = ROOT.'/modules/'.$this->dirClass.'/language/'.LANG.'.php'; 
		if (file_exists($fileName)) require ($fileName);
		$this->langList = array_merge($langList, $this->langList);
		return (isset($this->langList[$name])) ? $this->langList[$name] : $name;
	}
	protected function search($place) {
		if(isset($_POST['searchf'])) {
			@session_start();//стартуем сессию
			$_SESSION["searchf"] = array();
      		$_SESSION['searchf'][$this->args[0][1][0]]['text'] = clean_var($_POST['searchf']);
      		//$_SESSION['searchf']['url']['name'] = $this->args[0][1][0];
      	}
		if(isset($_SESSION['searchf'][$this->args[0][1][0]]['text']) and $_SESSION['searchf'][$this->args[0][1][0]]['text'] != '') {
			$text = $_SESSION['searchf'][$this->args[0][1][0]]['text'];
			$match = ["MATCH" =>[
				"columns" => [$place],
				"keyword" => "$text IN BOOLEAN MODE"
			]];
			return $match;
		}
		else return array();
	}

}

?>