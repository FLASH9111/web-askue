<?php
/*
*****************************************************
 https://сделаемсайт.рф
=====================================================
 Gorshkov O.V.
=====================================================
 Copyright (c) 2019
=====================================================
 Файл: modules/models/class_models.php
=====================================================
 Модуль управления моделями
*****************************************************
*/
if (!defined('READFILE')) {
	exit("Error! Hacking attempt!");
}
class models extends controller
{
	protected $config;
	public $script;

	protected $maxElem = 10;
	public function __construct()
	{
		parent::__construct(func_get_args());

		$this->config = new config;
		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'testcompute') {
			$this->testCompute();
			exit;
		}
		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'download' && isset($this->args[0][1][1])) {
			if (trim($this->args[0][1][1]) != '') {
				$file = $this->args[0][1][1];
				$this->file_force_download($file);
			}
			exit;
		}
		if (isset($_POST['postJson']) && !empty($_POST['postJson']) && clean_var($_POST['postJson']) == 'yes') {
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getmodel') {
				header('Content-type: application/json');
				$model = 1;
				if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
					$model = (int) ($this->args[0][1][1]);
				}
				echo  $this->getModel($model);
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getcommenttask' && is_numeric($this->args[0][1][1]) != false) {
				if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
					$id = (int) ($this->args[0][1][1]);
					header('Content-type: application/json');
					echo  $this->getComment($id);
				}
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendnewmodel') {
				echo  $this->sendNewModel();
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendcopymodel') {
				echo  $this->sendCopyModel();
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'computemodel') {
				$model = 1;
				if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
					$model = (int) ($this->args[0][1][1]);
				}
				echo  $this->computeModel($model);
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendmodeldata') {
				echo  $this->sendModelData();
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'tocompute') {
				$this->toCompute();
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'fromcompute') {
				$this->fromCompute();
			}
			exit;
		} else not_page();
		//exit("Error! Hacking attempt!");
	}
	function file_force_download($file)
	{
		$uploadDir = ROOT . '/resultfiles/';
		$patch = $uploadDir.$file.'.txt';
		if (file_exists($patch)) {
			// сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
			// если этого не сделать файл будет читаться в память полностью!
			if (ob_get_level()) {
				ob_end_clean();
			}
			// заставляем браузер показать окно сохранения файла
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . basename($patch));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($patch));
			// читаем файл и отправляем его пользователю
			readfile($patch);
			exit;
		}
		else {
			not_page();
		}
	}
	protected function computeModel($model)
	{
		if ($_SESSION['user']['id'] != null) {
			$table_indicator = 'indicator';
			$table_period = 'period';
			$table_tasks = 'tasks';
			$column = ['id', 'name', 'formula', 'type', 'importance', 'criterion_function', 'managed_rate'];
			$where = ['id_model' => $model];
			$post = $this->db->select($table_indicator, $column, $where);
			$joinTask = ['[>]models' =>['id' => 'id_task']];
			$whereTask = ['models.id' => $model];
			$columnTask = ['tasks.file_url'];
			$task = $this->db->get($table_tasks, $joinTask, $columnTask, $whereTask);
			//print_r($task);
			//echo $this->db->last();
			/*$where = ['id_task' => $task];
			$count = $this->db->count($table_models, $where);*/
			$k = 1;
			$m = 1;
			$p = 1;
			$br = "\n";
			$numColFile = "n1.1";
			$numColPerFile = "n1.1";
			$nameColFile = "num";
			$formulaColFile = '';
			$singleFile = '';
			$criterionFile = '';
			$importanceFile = '';
			$managedFile = '';
			$managed = '';
			$signFile = '';
			$limitFile = '';
			foreach ($post as $key => $value) {
				foreach ($value as $keyInd => $valueInd) {
					$value[$keyInd] = preg_replace('/\s/', "_", clean_var($valueInd));
				}
				if ($value['id'] != '') {
					$column = ['id', 'num', 'sign', 'limit', 'id_indicator'];
					$where = ['id_indicator' => $value['id']];
					$dataPeriod = $this->db->select($table_period, $column, $where);
					$countPeriod = count($dataPeriod);
					$post[$key] = $value + ['period' => $dataPeriod, 'count_period' => $countPeriod];
					foreach ($dataPeriod as $keyPer => $valuePer) {
						$numPer = ".$p";
						$p++;
					}
					if($value['type'] == 'single') {
						$typeInt = '2.'.$k;
						$k++;
						$singleFile = $singleFile."\tn" . $typeInt;
						$criterionFile = $criterionFile . "\t".$value['criterion_function'];
						$importanceFile = $importanceFile . "\t".$value['importance'];
						if($value['managed_rate'] == '1') {
							$managed = '1';
						}
						else $managed = '';
						$managedFile = $managedFile . "\t" . $managed;
						$tIndPer = $typeInt; 
						$numColPerFile = $numColPerFile . "\tn" . $tIndPer;

						$p = 1;
					}
					elseif ($value['type'] == 'multi') {
						$typeInt = '4.'.$m;
						$m++;
						$p = 1;
						foreach ($dataPeriod as $keyPer => $valuePer) {
							$numPer = ".$p";
							$tIndPer = '4.'.$m.$numPer;
							$numColPerFile = $numColPerFile . "\tn" . $tIndPer;
							$p++;
						}
					}
					foreach ($dataPeriod as $keyPer => $valuePer) {
						$signFile = $signFile . "\t" . $valuePer['sign'];
						if ($valuePer['sign'] == '') {
							$limit = '';
						}
						else $limit = $valuePer['limit'];
						$limitFile = $limitFile . "\t" . $limit;
					}
					$numColFile = $numColFile . "\tn" . $typeInt;
					$nameColFile = $nameColFile . "\t" . $value['name'];
					$formulaColFile = $formulaColFile . "\t" . $value['formula'];
				}
			}
			$fileModel = $numColFile.$br.$nameColFile.$br.$formulaColFile.$br.$singleFile.$br.$criterionFile.$br.$importanceFile.$br.$managedFile.$br.$numColPerFile.$br.$signFile.$br.$limitFile;
			$patch = ROOT . "/sendfiles/$model.txt";
			$fileData = $task['file_url'];
			if (file_exists($fileData)) {
				$data = file_get_contents($fileData);
				$fileCompute = $fileModel.$br.$data;
				$check_file = new check_file ();
				$check_file->inputFile = $fileCompute;
				if($check_file -> check()[0] == 0) {
					file_put_contents($patch, $fileCompute);
					$table_models = 'models';
					$whereModel = ['id' => $model];
					$data = ['to_server' => 1, 'file_url' => $patch];
					$res = $this->db->update($table_models, $data, $whereModel);
					return json_encode([0=>0]);
				}
				else {
					return json_encode($check_file -> check()[0]);
				}
			}
		}
		return json_encode([0=>1, 1=>"Error"]);
	}
	protected function sendNewModel()
	{
		$table_models = 'models';
		$table_tasks = 'tasks';
		$table_indicator = 'indicator';
		$table_period = 'period';
		$sumPeriods = 0;
		$errorShortLog[0] = 0;
		foreach ($_POST as $key => $value) {
			$_POST[$key] = clean_var($value);
		}
		if (isset($_POST['id_task']) && $_POST['id_task'] != '' && isset($_POST['name_model']) && $_POST['name_model'] != '' && (int) $_POST['countSingleIndicator'] >= 2) {
			$date = date('Y-m-d H:i:s');
			$data = ['id_task' => $_POST['id_task'], 'name_model' => $_POST['name_model'], 'date_create' => $date, 'status' => 'New', 'compute' => 0];
			$where = ['id' => $_POST['id_task']];
			if ($this->db->has($table_tasks, $where)) {
				$post = $this->db->get($table_tasks, ['id', 'sum_periods'], $where);
				if (is_numeric($_POST['countSingleIndicator']) != false && is_numeric($_POST['countMultiIndicator']) != false && is_numeric($_POST['countPeriodsIndicator']) != false) {
					$sumPeriods = (int) $_POST['countSingleIndicator'] + (int) $_POST['countMultiIndicator'] * (int) $_POST['countPeriodsIndicator'];
					if ($post['sum_periods'] == $sumPeriods)
						$res = $this->db->insert($table_models, $data);
					else {
						$errorShortLog[1] = 1;//Несоответствие суммы
						$errorShortLog[0] = 1;
					}
				}
				else {
					$errorShortLog[1] = 2;
					$errorShortLog[0] = 1;
				}
			}
			else {
				$errorShortLog[0] = 1;
				$errorShortLog[1] = 2;
			}
			$idModel = $this->db->max($table_models, 'id');
			if (is_numeric($_POST['countSingleIndicator']) != false) {
				for($i = 0; $i < $_POST['countSingleIndicator']; $i++) {
					$nameInd = 'Param '.($i+1);
					$dataSingle = ['name' => $nameInd, 'id_model' => $idModel, 'type' => 'single'];
					$res = $this->db->insert($table_indicator, $dataSingle);
					$idIndicator = $this->db->max($table_indicator, 'id');
					$dataPeriod = ['num' => 'X', 'id_indicator' => $idIndicator];
					$res = $this->db->insert($table_period, $dataPeriod);
				}
				$maxI = $i;
			}
			else {
				$errorShortLog[0] = 1;
				$errorShortLog[1] = 2;
			}
			if (is_numeric($_POST['countMultiIndicator']) != false) {
				for($i = 0; $i < $_POST['countMultiIndicator']; $i++) {
					$nameInd = 'Param '.($i+1+$maxI);
					$allDataPeriod = [];
					$dataMulti= ['name' => $nameInd, 'id_model' => $idModel, 'type' => 'multi'];
					$res = $this->db->insert($table_indicator, $dataMulti);
					$idIndicator = $this->db->max($table_indicator, 'id');
					if (is_numeric($_POST['countPeriodsIndicator']) != false) {
						for($j = 0; $j < (int) $_POST['countPeriodsIndicator']; $j++) {
							$namePeriod = ($j+1);
							$dataPeriod = ['num' => $namePeriod, 'id_indicator' => $idIndicator];
							$allDataPeriod [] = $dataPeriod;
						}
						$res = $this->db->insert($table_period, $allDataPeriod);
					}
					else {
						$errorShortLog[0] = 1;
						$errorShortLog[1] = 2;
					}
				}
			}
			else {
				$errorShortLog[0] = 1;
				$errorShortLog[1] = 2;
			}
		}
		return json_encode($errorShortLog);
	}
	protected function sendModelData()
	{
		$table_models = 'models';
		$table_indicator = 'indicator';
		$table_period = 'period';
		$modelData = json_decode($_POST['modelData'], true);
		//print_r(($modelData));
		if (count($modelData) > 0) {
			foreach ($modelData as $value) {
				$dataIndicator = ['name' => $value['name'], 'formula' => $value['formula'], 'importance' => $value['importance'], 'criterion_function' => $value['criterion_function'], 'managed_rate' => $value['managed_rate']];
				$where = ['id' => $value['id']];
				$res = $this->db->update($table_indicator, $dataIndicator, $where);
 			 	if (count($value['period']) > 0) {
					//print_r($value['period']);
					foreach ($value['period'] as $keyPeriod => $valuePeriod) {
						$dataPeriod = ['num' => $valuePeriod['num'], 'sign' => $valuePeriod['sign'], 'limit' => $valuePeriod['limit'], 'id_indicator' => $valuePeriod['id_indicator']];
						$wherePeriod = ['id' => $valuePeriod['id']];
						$res = $this->db->update($table_period, $dataPeriod, $wherePeriod);
					}
				}
			}
		} else return false;
		echo (1);

		exit;
		echo ($this->getLang('Successfully saved!'));
	}
	protected function sendCopyModel()
	{
		if (isset($_POST['id']) && $_POST['id'] != '') {
			$id = $_POST['id'];
			$table_models = 'models';
			$name = $_POST['name_model']; 
			$where = ['id_model' => $id];
			$whereModel = ['id' => $id];
			$main = $this->db->get($table_models, ['id_task'], $whereModel);
			$idTask = $main['id_task'];
			$date = date('Y-m-d H:i:s');
			$data = ['id_task' => $idTask, 'name_model' => $name, 'date_create' => $date, 'status' => 'New', 'compute' => 0];
			
			$res = $this->db->insert($table_models, $data);
			$newId = $this->db->id();
			$table_indicator = 'indicator';
			$table_period = 'period';
			$columnInd = ['id', 'name', 'formula', 'type', 'importance', 'criterion_function', 'managed_rate'];
			$ind = $this->db->select($table_indicator, $columnInd, $where);
			if (count($ind) > 0)
				foreach ($ind as $keyInd => $valueInd) {
					$valueInd['id_model'] = $newId;
					$idThisInd = $valueInd['id'];
					unset($valueInd['id']);
					$res = $this->db->insert($table_indicator,  $valueInd);
					$idNewInd = $this->db->id();
					$columnPer = ['num', 'sign', 'limit'];
					$where = ['id_indicator' => $idThisInd];
					$per = $this->db->select($table_period, $columnPer, $where);
					foreach ($per as $valuePer) {
						$valuePer['id_indicator'] = $idNewInd;
						$res = $this->db->insert($table_period,  $valuePer);
					}
				}
		} else return false;
		$errorShortLog[0] = 0;

		return json_encode($errorShortLog);
	}
	protected function getModel($model)
	{
		if ($_SESSION['user']['id'] != null) {
			$table_indicator = 'indicator';
			$table_period = 'period';
			$table_models = 'models';
			$column = ['id', 'name', 'formula', 'type', 'importance', 'criterion_function', 'managed_rate'];
			$columnModels = ['id', 'name_model', 'status', 'compute', 'result_url', 'to_server'];
			$where = ['id_model' => $model];
			$whereModel = ['id' => $model];
			$post = $this->db->select($table_indicator, $column, $where);
			$main = $this->db->get($table_models, $columnModels, $whereModel);
			/*$where = ['id_task' => $task];
			$count = $this->db->count($table_models, $where);*/
			foreach ($post as $key => $value) {
				if ($value['id'] != '') {
					$column = ['id', 'num', 'sign', 'limit', 'id_indicator'];
					$where = ['id_indicator' => $value['id']];
					$dataPeriod = $this->db->select($table_period, $column, $where);
					$countPeriod = count($dataPeriod);
					$post[$key] = $value + ['period' => $dataPeriod, 'count_period' => $countPeriod];
				}
			}
			//print_r($post);
			//$fileResultName = basename($post['result_url'], ".txt"); 
			$resp = ['model' => $post, 'main' => $main];
			return json_encode($resp);
		}
		return;
	}
	protected function getHistoryTasks($page)
	{
		if ($_SESSION['user']['id'] != null) {
			$table_tasks = 'tasks';
			$begin = $page * $this->maxElem - $this->maxElem;
			$column = ['id', 'date_upload', 'name_task'];
			$id = clean_var($_SESSION['user']['id']);
			$where = ['id_user' => $id, 'LIMIT' => [$begin, $this->maxElem], 'ORDER' => ['id' => 'DESC']];
			$post = $this->db->select($table_tasks, $column, $where);
			$where = ['id_user' => $id];
			$count = $this->db->count($table_tasks, $where);
			//print_r($post);
			//$fileResultName = basename($post['result_url'], ".txt"); 
			$resp = ['list' => $post, 'count' => $count, 'countPage' => $this->maxElem];
			return json_encode($resp);
		}
		return;
	}
	protected function getComment($id)
	{
		if ($id != null) {
			$table_comments_task = 'comments_task';
			$column = ['id', 'text'];
			$where = ['id_task' => $id];
			$post = $this->db->get($table_comments_task, $column, $where);
			return json_encode($post);
		}
		return;
	}
	protected function delTasks()
	{
		if (isset($_POST['id_task']) && $_POST['id_task'] != '') {
			$idArr = explode(',', $_POST['id_task']);
			foreach ($idArr as $id) {
				$table_tasks = 'tasks';
				$table_comments_task = 'comments_task';
				$where = ['id' => $id];
				$where2 = ['id_task' => $id];
				$post = $this->db->delete($table_tasks, $where);
				$post = $this->db->delete($table_comments_task, $where2);
			}
			
			$ans = ['status' => 'ok'];
			return json_encode($ans);
		}
		return;
	}
	protected function AddTask()
	{
		$table_tasks = 'tasks';
		$i = 0;
		$file = '';
		$flag = false;
		$errorLog = [0];
		$errorShortLog = array();
		$errorHtmlLog = '';
		$filename = clean_var($_POST['nameFile']);
		$date = date('Y-m-d H:i:s');
		if (!isset($_POST['nameFile']) || $_POST['nameFile'] == '') {
			$data = ["id_user" => $_SESSION['user']['id'], "status" => 'Error', "date_upload" => $date, "sum" => 0, "file_name" => $filename];
			$res = $this->db->insert($table_tasks, $data);
			exit('error');
		}
		foreach (file($_FILES['dataFile']['tmp_name']) as $value) {
			$value = str_replace(" ", "\t", $value);
			$value = mb_convert_encoding($value, "UTF-8", "windows-1251");
			/*Номер элемента*/
			if ($i == 0 || $i == 3 || $i == 7) {
				if (!preg_match("/^[n0-9.\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
				if ($i == 0 || $i == 7) {
					$mas = preg_split('/[\s]/u', $value, -1, PREG_SPLIT_NO_EMPTY);
					$pok = ['1', '2', '4'];
					foreach ($mas as $m) {
						if (@!in_array(trim($m[1]), $pok)) {
							$flag = true;
							array_push($errorLog, ["num_str" => $i, "mes" => "Not 1x/2x/4x", "value" => $value]);
							array_push($errorShortLog, "Line number: $i, Error: Not 1x/2x/4x");
						}
					}
				}
				if ($i == 0 || $i == 7) {
					$mas = preg_split('/[\s]/u', $value, -1, PREG_SPLIT_NO_EMPTY);
					$pok = ['1', '2', '4'];
					foreach ($mas as $m) {
						if (@!in_array(trim($m[1]), $pok)) {
							$flag = true;
							array_push($errorLog, ["num_str" => $i, "mes" => "Not 1x/2x/4x", "value" => $value]);
							array_push($errorShortLog, "Line number: $i, Error: Not 1x/2x/4x");
						}
					}
				}
			}
			if ($i == 2 || $i == 4) {
				if (!preg_match("/^[a-x\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
				if ($i == 2) {
					$mas = preg_split('/[\t]/u', $value, -1, PREG_SPLIT_NO_EMPTY);
					$ways = ['sum', 'risk', 'stdev', 'date'];
					foreach ($mas as $m) {
						if (!in_array(trim($m), $ways)) {
							$flag = true;
							array_push($errorLog, ["num_str" => $i, "mes" => "Not sum/risk/stdev/date", "value" => $value]);
							array_push($errorShortLog, "Line number: $i, Error: Not sum/risk/stdev/date");
						}
					}
				}
				if ($i == 4) {
					$mas = preg_split('/[\s]/u', $value, -1, PREG_SPLIT_NO_EMPTY);
					$maxmin = ['max', 'min'];
					foreach ($mas as $m) {
						if (!in_array(trim($m), $maxmin)) {
							$flag = true;
							array_push($errorLog, ["num_str" => $i, "mes" => "Not min/max", "value" => $value]);
							array_push($errorShortLog, "Line number: $i, Error: Not min/max");
						}
					}
				}
			}
			if ($i == 2  || $i == 3) {
				if (!preg_match("/^\t{1}+$/", $value[0])) { //Проверка на пробел
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters (tab/space)", "value" => $value[0]]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters (tab/space)");
				}
			}
			if ($i == 5) {
				if (!preg_match("/^[0-9\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
			}

			if ($i == 6) {
				if (!preg_match("/^\t{1}+$/", $value[0])) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value[0]]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
				if (trim($value) !== '1') { //Проверка на 1
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Not 1", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Not 1");
				}
			}
			if ($i == 8) {
				if (!preg_match("/^[<>\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
			}
			if ($i == 7) {
				if ($value != "") {
					$mtk = explode("\t", trim($value));
					$max10 = count($mtk);
				}
			}
			if ($i == 8) {
				if ($value != "") {
					$mtk8 = explode("\t", trim($value));
				}
			}
			if ($i == 9) {
				if (!preg_match("/^[0-9,\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
				if ($value != "") {
					$mtk9 = explode("\t", trim($value));
					$mtk8 = array_diff($mtk8, array(''));
					$mtk9 = array_diff($mtk9, array(''));
					if (count($mtk8) != count($mtk9)) {
						$flag = true;
						array_push($errorLog, ["num_str" => '8 and 9', "mes" => "Wrong number of items", "value" => $value]);
						array_push($errorShortLog, "Line number: 8 and 9, Error: Wrong number of items");
					}
					foreach ($mtk9 as $key => $value9) {
						if (!(($value9 != "" && $value9  != " ") && ($mtk8[$key] != "" && $mtk8[$key]  != " "))) {
							$flag = true;
							array_push($errorLog, ["num_str" => $i, "mes" => "Wrong number", "value" => $value]);
							array_push($errorShortLog, "Line number: $i, Error: Wrong number");
							print_r($mtk8);
							print_r($mtk9);
						}
					}
				}
			}
			if ($i > 9) {
				if (!preg_match("/^[0-9-,\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
				if ($value != "") {
					$mtk = explode("\t", trim($value));
					if ($max10 !== count($mtk)) { //Проверка на количество
						$flag = true;
						array_push($errorLog, ["num_str" => $i, "mes" => "Wrong number", "value" => $value]);
						array_push($errorShortLog, "Line number: $i, Error: Wrong number");
					}
				}
			}

 
			//echo $value[0];
			$i++;
			$file = $file . "\n" . $value;
		}
		if ($flag == true) {
			//print_r($errorLog);
			$data = ["id_user" => $_SESSION['user']['id'], "status" => 'Error', "date_upload" => $date, "sum" => 0, "file_name" => $filename];
			$res = $this->db->insert($table_files, $data);
			$errorHtmlLog = implode("<br>", array_slice($errorShortLog, 0, 10));
			$errorShortLog[1] = $errorHtmlLog;
			$errorShortLog[0] = 1;
			return json_encode($errorShortLog);
			exit('error');
		}

	
		$sum = 10;
		$newId = $this->db->max($table_files, 'id') + 1;
		// Каталог, в который мы будем принимать файл:
		$uploaddir = ROOT . '/userfiles/';
		$userFileName = $_POST['nameFile'];
		$filename = $newId . '.' . substr(strrchr(basename($_FILES['dataFile']['name']), '.'), 1);
		$uploadfile = $uploaddir . $filename;
		// Копируем файл из каталога для временного хранения файлов:
		if (move_uploaded_file($_FILES['dataFile']['tmp_name'], $uploadfile)) { } else {
			exit('error');
		}
		//mail($to, $subject, $body, $headers); //Отправляем письмо
		$data = ["id_user" => $_SESSION['user']['id'], "status" => 'Sending', "date_upload" => $date, "sum" => $sum, "file_name" => $userFileName, "compute" => 0, "file_url" => $uploadfile];
		$res = $this->db->insert($table_files, $data);
		//echo $file;
		/*for($i = 0; $i < end() {
			foreach (file($_FILES['dataFile']['tmp_name']) as $value) {
				print_r($value[0]);			
			}*/
		/*foreach ($_POST as $key => $value) {
				$_POST[$key] = clean_var($value);
			}*/
		//print_r(file($_FILES['dataFile']['tmp_name'])[10]);
		//$thFile = file_get_contents($_FILES['dataFile']['tmp_name']);
		//echo mb_convert_encoding($thFile,"UTF-8","windows-1251");
		//echo ($this->getLang('Successfully saved!'));
		$errorShortLog[0] = 0;
		return json_encode($errorShortLog);
		//echo 'done';
	}
}
