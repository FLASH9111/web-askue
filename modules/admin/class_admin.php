<?php
/*
*****************************************************
 CMS SVCE 2.0 
=====================================================
 http://svce.ru/
=====================================================
 Gorshkov O.V.
=====================================================
 Copyright (c) 2013-2016 Zed Group
=====================================================
 Файл: modules/all_modules/class_admin.php
=====================================================
 Вывод информации на главную страницу админ-панели 
*****************************************************
*/
class admin extends controller {
   	public function __construct() {
   		parent::__construct(func_get_args());
   		if (stristr($this->args[0][1][0], 'exit') == false) { 	  		

   			$this->tpl->set('{{title}}', $this->getLang('Dashboard'));
   			$this->tpl->set('{{subtext}}', $this->getLang('Welcome'));
   			$this->tpl->set('{{left}}', '');
   			$this->tpl->set('{{right}}', '');
   			$this->tpl->set('{{icon}}', '<img src="/modules/admin/images/dash.png">');
   		}
   		elseif (stristr($this->args[0][1][0], 'exit') != false) {
   			unset ($_SESSION['user']);
			session_destroy();
			header("location: /panel/");
			exit;
		}
   	}
    protected function home() {
		$this->tpl->load_template('home.tpl');
		$this->tpl->compile('content');
		$meta = '<title>'.$this->getLang('Home').'</title>';
  		$this->tpl->load_template_str($meta );
   		$this->tpl->compile('meta');
   	}
}
?>