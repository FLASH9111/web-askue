<?php if (! defined ( "READFILE" ))
{
    exit ( "Error! Hacking attempt!" );
}
$this->paramList = array(
'name' => 'Dashboard',
'description' => 'Home',
'active' => 'enable',
'pages' => ['admin'=> ['_start', 'exit', 'lk', 'balance', 'feedback', 'prices', 
                      'addobject', 'object', 'editobject', 
                      'devices', 'device', 'adddevice', 'editdevice', 
                      'registers', 'register', 'addregister', 'editregister']],
'priority' => '2'
);
?>
