<?php
/*
*****************************************************
 CMS SVCE 2.0 
=====================================================
 https://сделаемсайт.рф
=====================================================
 Gorshkov O.V.
=====================================================
 Copyright (c) 2019
=====================================================
 Файл: modules/all_modules/class_files.php
=====================================================
 Модуль управления файлами
*****************************************************
*/
if (!defined('READFILE')) {
	exit("Error! Hacking attempt!");
}
class files extends controller
{
	protected $config;
	public $script;

	protected $maxElem = 100;
	public function __construct()
	{
		parent::__construct(func_get_args());
		$this->config = new config;
		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'testcompute') {
			$this->testCompute();
			exit;
		}
		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'download' && isset($this->args[0][1][1])) {
			if (trim($this->args[0][1][1]) != '') {
				$file = $this->args[0][1][1];
				$this->file_force_download($file);
			}
			exit;
		}
		if (isset($_POST['postJson']) && !empty($_POST['postJson']) && clean_var($_POST['postJson']) == 'yes') {
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'gethistoryfiles') {
				header('Content-type: application/json');
				$page = 1;
				if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
					$page = (int) ($this->args[0][1][1]);
				}
				echo  $this->getHistoryFiles($page);
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getcomment' && is_numeric($this->args[0][1][1]) != false) {
				if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
					$id = (int) ($this->args[0][1][1]);
					header('Content-type: application/json');
					echo  $this->getComment($id);
				}
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'delfile') {
				header('Content-type: application/json');
				echo  $this->delFile();
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendcomment') {
				echo  $this->AddSQL();
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendfile') {
				echo  $this->AddFile();
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'tocompute') {
				$this->toCompute();
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'fromcompute') {
				$this->fromCompute();
			}
			exit;
		} else not_page();
		//exit("Error! Hacking attempt!");
	}
	function file_force_download($file)
	{
		$uploadDir = ROOT . '/resultfiles/';
		$patch = $uploadDir.$file.'.txt';
		if (file_exists($patch)) {
			// сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
			// если этого не сделать файл будет читаться в память полностью!
			if (ob_get_level()) {
				ob_end_clean();
			}
			// заставляем браузер показать окно сохранения файла
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . basename($patch));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($patch));
			// читаем файл и отправляем его пользователю
			readfile($patch);
			exit;
		}
		else {
			not_page();
		}
	}
	protected function getHistoryFiles($page)
	{
		if ($_SESSION['user']['id'] != null) {
			$table_files = 'files';
			$begin = $page * $this->maxElem - $this->maxElem;
			$column = ['id', 'date_upload', 'status', 'sum', 'file_name', 'result_url'];
			$id = clean_var($_SESSION['user']['id']);
			$where = ['id_user' => $id, 'LIMIT' => [$begin, $this->maxElem], 'ORDER' => ['date_upload' => 'DESC']];
			$post = $this->db->select($table_files, $column, $where);
			$where = ['id_user' => $id];
			$count = $this->db->count($table_files, $where);
			foreach ($post as $key => $value) {
				if ($value['result_url'] != '') {
					$fileResultName = basename($value['result_url'], ".txt"); 
					$downloadUrl = '/download/'.$fileResultName;
					$post[$key] = $value + ['download_url' => $downloadUrl];
				}
			}
			//print_r($post);
			//$fileResultName = basename($post['result_url'], ".txt"); 
			$resp = ['list' => $post, 'count' => $count, 'countPage' => $this->maxElem];
			return json_encode($resp);
		}
		return;
	}
	protected function getComment($id)
	{
		if ($id != null) {
			$table_comments = 'comments';
			$column = ['id', 'text'];
			$where = ['id_file' => $id];
			$post = $this->db->get($table_comments, $column, $where);
			return json_encode($post);
		}
		return;
	}
	protected function delfile()
	{
		if (isset($_POST['id_file']) && $_POST['id_file'] != '') {
			$id = $_POST['id_file'];
			$table_files = 'files';
			$table_comments = 'comments';
			$where = ['id' => $id];
			$where2 = ['id_file' => $id];
			$post = $this->db->delete($table_files, $where);
			$post = $this->db->delete($table_comments, $where2);
			$ans = ['status' => 'ok'];
			return json_encode($ans);
		}
		return;
	}
	protected function toCompute()
	{
		if (isset($_POST['getFile']) && !empty($_POST['getFile']) && clean_var($_POST['getFile']) == 'yes') {
			$table_files = 'files';
			$column = ['id', 'file_url', 'compute'];
			$where = ['compute' => 0, 'ORDER' => 'date_upload'];
			$post = $this->db->get($table_files, $column, $where);
			if ($post != '') {
				//   			print_r($post);
				$file = $post['file_url'];
				if (file_exists($file)) {
					header('Content-Description: File Transfer');
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename=' . basename($file));
					header('Expires: 0');
					header('Cache-Control: must-revalidate');
					header('Pragma: public');
					header('Content-Length: ' . filesize($file));
					readfile($file);
					$data = ["compute" => 1, "status" => 'Send'];
					$where = ["id" => $post["id"]];
					$res = $this->db->update($table_files, $data, $where);
					exit;
				} else {
					$data = ["compute" => 1, "status" => 'Error'];
					$where = ["id" => $post["id"]];
					$res = $this->db->update($table_files, $data, $where);
					echo "";
				}
				//echo $post['file_url'];	
			}
		}
		return;
	}
	protected function fromCompute()
	{
		$table_files = 'files';
		if (isset($_POST['sendFile']) && !empty($_POST['sendFile']) && clean_var($_POST['sendFile']) == 'yes') {
			if ($_FILES['resultFile']['tmp_name'] != "") {
				$uploadDir = ROOT . '/resultfiles/';
				$fileName = basename($_FILES['resultFile']['name']);
				$idFile = clean_var($_POST["idFile"]);
				$uploadFile = $uploadDir . $fileName;
				// Копируем файл из каталога для временного хранения файлов:
				if (move_uploaded_file($_FILES['resultFile']['tmp_name'], $uploadFile)) {
					if ($idFile != "") {
						$status = clean_var($_POST['status']);
						$data = ["result_url" => "/resultfiles/$fileName", "status" => $status];
						$where = ["id" => $idFile];
						$res = $this->db->update($table_files, $data, $where);
						echo 'Ok';
					}
				} else {
					exit('error');
				}
			}
		}
		return;
	}
	protected function testCompute()
	{
		$url = 'http://lk.seosp.ru/fromcompute';
		$file = ROOT . '/robots.txt';
		//$r = new HttpRequest('http://lk.seosp.ru/fromcompute', HttpRequest::METH_POST);
		/*$r->setOptions(array('cookies' => array('lang' => 'en')));
$r->addPostFields(array('postJson' => 'yes', 'sendFile' => 'yes'));
$r->addPostFile('resultFile', $file, 'text/pain');*/
		/*try {
    echo $r->send()->getBody();
} catch (HttpException $ex) {
    echo $ex;
}*/
		$sURL = "http://lk.seosp.ru/fromcompute"; // URL-адрес POST 
		$sPD = "postJson=yes&sendFile=yes&bench=150"; // Данные POST
		$aHTTP = array(
			'http' => // Обертка, которая будет использоваться
			array(
				'method'  => 'POST', // Метод запроса
				// Ниже задаются заголовки запроса
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $sPD
			)
		);
		$context = stream_context_create($aHTTP);
		$contents = file_get_contents($sURL, false, $context);
		var_dump($contents);
	}
	protected function AddSQL()
	{
		$table_comments = 'comments';
		foreach ($_POST as $key => $value) {
			$_POST[$key] = clean_var($value);
		}
		if (isset($_POST['id']) && $_POST['id'] != '') {
			$id = $_POST['id'];
			$where = ['id' => $id];
			if ($this->db->has($table_comments, $where))
				$res = $this->db->update(
					$table_comments,
					[
						"text" => $_POST["comment"]
					],
					$where
				);
			else $res = $this->db->insert($table_comments, [
				"text" => $_POST["comment"], "id_file" => $_POST["id_file"]
			]);
		}
		echo ($this->getLang('Successfully saved!'));
	}
	protected function AddFile()
	{
		$table_files = 'files';
		$i = 0;
		$file = '';
		$flag = false;
		$errorLog = [0];
		$errorShortLog = array();
		$errorHtmlLog = '';
		$filename = clean_var($_POST['nameFile']);
		$date = date('Y-m-d H:i:s');
		if (!isset($_POST['nameFile']) || $_POST['nameFile'] == '') {
			$data = ["id_user" => $_SESSION['user']['id'], "status" => 'Error', "date_upload" => $date, "sum" => 0, "file_name" => $filename];
			$res = $this->db->insert($table_files, $data);
			exit('error');
		}
		foreach (file($_FILES['dataFile']['tmp_name']) as $value) {
			$value = str_replace(" ", "\t", $value);
			$value = mb_convert_encoding($value, "UTF-8", "windows-1251");
			/*Номер элемента*/
			if ($i == 0 || $i == 3 || $i == 7) {
				if (!preg_match("/^[n0-9.\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
				if ($i == 0 || $i == 7) {
					$mas = preg_split('/[\s]/u', $value, -1, PREG_SPLIT_NO_EMPTY);
					$pok = ['1', '2', '4'];
					foreach ($mas as $m) {
						if (@!in_array(trim($m[1]), $pok)) {
							$flag = true;
							array_push($errorLog, ["num_str" => $i, "mes" => "Not 1x/2x/4x", "value" => $value]);
							array_push($errorShortLog, "Line number: $i, Error: Not 1x/2x/4x");
						}
					}
				}
				if ($i == 0 || $i == 7) {
					$mas = preg_split('/[\s]/u', $value, -1, PREG_SPLIT_NO_EMPTY);
					$pok = ['1', '2', '4'];
					foreach ($mas as $m) {
						if (@!in_array(trim($m[1]), $pok)) {
							$flag = true;
							array_push($errorLog, ["num_str" => $i, "mes" => "Not 1x/2x/4x", "value" => $value]);
							array_push($errorShortLog, "Line number: $i, Error: Not 1x/2x/4x");
						}
					}
				}
			}
			if ($i == 2 || $i == 4) {
				if (!preg_match("/^[a-x\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
				if ($i == 2) {
					$mas = preg_split('/[\t]/u', $value, -1, PREG_SPLIT_NO_EMPTY);
					$ways = ['sum', 'risk', 'stdev', 'date'];
					foreach ($mas as $m) {
						if (!in_array(trim($m), $ways)) {
							$flag = true;
							array_push($errorLog, ["num_str" => $i, "mes" => "Not sum/risk/stdev/date", "value" => $value]);
							array_push($errorShortLog, "Line number: $i, Error: Not sum/risk/stdev/date");
						}
					}
				}
				if ($i == 4) {
					$mas = preg_split('/[\s]/u', $value, -1, PREG_SPLIT_NO_EMPTY);
					$maxmin = ['max', 'min'];
					foreach ($mas as $m) {
						if (!in_array(trim($m), $maxmin)) {
							$flag = true;
							array_push($errorLog, ["num_str" => $i, "mes" => "Not min/max", "value" => $value]);
							array_push($errorShortLog, "Line number: $i, Error: Not min/max");
						}
					}
				}
			}
			if ($i == 2  || $i == 3) {
				if (!preg_match("/^\t{1}+$/", $value[0])) { //Проверка на пробел
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters (tab/space)", "value" => $value[0]]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters (tab/space)");
				}
			}
			if ($i == 5) {
				if (!preg_match("/^[0-9\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
			}

			if ($i == 6) {
				if (!preg_match("/^\t{1}+$/", $value[0])) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value[0]]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
				if (trim($value) !== '1') { //Проверка на 1
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Not 1", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Not 1");
				}
			}
			if ($i == 8) {
				if (!preg_match("/^[<>\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
			}
			if ($i == 7) {
				if ($value != "") {
					$mtk = explode("\t", trim($value));
					$max10 = count($mtk);
				}
			}
			if ($i == 8) {
				if ($value != "") {
					$mtk8 = explode("\t", trim($value));
				}
			}
			if ($i == 9) {
				if (!preg_match("/^[0-9,\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
				if ($value != "") {
					$mtk9 = explode("\t", trim($value));
					$mtk8 = array_diff($mtk8, array(''));
					$mtk9 = array_diff($mtk9, array(''));
					if (count($mtk8) != count($mtk9)) {
						$flag = true;
						array_push($errorLog, ["num_str" => '8 and 9', "mes" => "Wrong number of items", "value" => $value]);
						array_push($errorShortLog, "Line number: 8 and 9, Error: Wrong number of items");
					}
					foreach ($mtk9 as $key => $value9) {
						if (!(($value9 != "" && $value9  != " ") && ($mtk8[$key] != "" && $mtk8[$key]  != " "))) {
							$flag = true;
							array_push($errorLog, ["num_str" => $i, "mes" => "Wrong number", "value" => $value]);
							array_push($errorShortLog, "Line number: $i, Error: Wrong number");
							print_r($mtk8);
							print_r($mtk9);
						}
					}
				}
			}
			if ($i > 9) {
				if (!preg_match("/^[0-9-,\s]+$/", $value)) { //Проверка на символы
					$flag = true;
					array_push($errorLog, ["num_str" => $i, "mes" => "Forbidden characters", "value" => $value]);
					array_push($errorShortLog, "Line number: $i, Error: Forbidden characters");
				}
				if ($value != "") {
					$mtk = explode("\t", trim($value));
					if ($max10 !== count($mtk)) { //Проверка на количество
						$flag = true;
						array_push($errorLog, ["num_str" => $i, "mes" => "Wrong number", "value" => $value]);
						array_push($errorShortLog, "Line number: $i, Error: Wrong number");
					}
				}
			}


			//echo $value[0];
			$i++;
			$file = $file . "\n" . $value;
		}
		if ($flag == true) {
			//print_r($errorLog);
			$data = ["id_user" => $_SESSION['user']['id'], "status" => 'Error', "date_upload" => $date, "sum" => 0, "file_name" => $filename];
			$res = $this->db->insert($table_files, $data);
			$errorHtmlLog = implode("<br>", array_slice($errorShortLog, 0, 10));
			$errorShortLog[1] = $errorHtmlLog;
			$errorShortLog[0] = 1;
			return json_encode($errorShortLog);
			exit('error');
		}

		// $filename = "form.txt"; //Имя файла для прикрепления
		/*Отправка письма*/

		$to = "gorshkovoleg97@yandex.ru"; //Кому
		$to = $this->config->getParam('mainmail');
		$from = "support@seosp.ru"; //От кого
		$subject = "Файл с системы"; //Тема
		$message = "Файл пользователя с e-mail: " . $_SESSION['user']['email']; //Текст письма
		$boundary = "---"; //Разделитель
		/* Заголовки */
		$headers = "From: $from\nReply-To: $from\n";
		$headers .= "Content-Type: multipart/mixed; boundary=\"$boundary\"";
		$body = "--$boundary\n";
		/* Присоединяем текстовое сообщение */
		$body .= "Content-type: text/html; charset='utf-8'\n";
		$body .= "Content-Transfer-Encoding: quoted-printablenn";
		$body .= "Content-Disposition: attachment; filename==?utf-8?B?" . base64_encode($filename) . "?=\n\n";
		$body .= $message . "\n";
		$body .= "--$boundary\n";
		//$file = fopen($filename, "r"); //Открываем файл
		//$text = fread($file, filesize($filename)); //Считываем весь файл
		// fclose($file); //Закрываем файл
		/* Добавляем тип содержимого, кодируем текст файла и добавляем в тело письма */
		$body .= "Content-Type: application/octet-stream; name==?utf-8?B?" . base64_encode($filename) . "?=\n";
		$body .= "Content-Transfer-Encoding: base64\n";
		$body .= "Content-Disposition: attachment; filename==?utf-8?B?" . base64_encode($filename) . "?=\n\n";
		$body .= chunk_split(base64_encode($file)) . "\n";
		$body .= "--" . $boundary . "--\n";
		$sum = 10;
		$newId = $this->db->max($table_files, 'id') + 1;
		// Каталог, в который мы будем принимать файл:
		$uploaddir = ROOT . '/userfiles/';
		$userFileName = $_POST['nameFile'];
		$filename = $newId . '.' . substr(strrchr(basename($_FILES['dataFile']['name']), '.'), 1);
		$uploadfile = $uploaddir . $filename;
		// Копируем файл из каталога для временного хранения файлов:
		if (move_uploaded_file($_FILES['dataFile']['tmp_name'], $uploadfile)) { } else {
			exit('error');
		}
		//mail($to, $subject, $body, $headers); //Отправляем письмо
		$data = ["id_user" => $_SESSION['user']['id'], "status" => 'Sending', "date_upload" => $date, "sum" => $sum, "file_name" => $userFileName, "compute" => 0, "file_url" => $uploadfile];
		$res = $this->db->insert($table_files, $data);
		//echo $file;
		/*for($i = 0; $i < end() {
			foreach (file($_FILES['dataFile']['tmp_name']) as $value) {
				print_r($value[0]);			
			}*/
		/*foreach ($_POST as $key => $value) {
				$_POST[$key] = clean_var($value);
			}*/
		//print_r(file($_FILES['dataFile']['tmp_name'])[10]);
		//$thFile = file_get_contents($_FILES['dataFile']['tmp_name']);
		//echo mb_convert_encoding($thFile,"UTF-8","windows-1251");
		//echo ($this->getLang('Successfully saved!'));
		$errorShortLog[0] = 0;
		return json_encode($errorShortLog);
		//echo 'done';
	}
}
