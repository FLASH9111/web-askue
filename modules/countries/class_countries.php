<?php
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: modules/countries/class_countries.php
=====================================================
 Модуль вывода JSON стран
*****************************************************
*/
if (!defined('READFILE')) {
	exit("Error! Hacking attempt!");
}
class countries extends controller
{
	public function __construct()
	{
		parent::__construct(func_get_args());
		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getcountries') {
			header('Content-type: application/json');
			echo  $this->getCountries();
		}
		exit;
	}

	protected function getCountries()
	{
		if (isset($_SESSION['user']['id']) && $_SESSION['user']['id'] != null) {
			$table_countries = 'countries';
			$countries = $this->db->select($table_countries, ['id_countries', 'country'], ['ORDER' => 'id_countries']);
			return json_encode($countries);
		}
		else{
			return json_encode(['errorCode' => 'ACCESS_DENIED']);
		}
		return;
	}
}
