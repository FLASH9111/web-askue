<?php
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: modules/regions/class_regions.php
=====================================================
 Модуль вывода JSON регионов
*****************************************************
*/
if (!defined('READFILE')) {
	exit("Error! Hacking attempt!");
}
class regions extends controller
{
	protected $config;
	public $script;

	protected $maxElem = 10;
	public function __construct()
	{
		parent::__construct(func_get_args());
		$this->config = new config;
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getregions') {
				header('Content-type: application/json');
				echo  $this->getRegions();
			}
			exit;
	}

	protected function getRegions()
	{
		if (isset($_SESSION['user']['id']) && $_SESSION['user']['id'] != null) {
			$table_regions = 'regions';
			$regions = $this->db->select($table_regions, ['id_regions', 'region'], ['ORDER'=>'id_regions']);
			return json_encode($regions);
		}
		else{
			return json_encode(['errorCode' => 'ACCESS_DENIED']);
		}
		return;
	}
}
