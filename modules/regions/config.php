<?php if (! defined ( "READFILE" ))
{
    exit ( "Error! Hacking attempt!" );
}
$this->paramList = array(
'name' => 'Модуль вывода JSON регионов',
'active' => 'enable',
'pages' => 'index: getregions',
'priority' => '2'
);
?>