<?php
/*
*****************************************************
 CMS SVCE 2.0 
=====================================================
 https://сделаемсайт.рф
=====================================================
 Gorshkov O.V.
=====================================================
 Copyright (c) 2019
=====================================================
 Файл: modules/all_modules/class_users.php
=====================================================
 Модуль вывода и редактирования профиля
*****************************************************
*/
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!");
}
class account extends controller {
  	protected $config;
  	public $script;
   	public function __construct() {
   		parent::__construct(func_get_args());
   		if(isset($_POST['postJson']) && !empty($_POST['postJson']) && clean_var($_POST['postJson']) == 'yes') {
	   		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getlk') {
 		   		header('Content-type: application/json');
					echo  $this->getData();
				}
	   		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendlk') {
					echo  $this->addSQL();
				}
    			exit;
			}
    		exit("Error! Hacking attempt!");
   	}
   	protected function getData() {
   		if ($_SESSION['user']['email'] != null) {
				$table_users = 'users';
				$column = ['id', 'name', 'email', 'phone'];
				$name = clean_var($_SESSION['user']['email']);
				$where = ['email' => $name];
				$post = $this->db->get($table_users, $column, $where);
				return json_encode($post);		
   		}
			return;
   	}


   	protected function addSQL() {
   		$table_users = 'users';
   		foreach ($_POST as $key => $value) {
   			if ($key == 'inMailLk') {
   				if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
					  	echo ($this->getLang('Email is not valid!'));
						return; 			
   				}
   				$where = ['email' => $value];

					if($this->db->has($table_users, $where)) {
					  	echo ($this->getLang('Email is not valid!'));
					}
   			}
   			if ($key == 'Access') exit("Error! Hacking attempt!");
   			if ($key == 'inNewPasswordLk' and trim($value) != '') { 
   				$value = hash_formula($value);
   				$value = password_hash($value, PASSWORD_BCRYPT);
   			}

				$_POST[$key] = clean_var($value);
			}
				$email = clean_var($_SESSION['user']['email']);
				$where = ['email' => $email];
			if (isset($_POST["inNewPasswordLk"]) and trim($_POST["inNewPasswordLk"]) != '') {
					$res = $this->db->update($table_users, [
						"password" => $_POST["inNewPasswordLk"],
						"phone" => $_POST["inPhoneLk"],
						"email" => $_POST["inMailLk"]
					],
					$where);
			}
			else {
					$res = $this->db->update($table_users, [
						"phone" => $_POST["inPhoneLk"],
						"email" => $_POST["inMailLk"]
					],
					$where);
			}
			echo ($this->getLang('Successfully saved!'));
   	}
}
?>