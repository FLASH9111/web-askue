<?php
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: modules/objects/class_objects.php
=====================================================
 Модуль управления объектами
*****************************************************
*/
if (!defined('READFILE')) {
	exit("Error! Hacking attempt!");
}
class objects extends controller
{
	protected $config;
	public $script;

	protected $maxElem = 10;
	public function __construct()
	{
		parent::__construct(func_get_args());
		$this->config = new config;
		if ((isset($this->args[0][1][1]) && $this->args[0][1][0] == 'getobject' && is_numeric($this->args[0][1][1]) != false) || (isset($this->args[0][1][1]) && $this->args[0][1][0] == 'geteditobject' && is_numeric($this->args[0][1][1]) != false)) {
			if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
				$id = (int) ($this->args[0][1][1]);
				header('Content-type: application/json');
				echo  $this->getObject($id);
				exit;
			}
		}
		if (isset($_POST['postJson']) && !empty($_POST['postJson']) && clean_var($_POST['postJson']) == 'yes') {
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getobjects') {
				header('Content-type: application/json');
				$page = 1;
				if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
					$page = (int) ($this->args[0][1][1]);
				}
				echo  $this->getObjects($page);
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'delobjects') {
				header('Content-type: application/json');
				echo  $this->delObjects();
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendobject') {
				$id = 0;
				if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
					$id = (int) ($this->args[0][1][1]);
				}
				header('Content-type: application/json');
				echo  $this->sendObject($id);
				}
			exit;
		} else not_page();
		//exit("Error! Hacking attempt!");
	}
//eyJrIjoiUDY5c25TaFJUcFRrUHRsNTBwTTlOdUxqNkpmR1B1V08iLCJuIjoid2ViLWludGVyZmFjZSIsImlkIjoxfQ==
	protected function getObjects($page)
	{
		if (isset($_SESSION['user']['id']) && $_SESSION['user']['id'] != null) {
			$table_objects = 'objects';
			$table_regions = 'regions';
			$begin = $page * $this->maxElem - $this->maxElem;
			if (isset($_POST['searchFunction']) && clean_var($_POST['searchFunction']) != '') {
				if (isset($_POST['searchNameObject']) && clean_var($_POST['searchNameObject']) != '') {
					$_SESSION['searchObjects']['objects.object_name'] = clean_var($_POST['searchNameObject']);
				} else {
					unset($_SESSION['searchObjects']['objects.object_name']);
				}
				if (isset($_POST['searchRegionObject']) && clean_var($_POST['searchRegionObject']) != '') {
					$_SESSION['searchObjects']['objects.id_regions_regions'] = clean_var($_POST['searchRegionObject']);
				} else {
					unset($_SESSION['searchObjects']['objects.id_regions_regions']);
				}
				if (isset($_POST['searchStreetObject']) && clean_var($_POST['searchStreetObject']) != '') {
					$_SESSION['searchObjects']['objects.street'] = clean_var($_POST['searchStreetObject']);
				} else {
					unset($_SESSION['searchObjects']['objects.street']);
				}
			}
			$idUser = $_SESSION['user']['id'];
			$whereObjects = ['object_roles.id_users' => $idUser];
			if(isset($_SESSION['searchObjects'])) {
			foreach ($_SESSION['searchObjects'] as $key => $value) {
				if (isset($value) && $value != '') {
					$whereObjects[$key] =  $value;
				}
			}}
			//print_r($whereOrders);
			$join = [
				'[>]regions' => ['objects.id_regions_regions' => 'id_regions'],
				'[>]countries' => ['objects.id_countries_countries' => 'id_countries'],
				'[>]object_roles' => ['objects.id_objects' => 'id_objects_objects']
			];
			$column = ['objects.id_objects', 'count_devices'=>Medoo::raw('(SELECT COUNT(id_devices) FROM devices WHERE id_objects_objects = <id_objects>)'), 'objects.object_name', 'regions.region', 'countries.country', 'objects.locality', 'objects.street', 'objects.house', 'objects.building', 'objects.flat'];
			$where = ['AND' => $whereObjects, 'LIMIT' => [$begin, $this->maxElem], 'ORDER' => ['objects.id_objects' => 'DESC']];
			$post = $this->db->select($table_objects, $join, $column, $where);
			$sql = $this->db->last();

			$count = $this->db->count($table_objects, $join, '*', ['AND' => $whereObjects]);
			$regions = $this->db->select($table_regions, ['id_regions', 'region'], ['ORDER'=>'id_regions']);
			if (isset($_SESSION['searchObjects'])) {
				$searchObjects = $_SESSION['searchObjects'];
			}
			else {
				$searchObjects = '';
			}
			$resp = [
				'list' => $post, 'regions' => $regions, 'count' => $count, 'countPage' => $this->maxElem,
				'searchObjects' => $searchObjects, //'sql' => "$sql"
			];
			//print_r($_POST);
			return json_encode($resp);
		}
		return;
	}
	protected function getObject($id_objects)
	{
		if (isset($_SESSION['user']['id']) && $_SESSION['user']['id'] != null) {
			if ($id_objects != null) {
				$table_objects = 'objects';
				$join = [
					'[>]regions' => ['objects.id_regions_regions' => 'id_regions'],
					'[>]countries' => ['objects.id_countries_countries' => 'id_countries'],
					'[>]object_roles' => ['objects.id_objects' => 'id_objects_objects']
				];
				$column = ['objects.id_objects', 'objects.id_countries_countries', 'objects.id_regions_regions', 'count_devices'=>Medoo::raw('(SELECT COUNT(id_devices) FROM devices WHERE id_objects_objects = <id_objects>)'), 
				'objects.object_name', 'regions.region', 'countries.country', 'objects.locality', 'objects.street', 'objects.house', 'objects.building', 'objects.flat', 'objects.remark', 'objects.additional', 'object_roles.id_roles_roles'];
				$idUser = $_SESSION['user']['id'];
				$whereObject = ['AND' => ['object_roles.id_users' => $idUser, 'objects.id_objects' => $id_objects]];
				$objectData = $this->db->get($table_objects, $join, $column, $whereObject);
				return json_encode(['objectData' => $objectData]);
			}
			else{
				return json_encode(['errorCode' => 'INVALID_ID']);
			}
		}
		else{
			return json_encode(['errorCode' => 'ACCESS_DENIED']);
		}
	}
	protected function delObjects()
	{
		if (isset($_POST['id_pos']) && $_POST['id_pos'] != '') {
			$idArr = explode(',', $_POST['id_pos']);
			foreach ($idArr as $id) {	
				$table_objects = 'objects';
				$table_devices = 'devices';
				$table_modbustcp_parameters = 'modbustcp_parameters';
				$table_modbus_registers = 'modbus_registers';
				$table_device_roles = 'device_roles';
				$table_object_roles = 'object_roles';
				$where = ['id_objects' => $id];
				$this->db->delete($table_objects, $where);
				$whereDevices = ['id_objects_objects' => $id];
				$devices = $this->db->select($table_devices, ['id_devices'], $whereDevices);
				foreach ($devices as $device) {
					$whereDevice = ['id_devices_devices' => $device['id_devices']];
					$this->db->delete($table_modbustcp_parameters, $whereDevice);
					$this->db->delete($table_modbus_registers, $whereDevice);
					$this->db->delete($table_device_roles, $whereDevice);
				}
				$this->db->delete($table_object_roles, $whereDevices);
				$this->db->delete($table_devices, $whereDevices);
			}
			$ans = ['status' => 'ok'];
			return json_encode($ans);
		}
		return;
	}

	protected function sendObject($id_objects)
	{
		if (isset($_SESSION['user']['id']) && $_SESSION['user']['id'] != null) {
			$table_objects = 'objects';
			$table_object_roles = 'object_roles';
			$id_users = (int) $_SESSION['user']['id'];
			if (isset($_POST['editobject']) && $_POST['editobject'] != '') {
				$editObjectDecode = json_decode($_POST['editobject'], true);
			}
			$editObject = [];
			foreach ($editObjectDecode as $key => $value) {
				if (clean_var($value) != "" && clean_var($value) != '0')
					$editObject[$key] = clean_var($value);
			}
			if (isset($editObject['object_name']) && $editObject['object_name'] != ''){
				if ($id_objects != 0) {
					if ($this->db->has($table_objects, ['id_objects' => $id_objects])) {
						if ($this->db->has($table_object_roles, 
							['AND' => ['id_objects_objects' => $id_objects, 'id_users' => $id_users, 'id_roles_roles' => 1]])) 
						{
							$this->db->update(
								$table_objects,
								$editObject,
								['id_objects' => $id_objects]
							);
							$ans = ['ans' => 'succesful'];
						}
					}
					else {
						$ans = ['ans' => 'not_id'];
					}
				}
				else {
					$this->db->insert(
						$table_objects,
						$editObject,
						['id_objects' => $id_objects]
					);
					$id_objects = $this->db->id();
					$this->db->insert(
						$table_object_roles,
						['id_users' => $id_users, 
						'id_objects_objects' => $id_objects,
						'id_roles_roles' => 1
						]
					);
					$ans = ['ans' => 'succesful'];
				}
			}
			else {
				$ans = ['ans' => 'not_name'];
			}
			return json_encode($ans);
		}
		else {
			$ans = ['ans' => 'no_permission'];
			return json_encode($ans);
		}
	}
}
