<?php if (! defined ( "READFILE" ))
{
    exit ( "Error! Hacking attempt!" );
}
$this->paramList = array(
'name' => 'Модуль отслеживания объектов',
'active' => 'enable',
'pages' => ['admin' => ['getobjects', 'sendobject', 'delobjects'], 'index' => ['getobject', 'geteditobject']],
'priority' => '2'
);
?>