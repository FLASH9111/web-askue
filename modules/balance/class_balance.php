<?php
/*
*****************************************************
 CMS SVCE 2.0 
=====================================================
 https://сделаемсайт.рф
=====================================================
 Gorshkov O.V.
=====================================================
 Copyright (c) 2019
=====================================================
 Файл: modules/all_modules/class_balance.php
=====================================================
 Модуль управления балансом
*****************************************************
*/
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!");
}
class balance extends controller {
  	protected $config;
  	public $script;
   	public function __construct() {
   		parent::__construct(func_get_args());
   		if(isset($_POST['postJson']) && !empty($_POST['postJson']) && clean_var($_POST['postJson']) == 'yes') {
	   		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getthisbalance') {
 		   		header('Content-type: application/json');
					echo  $this->getThisBalance();
				}
	   		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'gethistorybalance') {
 		   		header('Content-type: application/json');
					echo  $this->getHistoryBalance();
				}
	   		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendlk') {
					echo  $this->addSQL();
				}
    			exit;
			}
    		exit("Error! Hacking attempt!");
   		$this->config = new config;
   	}
   	protected function getThisBalance() {
   		if ($_SESSION['user']['id'] != null) {
				$table_users = 'users';
				$column = ['balance'];
				$id = clean_var($_SESSION['user']['id']);
				$where = ['id' => $id];
				$post = $this->db->get($table_users, $column, $where);
				return json_encode($post);		
   		}
			return;
   	}
   	protected function getHistoryBalance() {
   		if ($_SESSION['user']['id'] != null) {
				$table_transaction = 'transaction';
				$column = ['id', 'date_transaction', 'status', 'sum'];
				$id = clean_var($_SESSION['user']['id']);
				$where = ['id_user' => $id];
				$post = $this->db->select($table_transaction, $column, $where);
				//print_r($post);
				return json_encode($post);		
   		}
			return;
   	}

   	protected function addSQL() {
   		$table_users = 'users';
   		foreach ($_POST as $key => $value) {
   			if ($key == 'inMailLk') if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
			  	echo ($this->getLang('Email is not valid!'));
				return; 			
   			}
   			if ($key == 'Access') exit("Error! Hacking attempt!");
   			if ($key == 'inNewPasswordLk' and trim($value) != '') { 
   				$value = hash_formula($value);
   				$value = password_hash($value, PASSWORD_BCRYPT);
   			}

				$_POST[$key] = clean_var($value);
			}
				$name = clean_var($_SESSION['user']['name']);
				$where = ['name' => $name];
			if (isset($_POST["inNewPasswordLk"]) and trim($_POST["inNewPasswordLk"]) != '') {
					$res = $this->db->update($table_users, [
						"password" => $_POST["inNewPasswordLk"],
						"industry" => $_POST["inIndustryLk"],
						"phone" => $_POST["inPhoneLk"],
						"company" => $_POST["inCompanyLk"],
						"email" => $_POST["inMailLk"]
					],
					$where);
			}
			else {
					$res = $this->db->update($table_users, [
						"industry" => $_POST["inIndustryLk"],
						"phone" => $_POST["inPhoneLk"],
						"company" => $_POST["inCompanyLk"],
						"email" => $_POST["inMailLk"]
					],
					$where);
			}
			echo ($this->getLang('Successfully saved!'));
   	}
}
?>