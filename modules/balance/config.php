<?php if (! defined ( "READFILE" ))
{
    exit ( "Error! Hacking attempt!" );
}
$this->paramList = array('label' => 'no',
'label_priority' => '3',
'label_title' => 'Users',
'label_url' => '/panel/lk/',
'label_icon' => '/modules/users/images/users_label.png',
'menu' => 'no',
'menu_priority' => '9',
'menu_title' => 'Users',
'menu_url' => '/panel/lk/',
'menu_icon' => '/modules/users/images/users_menu.png',
'name' => 'Users',
'description' => 'Личный кабинет',
'active' => 'enable',
'pages' => 'admin: getthisbalance, gethistorybalance',
'priority' => '2'
);
?>