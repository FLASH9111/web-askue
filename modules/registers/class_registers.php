<?php
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: modules/registers/class_registers.php
=====================================================
 Модуль управления регистрами
*****************************************************
*/
if (!defined('READFILE')) {
	exit("Error! Hacking attempt!");
}
class registers extends controller
{
	protected $config;
	public $script;

	protected $maxElem = 10;
	public function __construct()
	{
		parent::__construct(func_get_args());
		$this->config = new config;
		if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getmodbusfunctions') {
			header('Content-type: application/json');
			echo  $this->getModbusFunctions();
			exit;
		}
		elseif (isset($this->args[0][1][1]) && $this->args[0][1][0] == 'geteditregister' && is_numeric($this->args[0][1][1]) != false) {
			if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
				$id = (int) ($this->args[0][1][1]);
				header('Content-type: application/json');
				echo  $this->getRegister($id);
				exit;
			}
		}
		elseif (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendregister') {
			header('Content-type: application/json');
			echo  $this->sendRegister();
			exit;
		}
		if (isset($_POST['postJson']) && !empty($_POST['postJson']) && clean_var($_POST['postJson']) == 'yes') {
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getregisters' && is_numeric($this->args[0][1][1]) != false) {
				header('Content-type: application/json');
				$page = 1;
				$id = (int) ($this->args[0][1][1]);
				if (isset($this->args[0][1][2]) && trim($this->args[0][1][2]) != '') {
					$page = (int) ($this->args[0][1][2]);
				}
				echo  $this->getRegisters($id, $page);
			}
			elseif (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'deldevices') {
				header('Content-type: application/json');
				echo  $this->delRegisters();
			}
			exit;
		} else not_page();
		//exit("Error! Hacking attempt!");
	}
	//eyJrIjoiUDY5c25TaFJUcFRrUHRsNTBwTTlOdUxqNkpmR1B1V08iLCJuIjoid2ViLWludGVyZmFjZSIsImlkIjoxfQ==
	protected function getModbusFunctions()
	{
		if (isset($_SESSION['user']['id']) && $_SESSION['user']['id'] != null) {
			$table_modbus_functions = 'modbus_functions';
			
			$modbus_functions = $this->db->select($table_modbus_functions, ['id_modbus_functions', 'function_code', 'function_name'], ['ORDER'=>'function_code']);
			return json_encode($modbus_functions);
		}
		else{
			return json_encode(['errorCode' => 'ACCESS_DENIED']);
		}
	}
	protected function getRegisters($idDevices = 0, $page)
	{
		if (isset($_SESSION['user']['id']) && $_SESSION['user']['id'] != null && $idDevices != 0) {
			$table_registers = 'modbus_registers';
			$begin = $page * $this->maxElem - $this->maxElem;
			if (isset($_POST['searchFunction']) && clean_var($_POST['searchFunction']) != '') {
				if (isset($_POST['searchNameRegisters']) && clean_var($_POST['searchNameRegisters']) != '') {
					$_SESSION['searchRegisters']['modbus_registers.channel_name'] = clean_var($_POST['searchNameRegisters']);
				} else {
					unset($_SESSION['searchRegisters']['modbus_registers.channel_name']);
				}
				if (isset($_POST['searchAddressRegisters']) && clean_var($_POST['searchAddressRegisters']) != '') {
					$_SESSION['searchRegisters']['modbus_registers.physical_address'] = clean_var($_POST['searchAddressRegisters']);
				} else {
					unset($_SESSION['searchRegisters']['modbus_registers.physical_address']);
				}
			}
			$idUser = $_SESSION['user']['id'];
			$whereRegisters = ['AND' => ['device_roles.id_users' => $idUser, 'modbus_registers.id_devices_devices' => $idDevices]];
			if (isset($_SESSION['searchRegisters'])) {
				foreach ($_SESSION['searchRegisters'] as $key => $value) {
					if (isset($value) && $value != '') {
						$whereRegisters[$key] =  $value;
					}
				}
			}
			$join = [
				'[>]device_roles' => ['modbus_registers.id_devices_devices' => 'id_devices_devices'],
				'[>]modbus_functions' => ['modbus_registers.id_modbus_functions_modbus_functions' => 'id_modbus_functions']
			];
			$column = ['modbus_registers.channel_name', 'modbus_registers.physical_address', 'modbus_functions.function_code', 'modbus_registers.id_modbus_registers'];
			$where = ['AND' => $whereRegisters, 'LIMIT' => [$begin, $this->maxElem], 'ORDER' => ['modbus_registers.id_modbus_registers' => 'DESC']];
			$post = $this->db->select($table_registers, $join, $column, $where);
			$sql = $this->db->last();

			$count = $this->db->count($table_registers, $join, '*', ['AND' => $whereRegisters]);
			if (isset($_SESSION['searchRegisters'])) {
				$searchRegisters = $_SESSION['searchRegisters'];
			} else {
				$searchRegisters = '';
			}
			$resp = [
				'list' => $post, 'count' => $count, 'countPage' => $this->maxElem,
				'searchRegisters' => $searchRegisters, //'sql' => "$sql"
			];
			//print_r($_POST);
			return json_encode($resp);
		}
		return;
	}
	protected function getRegister($id_modbus_registers)
	{
		if (isset($_SESSION['user']['id']) && $_SESSION['user']['id'] != null) {
			if ($id_modbus_registers != null) {
				$table_modbus_registers = 'modbus_registers';
				$join = [
					'[>]device_roles' => ['modbus_registers.id_devices_devices' => 'id_devices_devices'],
				];
				$column = [
					'modbus_registers.id_modbus_registers', 'modbus_registers.physical_address', 'modbus_registers.id_modbus_functions_modbus_functions', 
					'modbus_registers.formula', 'modbus_registers.round', 'modbus_registers.channel_name'
				];
				$idUser = $_SESSION['user']['id'];
				$where = ['AND' => ['device_roles.id_users' => $idUser, 'modbus_registers.id_modbus_registers' => $id_modbus_registers]];
				$modbus_registers = $this->db->get($table_modbus_registers, $join, $column, $where);
				return json_encode(['modbus_registers' => $modbus_registers]);
			}
			return;
		}
	}
	protected function delRegisters()
	{
		if (isset($_POST['id_pos']) && $_POST['id_pos'] != '') {
			$idArr = explode(',', $_POST['id_pos']);
			foreach ($idArr as $id) {
				$table_modbus_registers = 'modbus_registers';
				$whereDevice = ['id_modbus_registers' => $id];
				$this->db->delete($table_modbus_registers, $whereDevice);
			}
			$ans = ['status' => 'ok'];
			return json_encode($ans);
		}
		return;
	}

	protected function sendRegister()
	{
		if ($_SESSION['user']['id'] != null) {
			$postBody = file_get_contents('php://input');
			$postData = json_decode($postBody, true);
			if (isset($postBody) && !empty($postData)) {
				$table_modbus_registers = 'modbus_registers';
				$table_devices = 'devices';
				$table_device_roles = 'device_roles';
				$id_users = (int) $_SESSION['user']['id'];
				if (isset($postData['id_modbus_registers']) && $postData['id_modbus_registers'] != '' && is_int($postData['id_modbus_registers'])) {
					$id_modbus_registers = $postData['id_modbus_registers'];
				}
				else {
					return json_encode(['status' => 'error', 'errorMsg' => 'not_id']);
				}
				if (isset($postData['channel_name']) && $postData['channel_name'] != '') {
					$channel_name = $postData['channel_name'];
				}
				else {
					return json_encode(['status' => 'error', 'errorMsg' => 'not_name']);
				}
				if (isset($postData['physical_address']) && $postData['physical_address'] != '') {
					$physical_address = $postData['physical_address'];
				}
				else {
					return json_encode(['status' => 'error', 'errorMsg' => 'not_physical_address']);
				}
				if (isset($postData['id_modbus_functions_modbus_functions']) && $postData['id_modbus_functions_modbus_functions'] != '') {
					$id_modbus_functions_modbus_functions = $postData['id_modbus_functions_modbus_functions'];
				}
				else {
					return json_encode(['status' => 'error', 'errorMsg' => 'not_modbus_functions']);
				}
				if (isset($postData['id_devices_devices']) && $postData['id_devices_devices'] != '') {
					$id_devices_devices = $postData['id_devices_devices'];
				}
				else {
					$id_devices_devices = 0;
				}
				if (isset($postData['round']) && $postData['round'] != '') {
					$round = $postData['round'];
				}
				else {
					$round = 1;
				}
				if (isset($postData['formula']) && $postData['formula'] != '') {
					$formula = $postData['formula'];
				}
				else {
					$formula = "";
				}
				if ($id_modbus_registers == 0) {
					if ($id_devices_devices != 0) {
						if ($this->db->has($table_devices, ['id_devices' => $id_devices_devices])) {
							if ($this->db->has(
								$table_device_roles,
								['AND' => ['id_devices_devices' => $id_devices_devices, 'id_users' => $id_users, 'id_roles_roles' => 1]]
							)) {
								$dataToBase = [
									'physical_address' => $physical_address,
									'channel_name' => $channel_name,
									'id_modbus_functions_modbus_functions' => $id_modbus_functions_modbus_functions,
									'id_devices_devices' => $id_devices_devices,
									'formula' => $formula,
									'round' => $round
								];
								$this->db->insert(
									$table_modbus_registers,
									$dataToBase,
								);			
								return json_encode(['status' => 'succesful']);	
							}
							else {
								return json_encode(['status' => 'error', 'errorMsg' => 'no_permission']);
							}
						} else {
							return json_encode(['status' => 'error', 'errorMsg' => 'not_id']);
						}
					} else {
						return json_encode(['status' => 'error', 'errorMsg' => 'not_id']);
					}
				}
				else {
					$id_devices_devices = $this->db->get($table_modbus_registers, ['id_devices_devices'], ['id_modbus_registers' => $id_modbus_registers])['id_devices_devices'];
					
					if ($this->db->has(
						$table_device_roles,
						['AND' => ['id_devices_devices' => $id_devices_devices, 'id_users' => $id_users, 'id_roles_roles' => 1]]
					)) {
						$dataToBase = [
							'physical_address' => $physical_address,
							'channel_name' => $channel_name,
							'id_modbus_functions_modbus_functions' => $id_modbus_functions_modbus_functions,
							'formula' => $formula,
							'round' => $round
						];
						$this->db->update(
							$table_modbus_registers,
							$dataToBase,
							['id_modbus_registers' => $id_modbus_registers]
						);
						return json_encode(['status' => 'successful']);
					}
					else {
						return json_encode(['status' => 'error', 'errorMsg' => 'no_permission']);
					}
				}
			}
			else {
				return json_encode(['status' => 'error', 'errorMsg' => 'no_data']);
			}
		}
		else {
			return json_encode(['status' => 'error', 'errorMsg' => 'no_permission']);
		}
	}
}
