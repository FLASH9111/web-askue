<?php 
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!" );
}
$this->langList = array(
'Password does not match the confirm password!' => 'Пароли не совпадают!',
'Permission management' => 'Управление доступом',
'Users' => 'Пользователи',
'Login' => 'Логин',
'Access' => 'Доступ',
'New Password' => 'Новый пароль',
'Repeat Password' => 'Повторите пароль',
'Secret word' => 'Секретное слово',
'Edit user' => 'Редактирование пользователя',
'Add user' => 'Создание пользователя',
'Edit' => 'Изменить',
'View and edit users' => 'Просмотр и редактирование пользователей',
'Perhaps the login is incorrect.' => 'Возможно, логин некорректен.'
);
?>