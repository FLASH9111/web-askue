<?php if (! defined ( "READFILE" ))
{
    exit ( "Error! Hacking attempt!" );
}

$this->paramList = [ 
    'name' => 'Модуль работы с регистрами',
    'active' => 'enable',
    'pages' => ['admin' => ['getregisters', 'delregisters'], 'index' => ['getmodbusfunctions', 'geteditregister', 'sendregister']],
    'priority' => '2'
];
?>
