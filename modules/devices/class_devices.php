<?php
/*
*****************************************************
 https://gitlab.com/FLASH9111
=====================================================
 Gorshkov Oleg
=====================================================
 Copyright (c) 2020
=====================================================
 Файл: modules/devices/class_devices.php
=====================================================
 Модуль управления устройствами
*****************************************************
*/
if (!defined('READFILE')) {
	exit("Error! Hacking attempt!");
}
class devices extends controller
{
	protected $config;
	public $script;

	protected $maxElem = 10;
	public function __construct()
	{
		parent::__construct(func_get_args());
		$this->config = new config;
		if (isset($_POST['postJson']) && !empty($_POST['postJson']) && clean_var($_POST['postJson']) == 'yes') {
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'getdevices') {
				header('Content-type: application/json');
				$page = 1;
				if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
					$page = (int) ($this->args[0][1][1]);
				}
				echo  $this->getDevices($page);
			}
			if ((isset($this->args[0][1][1]) && $this->args[0][1][0] == 'getobject' && is_numeric($this->args[0][1][1]) != false) || (isset($this->args[0][1][1]) && $this->args[0][1][0] == 'geteditobject' && is_numeric($this->args[0][1][1]) != false)) {
				if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
					$id = (int) ($this->args[0][1][1]);
					header('Content-type: application/json');
					echo  $this->getObject($id);
				}
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'deldevices') {
				header('Content-type: application/json');
				echo  $this->delDevices();
			}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendobject') {
				$id = 0;
				if (isset($this->args[0][1][1]) && trim($this->args[0][1][1]) != '') {
					$id = (int) ($this->args[0][1][1]);
				}
				header('Content-type: application/json');
				echo  $this->sendObject($id);
				}
			if (isset($this->args[0][1][0]) && $this->args[0][1][0] == 'sendprice') {
				header('Content-type: application/json');
				echo  $this->sendPrice();
			}


			exit;
		} else not_page();
		//exit("Error! Hacking attempt!");
	}
//eyJrIjoiUDY5c25TaFJUcFRrUHRsNTBwTTlOdUxqNkpmR1B1V08iLCJuIjoid2ViLWludGVyZmFjZSIsImlkIjoxfQ==
	protected function getDevices($page)
	{
		if ($_SESSION['user']['id'] != null) {
			$table_devices = 'devices';
			$begin = $page * $this->maxElem - $this->maxElem;
			if (isset($_POST['searchFunction']) && clean_var($_POST['searchFunction']) != '') {
				if (isset($_POST['searchNameDevice']) && clean_var($_POST['searchNameDevice']) != '') {
					$_SESSION['searchDevices']['devices.device_name'] = clean_var($_POST['searchNameDevice']);
				} else {
					unset($_SESSION['searchDevices']['devices.device_name']);
				}
				if (isset($_POST['searchStatusDevice']) && clean_var($_POST['searchStatusDevice']) != '') {
					$_SESSION['searchDevices']['devices.status'] = clean_var($_POST['searchStatusDevice']);
				} else {
					unset($_SESSION['searchDevices']['devices.status']);
				}
			}
			$idUser = $_SESSION['user']['id'];
			$whereDevices = ['device_roles.id_users' => $idUser];
			if(isset($_SESSION['searchDevices'])) {
			foreach ($_SESSION['searchDevices'] as $key => $value) {
				if (isset($value) && $value != '') {
					$whereDevices[$key] =  $value;
				}
			}}
			//print_r($whereOrders);
			$join = [
				'[>]objects' => ['devices.id_objects_objects' => 'id_objects'],
				'[>]device_roles' => ['devices.id_devices' => 'id_devices_devices']
			];
			$column = ['objects.object_name', 'devices.status', 'devices.device_name', 'devices.id_devices'];
			$where = ['AND' => $whereDevices, 'LIMIT' => [$begin, $this->maxElem], 'ORDER' => ['devices.id_devices' => 'DESC']];
			$post = $this->db->select($table_devices, $join, $column, $where);
			$sql = $this->db->last();

			$count = $this->db->count($table_devices, $join, '*', ['AND' => $whereDevices]);
			if (isset($_SESSION['searchDevices'])) {
				$searchDevices = $_SESSION['searchDevices'];
			}
			else {
				$searchDevices = '';
			}
			$resp = [
				'list' => $post, 'count' => $count, 'countPage' => $this->maxElem,
				'searchDevices' => $searchDevices, //'sql' => "$sql"
			];
			//print_r($_POST);
			return json_encode($resp);
		}
		return;
	}
	protected function getObject($id_objects)
	{
		if ($_SESSION['user']['id'] != null) {
			if ($id_objects != null) {
				$table_objects = 'objects';
				$join = [
					'[>]regions' => ['objects.id_regions_regions' => 'id_regions'],
					'[>]countries' => ['objects.id_countries_countries' => 'id_countries'],
					'[>]object_roles' => ['objects.id_objects' => 'id_objects_objects']
				];
				$column = ['objects.id_objects', 'objects.id_countries_countries', 'objects.id_regions_regions', 'count_devices'=>Medoo::raw('(SELECT COUNT(id_devices) FROM devices WHERE id_objects_objects = <id_objects>)'), 
				'objects.object_name', 'regions.region', 'countries.country', 'objects.locality', 'objects.street', 'objects.house', 'objects.building', 'objects.flat', 'objects.remark', 'objects.additional', 'object_roles.id_roles_roles'];
				$idUser = $_SESSION['user']['id'];
				$whereObject = ['AND' => ['object_roles.id_users' => $idUser, 'objects.id_objects' => $id_objects]];
				$objectData = $this->db->get($table_objects, $join, $column, $whereObject);
				return json_encode(['objectData' => $objectData]);
			}
			return;
		}
	}
	protected function delDevices()
	{
		if (isset($_POST['id_pos']) && $_POST['id_pos'] != '') {
			$idArr = explode(',', $_POST['id_pos']);
			foreach ($idArr as $id) {	
				$table_devices = 'devices';
				$table_modbustcp_parameters = 'modbustcp_parameters';
				$table_modbus_registers = 'modbus_registers';
				$table_device_roles = 'device_roles';
				$table_object_roles = 'object_roles';
				$where = ['id_devices' => $id];
				$whereDevice = ['id_devices_devices' => $id];
				$this->db->delete($table_modbustcp_parameters, $whereDevice);
				$this->db->delete($table_modbus_registers, $whereDevice);
				$this->db->delete($table_device_roles, $whereDevice);
				$this->db->delete($table_devices, $where);
			}
			$ans = ['status' => 'ok'];
			return json_encode($ans);
		}
		return;
	}

	protected function sendObject($id_objects)
	{
		if ($_SESSION['user']['id'] != null) {
			$table_objects = 'objects';
			$table_object_roles = 'object_roles';
			$id_users = (int) $_SESSION['user']['id'];
			if (isset($_POST['editobject']) && $_POST['editobject'] != '') {
				$editObjectDecode = json_decode($_POST['editobject'], true);
			}
			$editObject = [];
			foreach ($editObjectDecode as $key => $value) {
				if (clean_var($value) != "" && clean_var($value) != '0')
					$editObject[$key] = clean_var($value);
			}
			if (isset($editObject['object_name']) && $editObject['object_name'] != ''){
				if ($id_objects != 0) {
					if ($this->db->has($table_objects, ['id_objects' => $id_objects])) {
						if ($this->db->has($table_object_roles, 
							['AND' => ['id_objects_objects' => $id_objects, 'id_users' => $id_users, 'id_roles_roles' => 1]])) 
						{
							$this->db->update(
								$table_objects,
								$editObject,
								['id_objects' => $id_objects]
							);
							$ans = ['ans' => 'succesful'];
						}
					}
					else {
						$ans = ['ans' => 'not_id'];
					}
				}
				else {
					$this->db->insert(
						$table_objects,
						$editObject,
						['id_objects' => $id_objects]
					);
					$id_objects = $this->db->id();
					$this->db->insert(
						$table_object_roles,
						['id_users' => $id_users, 
						'id_objects_objects' => $id_objects,
						'id_roles_roles' => 1
						]
					);
					$ans = ['ans' => 'succesful'];
				}
			}
			else {
				$ans = ['ans' => 'not_name'];
			}
			return json_encode($ans);
		}
		else {
			$ans = ['ans' => 'no_permission'];
			return json_encode($ans);
		}
	}
	protected function sendPrice()
	{
		$table_orders = 'orders';
		foreach ($_POST as $key => $value) {
			$_POST[$key] = clean_var($value);
		}
		if (isset($_POST['id_orders']) && $_POST['id_orders'] != '' && isset($_POST['total_price']) && $_POST['total_price'] != '') {
			$id = $_POST['id_orders'];
			$where = ['id_orders' => $id];
			if ($this->db->has($table_orders, $where)) {
				$res = $this->db->update(
					$table_orders,
					[
						"total_price" => $_POST["total_price"]
					],
					$where
				);
				$ans = ['ans' => 'ok'];
				return json_encode($ans);
			}
		}
		$ans = ['ans' => 'error'];
		return json_encode($ans);
	}
}
