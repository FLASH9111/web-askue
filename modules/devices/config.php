<?php if (! defined ( "READFILE" ))
{
    exit ( "Error! Hacking attempt!" );
}
$this->paramList = array(
'name' => 'Модуль отслеживания объектов',
'active' => 'enable',
'pages' => 'admin: getdevices, senddevice, deldevices, getdevice, geteditdevice',
'priority' => '2'
);
?>