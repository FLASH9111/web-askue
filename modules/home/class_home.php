<?php
/*
*****************************************************
 CMS SVCE 2.0 
=====================================================
 http://svce.ru/
=====================================================
 Gorshkov O.V.
=====================================================
 Copyright (c) 2013-2016 Zed Group
=====================================================
 Файл: modules/all_modules/class_home.php
=====================================================
 Вывод главной страницы
*****************************************************
*/
if (! defined ( 'READFILE' ))
{
    exit ( "Error! Hacking attempt!" );
}
class home extends controller {
  	protected $config;
   	public function __construct() {
  		parent::__construct(func_get_args());
   		$this->config = new config;
   		$this->theme();
   	} 
   	protected function theme() {
    	
		$table_home = 'home';
		$column = ['full_story'];
		$post = $this->db->get($table_home, $column);
  		if($post == "")  return false;
  		if ($this->tpl->load_template('home.tpl')) {
  			$this->tpl->lng(get_class($this));
			$this->tpl->set('[full_story]', $post['full_story']);
			$this->tpl->set('[home_page]', $this->getLang('Home page'));
			$this->tpl->compile('content');
			$this->tpl->set('{{content}}', $post['full_story']);
		}
		else {
			$this->tpl->set('{{content}}', $this->tpl->result['content']);
		}
		return true;
   }
}
?>