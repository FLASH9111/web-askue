<?php if (! defined ( "READFILE" ))
{
    exit ( "Error! Hacking attempt!" );
}
$this->paramList = array('name' => 'Home page',
'description' => 'Home page output on website',
'active' => 'disable',
'pages' => 'index: _start',
'category' => 'home',
'priority' => '2'
);?>