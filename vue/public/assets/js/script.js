// start app
Vue.component('modal', {
  template: '#modal-template'
})
Vue.component('line-chart', {
  extends: VueChartJs.Line,
  mounted() {
    this.renderChart({
      labels: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
      datasets: [{
        label: 'Суммарное потребление электроэнергии',
        data: [12, 15, 3, 5, 2, 3, 8],
        backgroundColor: [
          'rgba(0, 98, 204, 0.2)'
        ],
        borderColor: [

          'rgba(0, 98, 204, 1)'
        ],
        borderWidth: 1
      }]
    }, {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      legend: {
        display: false
      },
      responsive: true,
      maintainAspectRatio: false,
      height: 50
    })
  }
})
Vue.component('bar-chart', {
  extends: VueChartJs.Bar,
  data: function () {
    return {
      datacollection: {
        labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        datasets: [
          {
            label: 'Data One',
            backgroundColor: '#0d69b1',
            pointBackgroundColor: 'white',
            borderWidth: 1,
            pointBorderColor: '#0d69b1',
            data: [40, 20, 30, 50, 90, 10, 20, 40, 50, 70, 90, 100]
          }
        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            },
            gridLines: {
              display: true
            }
          }],
          xAxes: [{
            ticks: {
              beginAtZero: true
            },
            gridLines: {
              display: false
            }
          }]
        },
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          mode: 'single',
          callbacks: {
            label: function (tooltipItems, data) {
              return tooltipItems.yLabel + ' кВт';
            }
          }
        },
        responsive: true,
        maintainAspectRatio: false,
        height: 100
      }
    }
  },
  mounted() {
    // this.chartData is created in the mixin
    this.renderChart(this.datacollection, this.options)
  }
})
const messages = {
  en: {
    lk: {
      Cancel: 'Cancel',
      Name: 'Name',
      Password: 'Password',
      Repeat_password: 'Repeat password',
      Next: 'Next',
      Prev: 'Previous',
      All_fields_must_be_filled: 'All fields must be filled!',
      Phone: 'Phone',
      Download: 'Download',
      Result: 'Result',
      Confirmation: 'Confirmation',
      No: 'No',
      Yes: 'Yes',
      Information: "Information",
      Web_version_created_by: "Dash panel created by",
      seosp_ru: "seosp.ru",

      Search_by_all_data: "Search by all data",

      Name_of_order: "Order-1-02-2020-20-45-00",
      Date_of_order: "10.02.2020",
      Client_of_order: "Jonh Smith",
      INN_KPP_OGRN_ADDRESS: "INN/KPP",
      Clients: "Clients",
      Search: "Search",
      Select_type_of_search_and_click_search: 'Select type of search and click "Search"',
      Serach_order: "Serach of orders",
      Orders: "Orders",
      Date_of_formation: "Date of formation",

      Total_price: "Total price",
      Apply: "Apply",
      Change_status: "Change status",
      Delete_selected: "Delete selected",
      Search_option: "Search option",

      FIO: "Jonh Smith",
      Personal_account: 'Personal account',
      Feedback: "Feedback",
      Logout: "Logout",
      Submit: "Submit",
      Order_information: "Order information",
      Order_data: "Order data",
      Customer_information: "Customer information",
      Check_payment: "Check payment",
      Material: "Material",
      Technology: "Technology",
      Quantity: "Quantity",
      Delivery: "Delivery",
      Contact_person: "Contact person",
      Legal_address: "Legal_address",
      Inn_kpp: "INN/KPP",
      OGRN: "OGRN",
      Click_to_select: "Click to select",
      Clear: 'Clear',
      Refresh: "Refresh",
      OGRN: "OGRN",
      Search_by_OGRN: "Search by OGRN",
      Client_name: "Client name",
      Client_information: "Client information",
      Email_address: "Email address",
      New_password: "New password",
      Data_editing: "Data editing",
      In_this_section_you_can_manage_your_personal_data: 'In this section you can manage your personal data',
      Dash_Panel: "Dash panel",

      Objects: "Objects",
      Add: "Add",
      Search_by_street: "Search by street",
      Search_by_region: "Search by region",
      Search_by_status: "Search by status",
      Search_by_address: "Search by address",
      Lomonosov: "Lomonosov",
      Moscow: "Moscow",
      House_1: "House 1",
      Search_by_name: "Search by name",
      Title: "Title",
      Address: "Address",
      Number_of_devices: "Number of devices",
      Edit: "Edit",
      Name_of_object: "Name of object",
      Remark: "Remark",
      Country: "Country",
      Region: "Region",
      Locality: "Locality",
      Street: "Street",
      House: "House",
      Building: "Building",
      Flat: "Flat",
      Office: "Office",
      Additional: "Additional",
      Save: "Save",
      Vyatskoe: "Vyatskoe",
      Add_object: "Add object",
      Edit_object: "Edit object",
      Devices: "Devices",
      Please_enter_valid_name: "Please enter valid name",
      Invalid_Id: "Invalid Id",
      No_permission: "No permission",
      Access_control: "Access control",
      Main_information: "Main information",
      Create_report: "Create_report",
      Month: "Month",
      Today: "Today",
      Year: "Year",
      Type_of_resource: "Type of resource",
      Hot_water: "Hot water",
      Cold_water: "Cold water",
      Electricity: "Electricity",
      Thermal_energy: "Thermal energy",
      General: "General",
      General_information: "General information",
      Device_1: "Device 1",
      Enabled: "Enabled",
      Disabled: "Disabled",
      Create_report: "Create report",
      Object: "Object",
      Status: "Status",
      Modbus_Registers: "Modbus Registers",
      Physical_address: "Physical address",
      Temperature_1: "Temperature_1",
      Function: "Function",
      Channel_name: "Channel name",
      Formula: "Formula",
      Rounding: "Rounding",
      Add_register: "Add register",
      Edit_register: "Edit register",
      Please_enter_valid_physical_address: "Please enter valid physical address",
      Please_enter_valid_modbus_functions: "Please enter modbus functions"

    }
  },
  ru: {
    lk: {
      Cancel: 'Отмена',
      Name: 'Имя',
      Password: 'Пароль',
      Repeat_password: 'Пароль ещё раз',
      Next: 'Далее',
      Prev: 'Назад',
      All_fields_must_be_filled: 'Все поля должны быть обязательно заполнены!',
      Phone: 'Телефон',
      Company: 'Компания',
      This_field_is_required: 'Это поле обязательно к заполнению',
      Please_enter_valid_email: 'Пожалуйста, укажите коректный Email',
      Please_enter_valid_phone_number: 'Пожалуйста, укажите коректный телефон',
      Please_enter_your_name: 'Пожалуйста, укажите имя',
      Please_enter_password: 'Пожалуйста, укажите пароль',
      Passwords_do_not_match: 'Пароли не совпадают',
      Personal_account: 'Личный кабинет',
      Data_editing: 'Изменение данных',
      The_cost_of_one_file_is: 'Анализ 1 файла стоит',
      Submit: 'Отправить',
      Select_file: 'Выбрать файл',
      Balance: 'Баланс',
      Email_address: 'Адрес Email',
      New_password: 'Новый пароль',
      Logout: 'Выйти',
      Feedback: 'Обратная связь',
      Price_for_services: 'Цены на услуги',
      Date: 'Дата',
      Status: 'Статус',
      Comment: 'Комментарий',
      Name_of_file: 'Имя файла',
      Previous_transactions: 'Предыдущие транзакции',
      Amount: 'Сумма',
      Comment: 'Комментарий',
      Email_after: 'Ваш email никогда не попадет в руки к третьим лицам',
      Phone_after: 'Ваш телефон никогда не попадет в руки к третьим лицам',
      Example: 'Пример',
      Text: 'Текст',
      thisLang: 'Русский',
      Please_enter_text: 'Пожалуйста, введите текст',
      Please_enter_topic_of_massage: 'Пожалуйста, введите тему обращения',
      Operation_completed_successfully: 'Операция выполнена успешно!',
      Close_the_window: 'Закрыть окно',
      Error: 'Ошибка',
      Download: 'Скачать',
      Result: 'Результат',
      Confirmation: 'Подтверждение',
      No: 'Нет',
      Yes: 'Да',
      Create: 'Создать',
      Information: "Информация",
      Web_version_created_by: "Панель управления разработана в",
      seosp_ru: "СДЕЛАЕМСАЙТ.РФ",

      Name_of_order: "Заказ-1-02-2020-20-45-00",
      Date_of_order: "10.02.2020",
      Client_of_order: "Сидоров Иван Петрович",
      INN_KPP_OGRN_ADDRESS: "ИНН/КПП",
      Search: "Поиск",
      Select_type_of_search_and_click_search: 'Выберите условия поиска и нажмите "Поиск"',
      Serach_order: "Поиск заказов",
      Orders: "Заказы",
      Date_of_formation: "Дата формирования",

      Total_price: "Стоимость",
      Clients: "Клиенты",
      Apply: "Применить",
      Change_status: "Сменить статус",
      Delete_selected: "Удалить выбранное",
      Search_option: "Параметры поиска",

      FIO: "Сидоров Иван Петрович",
      Order_information: "Информация о заказе",
      Order_data: "Данные заказа",
      Customer_information: "Данные о заказчике",
      Check_payment: "Проверить оплату",
      Material: "Материал",
      Technology: "Технология",
      Quantity: "Количество",
      Delivery: "Доставка",
      Contact_person: "Контактное лицо",
      Legal_address: "Юридический адрес",
      Inn_kpp: "ИНН/КПП",
      OGRN: "ОГРН",
      Click_to_select: "Нажмите, чтобы выбрать",
      Clear: "Сбросить",
      Refresh: "Обновить",
      OGRN: "ОГРН",
      Search_by_OGRN: "По ОГРН",
      Client_name: "Имя клиента",
      Client_information: "Информация о клиенте",
      In_this_section_you_can_manage_your_personal_data: 'В данном разделе Вы можете изменить основные данные',
      Dash_Panel: "Панель управления",

      Objects: "Объекты",
      Add: "Добавить",
      Search_by_status: "По статусу",
      Search_by_region: "По региону",
      Search_by_street: "По улице",
      Search_by_address: "По адресу",
      Lomonosov: "Ломоносова",
      Moscow: "Москва",
      Search_by_name: "По названию",
      House_1: "Дом №1",
      Title: "Название",
      Address: "Адрес",
      Number_of_devices: "Количество устройств",
      Edit: "Редактировать",
      Name_of_object: "Имя объекта",
      Remark: "Примечание",
      Country: "Страна",
      Region: "Регион",
      Locality: "Населенный пункт",
      Street: "Улица",
      House: "Дом",
      Building: "Корпус",
      Flat: "Квартира",
      Office: "Офис",
      Additional: "Доп. информация",
      Save: "Сохранить",
      Vyatskoe: "село Вятское",
      Add_object: "Добавить объект",
      Edit_object: "Редактировать объект",
      Devices: "Устройства",
      Please_enter_valid_name: "Пожалуйста, укажите корректное название",
      Invalid_Id: "Неверный идентификатор",
      No_permission: "Нет прав",
      Access_control: "Управление доступом",
      Main_information: "Основная информация",
      Create_report: "Создать отчёт",
      Month: "Месяц",
      Year: "Год",
      Today: "Сегодня",
      Type_of_resource: "Тип ресурса",
      Hot_water: "Горячая вода",
      Cold_water: "Холодная вода",
      Electricity: "Электроэнергия",
      Thermal_energy: "Тепловая энергия",
      General: "Общее",
      General_information: "Основная информация",
      Device_1: "Теплосчетчик 1",
      Enabled: "Включен",
      Disabled: "Выключен",
      Object: "Объект",
      Modbus_Registers: "Modbus регистры",
      Physical_address: "Адрес (физический)",
      Temperature_1: "Temperature_1",
      Function: "Функция Modbus",
      Channel_name: "Имя канала",
      Formula: "Формула",
      Rounding: "Округление",
      Add_register: "Добавление регистра",
      Edit_register: "Редактирование регистра",
      Please_enter_valid_physical_address: "Пожалуйста, введите корректный физический адрес",
      Please_enter_valid_modbus_functions: "Пожалуйста укажите modbus функцию"
    }
  }
}

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en', // set locale
  fallbackLocale: 'en',
  messages, // set locale messages
})

const routes = [

]

const router = new VueRouter({
  routes,
  mode: 'history'

})
var homeApp = new Vue({
  el: '#lkApp',
  router,
  data: {
    currentPage: 'index',
    lkName: 'User Name',
    price: '1',
    errors: '',
    mobMenuOn: 0,
    otherLang: 'Русский',
    mailLk: 'test',
    inMailLk: '12',
    inPhoneLk: '',
    inNewPasswordLk: '',
    inRepeatPasswordLk: '',
    nameForm: '',
    inTopicFeedback: '',
    inTextFeedback: '',
    comment: '',
    historyModels: [],
    commentModal: '',
    loadModal: false,
    idComment: '',
    submitVis: true,
    repeatVis: false,
    dataFile: '',
    submitOrdersVis: false,
    deleteObjectVis: false,
    deleteDeviceVis: false,
    deleteRegisterVis: false,
    deleteClientVis: false,
    nextNavObjects: false,
    prevNavObjects: false,
    nextNavDevices: false,
    prevNavDevices: false,
    nextNavRegisters: false,
    prevNavRegisters: false,
    nextNavClients: false,
    prevNavClients: false,
    historyFilesFlag: false,
    countHistoryFiles: 0,
    numPage: 1,
    prevObjectLink: '',
    nextObjectLink: '',
    prevDeviceLink: '',
    nextDeviceLink: '',
    prevRegisterLink: '',
    nextRegisterLink: '',
    prevClientLink: '',
    nextClientLink: '',
    confirmModal: false,
    idDeleteFile: 0,
    checkedObjects: [],
    checkedDevices: [],
    checkedRegisters: [],
    checkedClients: [],
    typeComment: '',
    typeDelete: '',
    idObjects: 0,
    idRegister: 0,
    idDevices: 0,
    oneLoad: false,

    clientInfo: {
      address: "none", legal_name: "none", fio: "none", phone: "none", legal_address: "none",
      inn_kpp: "none", ogrn: "none"
    },

    loader: false,
    allObjects: {},
    allDevices: {},
    allRegisters: {},
    allRegions: {},
    allCountries: {},
    allModbusFunctions: {},
    searchNameObject: '',
    searchRegionObject: '',
    searchStreetObject: '',
    searchNameDevice: '',
    searchStatusDevice: '',
    searchNameRegisters: '',
    searchAddressRegisters: '',

    searchPhoneClient: '',
    searchEmailClient: '',
    searchCompanyClient: '',
    searchOGRNClient: '',
    send: new FormData(),
    submitStatusFullOrderVis: false,
    changeStatusOrder: [],
    submitPriceFullOrderVis: false,
    changePriceOrder: [],
    editObjectTitle: 'Edit',
    editRegisterTitle: 'Edit register',
    typeResource: "general",
    editobject: {
      object_name: "", remark: "", locality: "", street: "", house: "", building: "",
      flat: "", office: "", additional: "", id_regions_regions: 0, id_countries_countries: 0
    },
    objectData: {
      object_name: "", remark: "", locality: "", street: "", house: "", building: "",
      flat: "", office: "", additional: "", country: "", region: ""
    },
    modbus_registers: {
      channel_name: "", physical_address: "", id_modbus_functions_modbus_functions: 0, formula: "", round: "", id_modbus_registers: 0
    },
    chartdata: {
      labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      datasets: [
        {
          label: 'Коммиты на GitHub',
          backgroundColor: '#f87979',
          data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11]
        }
      ]
    }
  },
  created: function () {
    var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage).substr(0, 2).toLowerCase();
    if (lang === 'ru') {
      this.$i18n.locale = 'ru';
      this.otherLang = 'English'
    };
    this.switchURL();

    //this.loader = false
  },
  watch: {
    '$route'(to, from) {
      this.switchURL();
    }
  },
  methods: {
    switchURL() {
      let sections = this.$router.currentRoute.path.split('/');
      if (sections[2]) {
        var current = sections[2];
      }
      else {
        var current = '_start';
      }
      switch (current) {
        case '_start':
          this.pageIndex();
          break;
        case 'addobject':
          this.pageAddObject();
          break;
        case 'editobject':
          this.pageEditObject();
          break;
        case 'addregister':
          this.pageAddRegister();
          break;
        case 'editregister':
          this.pageEditRegister();
          break;
        case 'object':
          this.pageObject();
          break;
        case 'devices':
          this.pageDevices();
          break;
        case 'registers':
          this.pageRegisters();
          break;
        case 'clients':
          this.pageClients();
          break;

        case 'client':
          this.pageClient();
          break;
        case 'lk':
          this.pageLk();
          break;
        case 'balance':
          this.pageBalance();
          break;
        case 'prices':
          this.pagePrices();
          break;
        case 'feedback':
          this.pageFeedback();
          break;
        default:
          this.pageIndex();
      }
    },
    postJson(postUrl) {
      if (!postUrl) {
        alert('JSON error 1!');
        return;
      }
      this.send.append('postJson', 'yes');
      let jsonResponse = '';
      let request = new XMLHttpRequest();
      request.open('POST', postUrl);
      request.onreadystatechange = function () {
        if (this.readyState === 4) {
          if (this.status >= 200 && this.status < 400) {
            console.log(request.responseText);
            try {
              jsonResponse = JSON.parse(request.responseText);
              console.log(jsonResponse);
              selectPrint(postUrl, jsonResponse);
              if (jsonResponse && jsonResponse != '')
                homeApp.loader = false;
              return this.jsonResponse;
            } catch (error) {
              return;
            }

          } else {
            //alert('JSON error 2!');
            return;
          }
        }
      };
      function selectPrint(postUrl, jsonResponse) {
        switch (postUrl.split('/')[2]) {
          case 'getclients':
            homeApp.printClients(jsonResponse);
            break;
          case 'getlk':
            homeApp.printLk(jsonResponse);
            break;
          case 'getclient':
            homeApp.printClient(jsonResponse);
            break;
          case 'getdevices':
            homeApp.printDevices(jsonResponse);
            break;
          case 'getregisters':
            homeApp.printRegisters(jsonResponse);
            break;
          case 'getobjects':
            homeApp.printObjects(jsonResponse);
            break;
          default:
        }
      };

      request.send(this.send);
    },
    getJson(postUrl) {
      if (!postUrl) {
        alert('JSON error 1!');
        return;
      }
      let request = new XMLHttpRequest();

      request.open('GET', postUrl);

      let jsonResponse = '';
      request.onreadystatechange = function () {
        if (this.readyState === 4) {
          if (this.status >= 200 && this.status < 400) {
            console.log(request.responseText);
            try {
              jsonResponse = JSON.parse(request.responseText);
              if (jsonResponse && jsonResponse != '' && !jsonResponse.errorCode) {
                selectPrint(postUrl, jsonResponse);
                homeApp.loader = false;
              }
              else {
                return;
              }
              return;
            } catch (error) {
              return;
            }
          } else {
            return;
          }

        }
      }
      function selectPrint(postUrl, jsonResponse) {
        switch (postUrl.split('/')[1]) {
          case 'getmodbusfunctions':
            //if (jsonResponse)
              homeApp.allModbusFunctions = jsonResponse;
            break;
          case 'getregions':
            if (jsonResponse)
              homeApp.allRegions = jsonResponse;
            break;
          case 'getcountries':
            if (jsonResponse)
              homeApp.allCountries = jsonResponse;
            break;
          case 'geteditobject':
            if (jsonResponse)
              homeApp.editobject = jsonResponse.objectData;
            break;
          case 'getobject':
            if (jsonResponse)
              homeApp.editobject = jsonResponse.objectData;            
            break;
          case 'geteditregister':
            if (jsonResponse)
              homeApp.modbus_registers = jsonResponse.modbus_registers;
              break;
          default:
        }
      };
      request.send(); // (1)
    },
    printLk(data) {
      if (data != 'undefenit' && data != '') {
        this.inMailLk = data.email;
        this.inPhoneLk = data.phone;
      }
    },
    printObject(data) {
      if (data != 'undefenit' && data != '') {
        this.objectData = data.objectData;
      }
    },
    printClient(data) {
      if (data != 'undefenit' && data != '') {
        this.clientInfo = data.client;
      }
    },

    printObjects(data) {
      if (data && data != 'undefenit' && data != '' && this.allObjects != data.list) {
        this.allObjects = data.list;
        this.allRegions = data.regions;
        let allPages = 1;
        try {
          allPages = data.count / data.countPage;
        } catch (error) {
          console.log(error);
        }
        if (this.numPage < allPages) {
          this.nextNavObjects = true;
          this.nextObjectLink = Number(this.numPage) + 1;
        }
        else {
          this.nextNavObjects = false;
        }
        if (this.numPage > 1) {
          this.prevNavObjects = true;
          this.prevObjectLink = Number(this.numPage) - 1;
        }
        else {
          this.prevNavObjects = false;
        }
        if (!this.oneLoad) {
          if (data.searchObjects && data.searchObjects['objects.object_name'] !== '' && data.searchObjects['objects.object_name'] != 'undefined' && data.searchObjects['objects.object_name'] != null) {
            this.oneLoad = true;
            this.searchNameObject = data.searchObjects['objects.object_name'];
          }
          if (data.searchObjects && data.searchObjects['objects.id_regions_regions'] !== '' && data.searchObjects['objects.id_regions_regions'] != 'undefined' && data.searchObjects['objects.id_regions_regions'] != null) {
            this.oneLoad = true;
            this.searchRegionObject = data.searchObjects['objects.id_regions_regions'];
          }
          if (data.searchObjects && data.searchObjects['objects.street'] !== '' && data.searchObjects['objects.street'] != 'undefined' && data.searchObjects['objects.street'] != null) {
            this.oneLoad = true;
            this.searchStreetObject = data.searchObjects['objects.street'];
          }
        }
        /*this.searchDateOrder = '';
         this.searchClientOrder = '';
         this.searchCompanyOrder ='';
         this.searchStatusOrder ='';*/
        //console.log('count3:' + this.countHistoryFiles);

        return true;
      }
      else if (data == 'undefenit' || data == '') {

      }
    },
    printDevices(data) {
      if (data && data != 'undefenit' && data != '' && this.allDevices != data.list) {
        this.allDevices = data.list;
        let allPages = 1;
        try {
          allPages = data.count / data.countPage;
        } catch (error) {
          console.log(error);
        }
        if (this.numPage < allPages) {
          this.nextNavDevices = true;
          this.nextDeviceLink = Number(this.numPage) + 1;
        }
        else {
          this.nextNavDevices = false;
        }
        if (this.numPage > 1) {
          this.prevNavDevices = true;
          this.prevDeviceLink = Number(this.numPage) - 1;
        }
        else {
          this.prevNavDevices = false;
        }
        if (!this.oneLoad) {
          if (data.searchDevices && data.searchDevices['devices.device_name'] !== '' && data.searchDevices['devices.device_name'] != 'undefined' && data.searchDevices['devices.device_name'] != null) {
            this.oneLoad = true;
            this.searchNameDevice = data.searchDevices['devices.device_name'];
          }
          if (data.searchDevices && data.searchDevices['devices.status'] !== '' && data.searchDevices['devices.status'] != 'undefined' && data.searchDevices['devices.status'] != null) {
            this.oneLoad = true;
            this.searchStatusDevice = data.searchDevices['devices.status'];
          }
        }
        /*this.searchDateOrder = '';
         this.searchClientOrder = '';
         this.searchCompanyOrder ='';
         this.searchStatusOrder ='';*/
        //console.log('count3:' + this.countHistoryFiles);

        return true;
      }
      else if (data == 'undefenit' || data == '') {

      }
    },
    printRegisters(data) {
      if (data && data != 'undefenit' && data != '' && this.allRegisters != data.list) {
        this.allRegisters = data.list;
        let allPages = 1;
        try {
          allPages = data.count / data.countPage;
        } catch (error) {
          console.log(error);
        }
        if (this.numPage < allPages) {
          this.nextNavRegisters = true;
          this.nextRegisterLink = Number(this.numPage) + 1;
        }
        else {
          this.nextNavDevices = false;
        }
        if (this.numPage > 1) {
          this.prevNavRegisters = true;
          this.prevRegisterLink = Number(this.numPage) - 1;
        }
        else {
          this.prevNavRegisters = false;
        }
        if (!this.oneLoad) {
          if (data.searchRegisters && data.searchRegisters['modbus_registers.channel_name'] !== '' && data.searchRegisters['modbus_registers.channel_name'] != 'undefined' && data.searchRegisters['modbus_registers.channel_name'] != null) {
            this.oneLoad = true;
            this.searchNameRegisters = data.searchRegisters['modbus_registers.channel_name'];
          }
          if (data.searchRegisters && data.searchRegisters['modbus_registers.physical_address'] !== '' && data.searchRegisters['modbus_registers.physical_address'] != 'undefined' && data.searchRegisters['modbus_registers.physical_address'] != null) {
            this.oneLoad = true;
            this.searchAddressRegisters = data.searchRegisters['modbus_registers.physical_address'];
          }
        }
        return true;
      }
      else if (data == 'undefenit' || data == '') {

      }
    },
    printClients(data) {
      if (data && data != 'undefenit' && data != '' && this.allClients != data.list) {
        this.allClients = data.list;
        let allPages = 1;
        try {
          allPages = data.count / data.countPage;
        } catch (error) {
          console.log(error);
        }
        if (this.numPage < allPages) {
          this.nextNavClients = true;
          this.nextClientLink = Number(this.numPage) + 1;
        }
        else {
          this.nextNavClients = false;
        }
        if (this.numPage > 1) {
          this.prevNavClients = true;
          this.prevClientLink = Number(this.numPage) - 1;
        }
        else {
          this.prevNavClients = false;
        }
        if (!this.oneLoad) {
          if (data.searchClients && data.searchClients['customers.fio'] !== '' && data.searchClients['customers.fio'] != 'undefined' && data.searchClients['customers.fio'] != null) {
            this.oneLoad = true;
            this.searchFioClient = data.searchClients['customers.fio'];
          }
          if (data.searchClients && data.searchClients['customers.phone'] !== '' && data.searchClients['customers.phone'] != 'undefined' && data.searchClients['customers.phone'] != null) {
            this.oneLoad = true;
            this.searchPhoneClient = data.searchClients['customers.phone'];
          }
          if (data.searchClients && data.searchClients['users.email'] !== '' && data.searchClients['users.email'] != 'undefined' && data.searchClients['users.email'] != null) {
            this.oneLoad = true;
            this.searchEmailClient = data.searchClients['users.email'];
          }
          if (data.searchClients && data.searchClients['customers.inn_kpp'] !== '' && data.searchClients['customers.inn_kpp'] != 'undefined' && data.searchClients['customers.inn_kpp'] != null) {
            this.oneLoad = true;
            this.searchCompanyClient = data.searchClients['customers.inn_kpp'];
          }
          if (data.searchClients && data.searchClients['customers.ogrn'] !== '' && data.searchClients['customers.ogrn'] != 'undefined' && data.searchClients['customers.ogrn'] != null) {
            this.oneLoad = true;
            this.searchOGRNClient = data.searchClients['customers.ogrn'];
          }
        }
        return true;
      }
      else if (data == 'undefenit' || data == '') {

      }
    },
    pageIndex() {
      this.loader = true;
      this.numPage = 1;
      let sections = this.$router.currentRoute.path.split('/');
      let lastSection = sections.pop() || sections.pop();
      if (!lastSection.replace(/\s/g, '').length === 0 || !isNaN(lastSection)) {
        this.numPage = lastSection;
      }
      this.postJson('/panel/getobjects/' + this.numPage);
      /*var timerId = setInterval(function () {
        homeApp.postJson('/panel/getorders/' + homeApp.numPage);
      }, 120000);*/
      this.currentPage = 'index';
      this.errors = '';
      feather.replace();
      return;
    },
    pageDevices() {
      this.loader = true;
      this.currentPage = 'devices';
      this.numPage = 1;
      let sections = this.$router.currentRoute.path.split('/');
      if (sections[3] && (!sections[3].replace(/\s/g, '').length === 0 || !isNaN(sections[3])))
        this.numPage = sections[3];
      this.postJson('/panel/getdevices/' + this.numPage);
      var timerId = setInterval(function () {
        homeApp.postJson('/panel/getdevices/' + homeApp.numPage);
      }, 18000);
      this.errors = '';
      feather.replace();
      return;
    },
    pageRegisters() {
      this.loader = true;
      this.currentPage = 'registers';
      this.numPage = 1;
      let sections = this.$router.currentRoute.path.split('/');
      if (sections[3] && (!sections[3].replace(/\s/g, '').length === 0 || !isNaN(sections[3])))
        this.idObjects = sections[3];
      if (sections[4] && (!sections[4].replace(/\s/g, '').length === 0 || !isNaN(sections[4])))
        this.numPage = sections[4];
      this.postJson('/panel/getregisters/' + this.idObjects + '/' + this.numPage);
      var timerId = setInterval(function () {
        homeApp.postJson('/panel/getregisters/' + homeApp.idObjects + '/' + homeApp.numPage);
      }, 38000);
      this.errors = '';
      return;
    },
    pageObject() {
      this.loader = true;
      this.currentPage = 'object';
      let sections = this.$router.currentRoute.path.split('/');
      if (sections[3] && (!sections[3].replace(/\s/g, '').length === 0 || !isNaN(sections[3]))) {
        this.idObjects = sections[3];
      }
      this.getJson('/getobject/' + this.idObjects);
      this.errors = '';
      return;
    },
    pageClient() {
      this.currentPage = 'client';

      let sections = this.$router.currentRoute.path.split('/');
      if (sections[3] && (!sections[3].replace(/\s/g, '').length === 0 || !isNaN(sections[3]))) {
        this.idClient = sections[3];
      }
      this.postJson('/panel/getclient/' + this.idClient);
      this.errors = '';
      return;
    },
    pageLk() {
      this.postJson('/panel/getlk');
      this.currentPage = 'lk';
      this.inNewPasswordLk = '';
      this.inRepeatPasswordLk = '';
      this.errors = '';
      return;
    },
    pageAddRegister() {
      try {
        let sections = this.$router.currentRoute.path.split('/');
        if (sections[3] && (!sections[3].replace(/\s/g, '').length === 0 || !isNaN(sections[3]))) {
          this.idDevices = sections[3];
        }
        this.loader = true;
        this.currentPage = 'editregister';
        this.getJson('/getmodbusfunctions');
        this.errors = '';
        this.editRegisterTitle = this.$t("lk.Add_register");
        return;
      } catch (error) {
        console.log(error);
        return;
      }
    },
    pageEditRegister() {
      try {
        this.loader = true;
        this.currentPage = 'editregister';
        this.getJson('/getmodbusfunctions');
        let sections = this.$router.currentRoute.path.split('/');
        if (sections[3] && (!sections[3].replace(/\s/g, '').length === 0 || !isNaN(sections[3]))) {
          this.idRegister = sections[3];
        }
        this.loader = true;
        this.getJson('/geteditregister/' + this.idRegister);
        this.errors = '';
        this.editRegisterTitle = this.$t("lk.Edit_register");
        return;
      } catch (error) {
        console.log(error);
        return;
      }
    },
    pageAddObject() {
      this.loader = true;
      this.currentPage = 'editobject';
      this.editobject = {
        object_name: "", remark: "", locality: "", street: "", house: "", building: "",
        flat: "", office: "", additional: "", id_regions_regions: 0, id_countries_countries: 0, id_objects: 0
      };
      this.getJson('/getregions');
      this.loader = true;
      this.getJson('/getcountries');
      this.errors = '';
      this.editObjectTitle = this.$t("lk.Add_object");
      return;
    },
    pageEditObject() {
      this.loader = true;
      this.currentPage = 'editobject';
      let sections = this.$router.currentRoute.path.split('/');
      if (sections[3] && (!sections[3].replace(/\s/g, '').length === 0 || !isNaN(sections[3]))) {
        this.idObject = sections[3];
      }
      this.getJson('/geteditobject/' + this.idObject);
      this.getJson('/getregions');
      this.loader = true;
      this.getJson('/getcountries');
      this.errors = '';
      this.editObjectTitle = this.$t("lk.Edit_object");
      return;
    },
    submitRegister() {
      if (!this.modbus_registers.channel_name || this.modbus_registers.channel_name == "") {
        this.errors = this.$t("lk.Please_enter_valid_name");
        return;
      }
      if (!this.modbus_registers.physical_address || this.modbus_registers.physical_address == "") {
        this.errors = this.$t("lk.Please_enter_valid_physical_address");
        return;
      }
      if (!this.modbus_registers.id_modbus_functions_modbus_functions || this.modbus_registers.id_modbus_functions_modbus_functions == "") {
        this.errors = this.$t("lk.Please_enter_valid_modbus_functions");
        return;
      }
      if(this.idDevices && this.idDevices != 0)
        this.modbus_registers.id_devices_devices = this.idDevices;
      this.errors = '';
      let xhr = new XMLHttpRequest();
      xhr.open("POST", "/sendregister");
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status >= 200 && xhr.status < 400) {
            try {
              if (JSON.parse(xhr.responseText)['status'] == 'error') {
                let ans = JSON.parse(xhr.responseText)['errorMsg'];
                console.log("Ans" + xhr.responseText);
                switch (ans) {
                  case 'not_id':
                    homeApp.errors = homeApp.$t("lk.Invalid_Id");
                    break;
                  case 'not_name':
                    homeApp.errors = homeApp.$t("lk.Please_enter_valid_name");
                    break;
                  case 'not_physical_address':
                    homeApp.errors = homeApp.$t("lk.Please_enter_valid_physical_address");
                    break;
                  case 'not_modbus_functions':
                    homeApp.errors = homeApp.$t("lk.Please_enter_valid_modbus_functions");
                    break;
                  case 'no_permission':
                    homeApp.errors = homeApp.$t("lk.No_permission");
                    break;
                  default:
                    homeApp.errors = homeApp.$t("lk.Error");
                    break;
                }
              }
              else if (JSON.parse(xhr.responseText)['status'] == 'successful') {
                homeApp.errors = homeApp.$t("lk.Operation_completed_successfully");
                if (homeApp.modbus_registers.id_modbus_registers == 0) {
                  router.push('/panel/devices/');
                  homeApp.pageDevices();
                }
              }
              else {
                homeApp.errors = homeApp.$t("lk.Error");
              }
            } catch (error) {
              console.log("Ans" + xhr.responseText + error);

              homeApp.errors = homeApp.$t("lk.Error");
              return;
            }
          } else {
            homeApp.errors = homeApp.$t("lk.Error");
            return;
          }
        }
      };
      setTimeout(function () { this.errors = '' }, 3000);
      xhr.send(JSON.stringify(this.modbus_registers));
    },
    submitObject() {
      if (!this.editobject.object_name || this.editobject.object_name == "") {
        this.errors = this.$t("lk.Please_enter_valid_name");
        return;
      }
      this.errors = '';
      let formData = new FormData();
      formData.append('editobject', JSON.stringify(this.editobject));
      formData.append('postJson', 'yes');
      let xhr = new XMLHttpRequest();
      if (!this.idObject || this.idObject == 0)
        xhr.open("POST", "/panel/sendobject");
      else
        xhr.open("POST", "/panel/sendobject/" + this.idObject);
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status >= 200 && xhr.status < 400) {
            try {
              let ans = JSON.parse(xhr.responseText)['ans'];
              console.log("Ans" + xhr.responseText);
              switch (ans) {
                case 'succesful':
                  homeApp.errors = homeApp.$t("lk.Operation_completed_successfully");
                  if (!homeApp.idObject || homeApp.idObject == 0) {
                    router.push('/panel/');
                    homeApp.pageIndex();
                  }
                  break;
                case 'not_id':
                  homeApp.errors = homeApp.$t("lk.Invalid_Id");
                  break;
                case 'not_name':
                  homeApp.errors = homeApp.$t("lk.Please_enter_valid_name");
                  break;
                case 'no_permission':
                  homeApp.errors = homeApp.$t("lk.No_permission");
                  break;
                default:
                  homeApp.errors = homeApp.$t("lk.Error");
                  break;
              }
            } catch (error) {
              console.log("Ans" + xhr.responseText + error);

              homeApp.errors = homeApp.$t("lk.Error");
              return;
            }
          } else {
            homeApp.errors = homeApp.$t("lk.Error");
            return;
          }
        }
      };
      setTimeout(function () { this.errors = '' }, 3000);
      xhr.send(formData);
    },
    submitLk() {
      if (!this.inMailLk) {
        this.errors = this.$t("lk.Please_enter_valid_email");
        return;
      } else if (!this.validEmail(this.inMailLk)) {
        this.errors = this.$t("lk.Please_enter_valid_email");
        return;
      }
      if (this.inNewPasswordLk != this.inRepeatPasswordLk) {
        this.errors = this.$t("lk.Passwords_do_not_match");
        return;
      }
      this.errors = '';
      var formData = new FormData();
      formData.append('inMailLk', this.inMailLk);
      formData.append('postJson', 'yes');
      if (this.inNewPasswordLk) {
        formData.append('inNewPasswordLk', this.inNewPasswordLk);
      }
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "/panel/sendlk");
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status >= 200 && xhr.status < 400) {
            homeApp.errors = homeApp.$t("lk.Operation_completed_successfully");
            setTimeout(function () { homeApp.errors = '' }, 3000);

          } else {
            alert('SubmitLk error 1!');
            return;
          }
        }
      };
      xhr.send(formData);
    },
    searchObjects() {
      this.send = new FormData();
      this.send.append('searchNameObject', this.searchNameObject);
      this.send.append('searchRegionObject', this.searchRegionObject);
      this.send.append('searchStreetObject', this.searchStreetObject);
      this.send.append('searchFunction', 'yes');
      this.send.append('postJson', 'yes');
      this.numPage = 1;
      this.pageIndex();
    },
    clearSearchObjects() {
      this.send = new FormData();
      this.searchNameObject = '';
      this.searchRegionObject = '';
      this.searchStreetObject = '';
      this.send.append('postJson', 'yes');
      this.send.append('searchFunction', 'yes');
      console.log(this.send);
      this.numPage = 1;
      this.pageIndex();
    },
    searchDevices() {
      this.send = new FormData();
      this.send.append('searchNameDevice', this.searchNameDevice);
      this.send.append('searchStatusDevice', this.searchStatusDevice);
      this.send.append('searchFunction', 'yes');
      this.send.append('postJson', 'yes');
      this.numPage = 1;
      this.pageDevices();
    },
    clearSearchDevices() {
      this.send = new FormData();
      this.searchNameDevice = '';
      this.searchStatusDevice = '';
      this.send.append('postJson', 'yes');
      this.send.append('searchFunction', 'yes');
      this.numPage = 1;
      this.pageDevices();
    },
    searchRegisters() {
      this.send = new FormData();
      this.send.append('searchNameRegisters', this.searchNameRegisters);
      this.send.append('searchAddressRegisters', this.searchAddressRegisters);
      this.send.append('searchFunction', 'yes');
      this.send.append('postJson', 'yes');
      this.numPage = 1;
      this.pageRegisters();
    },
    clearSearchRegisters() {
      this.send = new FormData();
      this.searchNameRegisters = '';
      this.searchAddressRegisters = '';
      this.send.append('postJson', 'yes');
      this.send.append('searchFunction', 'yes');
      this.numPage = 1;
      this.pageRegisters();
    },
    searchClients() {
      this.send = new FormData();
      this.send.append('searchFioClient', this.searchFioClient);
      this.send.append('searchPhoneClient', this.searchPhoneClient);
      this.send.append('searchEmailClient', this.searchEmailClient);
      this.send.append('searchCompanyClient', this.searchCompanyClient);
      this.send.append('searchOGRNClient', this.searchOGRNClient);
      this.send.append('searchFunction', 'yes');
      this.send.append('postJson', 'yes');
      this.numPage = 1;
      this.pageClients();
    },
    clearSearchClients() {
      this.send = new FormData();
      this.searchFioClient = '';
      this.searchCompanyClient = '';
      this.searchPhoneClient = '';
      this.searchEmailClient = '';
      this.searchOGRNClient = '';
      this.send.append('postJson', 'yes');
      this.send.append('searchFunction', 'yes');
      console.log(this.send);
      this.numPage = 1;
      this.pageClients();
    },
    clearPage() {
      this.loadModal = false
      this.submitOrdersVis = false;
      /*this.inputFileName = homeApp.$t("lk.Select_file");
      this.inputTaskName = homeApp.$t("lk.Name_of_task");
      this.pageIndex();*/
    },
    cleanFeedbackForm() {
      this.inTopicFeedback = '';
      this.inTextFeedback = '';
      this.errors = '';
      this.submitVis = true;
      this.repeatVis = false;
    },
    mobMenuToggle() {
      if (this.mobMenuOn === 1) {
        this.mobMenuOn = 0;
      }
      else this.mobMenuOn = 1;
      return;
    },
    langToggle() {
      if (this.$i18n.locale === 'en') {
        this.$i18n.locale = 'ru';
        this.otherLang = 'English'
      }
      else {
        this.$i18n.locale = 'en';
        this.otherLang = 'Русский'
      }
    },
    visibleDelObjects() {
      if (this.checkedObjects.length > 0)
        this.deleteObjectVis = true;
      else
        this.deleteObjectVis = false;
    },
    visibleDelDevices() {
      if (this.checkedDevices.length > 0)
        this.deleteDeviceVis = true;
      else
        this.deleteDeviceVis = false;
    },
    visibleDelRegisters() {
      if (this.checkedRegisters.length > 0)
        this.deleteRegisterVis = true;
      else
        this.deleteRegisterVis = false;
    },
    visibleDelClients() {
      if (this.checkedClients.length > 0)
        this.deleteClientVis = true;
      else
        this.deleteClientVis = false;
    },
    deleteObjects() {
      this.confirmModal = true;
      this.typeDelete = 'objects';
    },
    deleteDevices() {
      this.confirmModal = true;
      this.typeDelete = 'devices';
    },
    deleteRegisters() {
      this.confirmModal = true;
      this.typeDelete = 'registers';
    },
    deleteYes() {
      var formData = new FormData();
      formData.append('postJson', 'yes');
      var xhr = new XMLHttpRequest();
      if (this.typeDelete == 'objects') {
        formData.append('id_pos', this.checkedObjects);
        xhr.open("POST", "/panel/delobjects");
        this.deleteObjectVis = false;
        this.checkedObjects = [];
        this.pageIndex();
      }
      if (this.typeDelete == 'devices') {
        formData.append('id_pos', this.checkedDevices);
        xhr.open("POST", "/panel/deldevices");
        this.deleteDeviceVis = false;
        this.checkedDevices = [];
        this.searchDevices();
      }
      if (this.typeDelete == 'registers') {
        formData.append('id_pos', this.checkedRegisters);
        xhr.open("POST", "/panel/delregisters");
        this.deleteRegisterVis = false;
        this.checkedRegisters = [];
        this.searchRegisters();
      }
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status >= 200 && xhr.status < 400) {
            if (homeApp.typeDelete == 'objects') {
              homeApp.pageIndex();
            }
            if (homeApp.typeDelete == 'devices') {
              homeApp.searchDevices();
            }
            if (homeApp.typeDelete == 'registers') {
              homeApp.searchRegisters();
            }
            /*console.log(xhr.responseText);*/
          } else {
            homeApp.errors = homeApp.$t("lk.Error");
            return;
          }
        }
      };
      xhr.send(formData);

      this.confirmModal = false;
    },
    validEmail: function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },

  },
  i18n
})

