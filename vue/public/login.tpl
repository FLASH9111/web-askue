<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Panel</title>
        <link href="/vue/public/login.css" rel="stylesheet">
	</head>
<body>
    <div class="login">
    <h1>Panel</h1>
	<form name="login" method="post">
		<input placeholder="E-mail" type="text" name="user" /><br>
		<input placeholder="Password" type="password" name="password" /><br>
		<button type="button" onclick="check()">Enter</button><br>
		<a class="forpas" href="/panel/forgot">Forgot password</a>
	</form>
    </div>
    <div id="error">
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" >
    	addEventListener("keyup", function(event) {
    		if (event.keyCode == 13)
				check();
  		});
    	function check() {
  			var user = document.forms["login"]["user"].value;
  			var password = document.forms["login"]["password"].value;
  			
  			if (user.length==0){
  				return errorMessage("Fill in all the fields!");
   			}
  			if (user.length > 50 || password.length > 50){
  				return errorMessage("Symbol limit reached!");
   			}
  			$.post(
				"/panel/login/",
				{
					user: user,
					password: password
				},
				onSuccess 
			);
  		}
  		function onSuccess(answer)
		{
			return errorMessage(answer);
		}
		function errorMessage(mess)
		{
		    var err = document.getElementById("error"); 
            if (err.style.display != "block") { 
            	err.style.display = "block"; 
            }
      		err.innerHTML=mess;
      		setTimeout(function(){err.style.display = "none"}, 3000); 
      		return false;
		}
    </script>
</body>
</html>