<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Password recovery</title>
	<link href="/admin/templates/default/login.css" rel="stylesheet">
</head>

<body>
	<div class="login" style="height: 235px; 	margin-top: -117.5px; ">
		<h1>Password recovery</h1>
		<form name="login" method="post">
			<input placeholder="E-mail" type="text" name="user" /><br>
			<button type="button" onclick="check()">Enter</button><br>
		</form>
	</div>
	<div id="error">
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$(window).keydown(function (event) {
				if (event.keyCode == 13) {
					event.preventDefault();
					return false;
				}
			});
		});
		addEventListener("keyup", function (event) {
			if (event.keyCode == 13)
				check();
				//alert(1);
			event.preventDefault();
		});
		function check() {
			var user = document.forms["login"]["user"].value;

			if (user.length == 0) {
				return errorMessage("Fill in all the fields!");
			}
			$.post(
				"/panel/recovery/",
				{
					user: user,
				},
				onSuccess
			);
		}
		function onSuccess(answer) {
			errorMessage(answer);
			return false;
		}
		function errorMessage(mess) {
			var err = document.getElementById("error");
			if (err.style.display != "block") {
				err.style.display = "block";
			}
			err.innerHTML = mess;
			setTimeout(function () { err.style.display = "none" }, 3000);
			return false;
		}
	</script>
</body>

</html>