<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="zed">
  <link rel="icon" href="../../../../favicon.ico">
  <link href="/vue/public/assets/css/all.min.css" rel="stylesheet">
  <link
    href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=cyrillic"
    rel="stylesheet">
  {{meta}}
  <style>
    #peeek-loading {
      font: 12px/16px Arial, Helvetica, sans-serif;
      list-style: none;
      color: #fff;
      background-color: rgba(25, 122, 223, 0.8);
      overflow: hidden;
      position: fixed;
      z-index: 9999;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      height: 100%;
      width: 100%;
      transition: all 3s linear;
      -moz-transition: all 3s linear;
      -webkit-transition: all 3s linear
    }

    #peeek-loading ul {
      position: absolute;
      left: calc(50% - .7em);
      top: calc(50% - 4.2em);
      display: inline-block;
      text-indent: 2.8em
    }

    #peeek-loading ul li:after,
    #peeek-loading ul:after {
      width: 1em;
      height: 1em;
      background-color: #fff;
      border-radius: 100%
    }

    #peeek-loading ul li:after,
    #peeek-loading ul:after {
      content: "";
      display: block
    }

    #peeek-loading ul:after {
      position: absolute;
      top: 2.8em;
      left: 0
    }

    #peeek-loading li {
      list-style: none;
      position: absolute;
      padding-bottom: 5.6em;
      top: 0;
      left: 0
    }

    #peeek-loading li:nth-child(1) {
      transform: rotate(0deg);
      animation-delay: .125s
    }

    #peeek-loading li:nth-child(1):after {
      animation-delay: .125s
    }

    #peeek-loading li:nth-child(2) {
      transform: rotate(36deg);
      animation-delay: .25s
    }

    #peeek-loading li:nth-child(2):after {
      animation-delay: .25s
    }

    #peeek-loading li:nth-child(3) {
      transform: rotate(72deg);
      animation-delay: .375s
    }

    #peeek-loading li:nth-child(3):after {
      animation-delay: .375s
    }

    #peeek-loading li:nth-child(4) {
      transform: rotate(108deg);
      animation-delay: .5s
    }

    #peeek-loading li:nth-child(4):after {
      animation-delay: .5s
    }

    #peeek-loading li:nth-child(5) {
      transform: rotate(144deg);
      animation-delay: .625s
    }

    #peeek-loading li:nth-child(5):after {
      animation-delay: .625s
    }

    #peeek-loading li:nth-child(6) {
      transform: rotate(180deg);
      animation-delay: .75s
    }

    #peeek-loading li:nth-child(6):after {
      animation-delay: .75s
    }

    #peeek-loading li:nth-child(7) {
      transform: rotate(216deg);
      animation-delay: .875s
    }

    #peeek-loading li:nth-child(7):after {
      animation-delay: .875s
    }

    #peeek-loading li:nth-child(8) {
      transform: rotate(252deg);
      animation-delay: 1s
    }

    #peeek-loading li:nth-child(8):after {
      animation-delay: 1s
    }

    #peeek-loading li:nth-child(9) {
      transform: rotate(288deg);
      animation-delay: 1.125s
    }

    #peeek-loading li:nth-child(9):after {
      animation-delay: 1.125s
    }

    #peeek-loading li:nth-child(10) {
      transform: rotate(324deg);
      animation-delay: 1.25s
    }

    #peeek-loading li:nth-child(10):after {
      animation-delay: 1.25s
    }

    #peeek-loading li {
      animation: dotAnimation 2.5s infinite
    }

    @keyframes dotAnimation {

      0%,
      55%,
      100% {
        padding: 0 0 5.6em 0
      }

      5%,
      50% {
        padding: 2.8em 0
      }
    }

    #peeek-loading li:after {
      animation: dotAnimationTwo 2.5s infinite
    }

    @-webkit-keyframes dotAnimationTwo {

      0%,
      55%,
      100% {
        opacity: 1;
        transform: scale(1)
      }

      5%,
      50% {
        opacity: .5;
        transform: scale(0.5)
      }
    }
  </style>
  <!-- Bootstrap core CSS -->
  <link href="/vue/public/assets/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="/vue/public/assets/css/dashboard.css?ver=1" rel="stylesheet">

</head>

<body>
  <div id="lkApp">
    <div id="peeek-loading" v-if="loader" style="/* display: none; */">
      <ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </div>
    <div class="content-zed">
      <nav class="navbar navbar-dark fixed-top bg-white flex-md-nowrap p-0 shadow">
        <div class="bg-color col-sm-3 col-md-2">
          <div class="row">
            <a class="navbar-brand mr-0 col-10" href="/panel/">{{$t('lk.Dash_Panel')}}</a>
            <button @click="mobMenuToggle" data-toggle="collapse" data-target="#navbarsExampleDefault"
              class="navbar-toggler d-inline d-sm-none justify-content-end col-2" type="button"
              aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
        </div>

        <div class="nav-about d-none d-md-block w-100">
        </div>
        <ul class="d-none navbar-nav px-3" v-bind:class="{'d-block': mobMenuOn === 1}">
          <li class="nav-item">
            <router-link v-on:click.native="pageIndex" class="nav-link" to="/panel/">
              <i class="far fa-home-alt">&nbsp;</i>
              {{$t('lk.Objects')}} <span class="sr-only">(current)</span>
            </router-link>
          </li>
          <li class="nav-item">
            <router-link v-on:click.native="pageLk" class="nav-link" to="/panel/lk">
              <i class="far fa-user">&nbsp;</i>
              {{$t('lk.Personal_account')}}
            </router-link>
          </li>

          <li class="nav-item">
            <router-link v-on:click.native="pageDevices" class="nav-link" to="/panel/clients">
              <i class="far fa-tachometer">&nbsp;</i>
              {{$t('lk.Devices')}}
            </router-link>
          </li>
          <li class="nav-item">
            <a class="nav-link" @click="langToggle" href="#">{{otherLang}}</a>

          </li>
          <li class="nav-item">
            <a class="nav-link" href="/panel/exit">{{$t('lk.Logout')}}</a>
          </li>
        </ul>
        <a class="toggleLang d-none d-md-block" @click="langToggle" href="#">{{otherLang}}</a>
        <ul class="navbar-nav d-none d-md-block px-3 btn-exit ">
          <li class="nav-item text-nowrap">
            <a class="nav-link" href="/panel/exit">{{$t('lk.Logout')}}</a>
          </li>
        </ul>
      </nav>

      <div class="container-fluid" id="">
        <div class="row">
          <nav class="col-md-2 d-none d-md-block bg-dark sidebar">
            <div class="sidebar-sticky">
              <ul class="nav flex-column">
                <li class="nav-item">
                  <router-link v-on:click.native="pageIndex" class="nav-link" to="/panel/">
                    <i class="far fa-home-alt">&nbsp;</i>
                    {{$t('lk.Objects')}} <span class="sr-only">(current)</span>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link v-on:click.native="pageDevices" class="nav-link" to="/panel/devices">
                    <i class="far fa-tachometer">&nbsp;</i>
                    {{$t('lk.Devices')}}
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link v-on:click.native="pageLk" class="nav-link" to="/panel/lk">
                    <i class="far fa-user">&nbsp;</i>
                    {{$t('lk.Personal_account')}}
                  </router-link>
                </li>
              </ul>
            </div>
          </nav>

          <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
            <div v-if="currentPage == 'index'">
              <section class="py-4">
                <div class="row">
                  <div class="col-md-2">
                    <router-link to="/panel/addobject" class="btn btn-primary btn-mob w-100"
                      v-on:click.native="pageAddObject()">
                      {{$t('lk.Add')}}</router-link>
                  </div>
                  <div class="col-md-2">
                    <router-link to="/panel/" class="btn btn-secondary btn-mob w-100"
                      v-on:click.native="searchObjects()">{{$t('lk.Refresh')}}</router-link>
                  </div>
                </div>
              </section>
              <section class="panel panel-default">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center px-2">
                    <h1 class="h3">{{$t('lk.Objects')}}
                    </h1>
                  </div>
                  <form novalidate="true" class="form-search px-2">
                    <div class="row">
                      <div class="col-md-3 pt-2">
                        <label for="searchNameObject">{{$t('lk.Search_by_name')}}:</label>
                        <input type="text" v-model="searchNameObject" class="form-control" id="searchNameObject"
                          :placeholder="$t('lk.House_1')">
                      </div>
                      <div class="col-md-3 pt-2">
                        <label for="searchRegionObject">{{$t('lk.Search_by_region')}}:</label>
                        <select class="form-control" v-model="searchRegionObject" id="searchRegionObject">
                          <option disabled value="">{{$t('lk.Click_to_select')}}</option>
                          <option v-for="ar in allRegions" v-bind:value="ar.id_regions">
                            {{ ar.region }}
                          </option>
                        </select>
                      </div>
                      <div class="col-md-2 pt-2">
                        <label for="searchStreetObject">{{$t('lk.Search_by_street')}}:</label>
                        <input type="text" v-model="searchStreetObject" class="form-control" id="searchStreetObject"
                          :placeholder="$t('lk.Lomonosov')">
                      </div>
                      <div class="col pt-3">
                        <router-link to="/panel/" class="btn btn-light btn-mob mt-4 w-100"
                          v-on:click.native="searchObjects()">{{$t('lk.Search')}}</router-link>
                      </div>
                      <div class="col pt-3">
                        <router-link to="/panel/" class="btn btn-secondary btn-mob mt-4 w-100"
                          v-on:click.native="clearSearchObjects()">{{$t('lk.Clear')}}</router-link>
                      </div>
                    </div>

                  </form>
                  <div class="table-responsive px-2">

                    <table class="table table-striped table-sm">
                      <thead>
                        <tr>
                          <th>{{$t('lk.Title')}}</th>
                          <th class="d-none d-md-table-cell">{{$t('lk.Address')}}</th>
                          <th class="d-none d-md-table-cell">{{$t('lk.Number_of_devices')}}</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="(ao, i) in allObjects" :key="ao.id">
                          <td>
                            <router-link v-on:click.native="pageObject" class="text-dark"
                              :to="`/panel/object/${ao.id_objects}`">
                              {{ao.object_name}}
                              <span data-feather="chevron-right"></span>
                            </router-link>
                          </td>
                          <td class="d-none d-md-table-cell">{{ao.country}}, {{ao.region}}<span v-if="ao.locality">,
                            </span>{{ao.locality}}, {{ao.street}}<span v-if="ao.house">, </span>{{ao.house}}<span
                              v-if="ao.building">, </span>{{ao.building}}<span v-if="ao.flat">, кв. </span> {{ao.flat}}
                          </td>
                          <td class="d-none d-md-table-cell">{{ao.count_devices}}</td>

                          <td>
                            <input type="checkbox" v-on:change="visibleDelObjects" :value="ao.id_objects"
                              v-model="checkedObjects">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="row">
                    <div class="col">
                      <nav class="mx-2" aria-label="...">
                        <ul class="pagination">
                          <li class="page-item" v-bind:class="{ disabled: !prevNavObjects }">
                            <router-link v-on:click.native="pageIndex" class="page-link"
                              :to="`/panel/${prevObjectLink}`">
                              {{$t('lk.Prev')}}</router-link>
                          </li>
                          <li class="page-item" v-bind:class="{ disabled: !nextNavObjects }">
                            <router-link v-on:click.native="pageIndex" class="page-link"
                              :to="`/panel/${nextObjectLink}`">
                              {{$t('lk.Next')}}</router-link>
                          </li>
                        </ul>
                      </nav>
                    </div>
                    <div class="col text-right">
                      <button type="button" class="btn btn-primary btn-mob mr-2" :disabled="!deleteObjectVis"
                        @click="deleteObjects()">{{$t('lk.Delete_selected')}}</button>
                    </div>
                  </div>
                </div>
              </section>
            </div>
            <div v-if="currentPage == 'registers'">
              <section class="py-4">
                <div class="row">
                  <div class="col-md-2">
                    <router-link to="/panel/addregister" class="btn btn-primary btn-mob w-100"
                      v-on:click.native="pageAddRegister()">
                      {{$t('lk.Add')}}</router-link>
                  </div>

                </div>
              </section>
              <section class="panel panel-default">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center px-2">
                    <h1 class="h3">{{$t('lk.Modbus_Registers')}}
                    </h1>
                  </div>
                  <form novalidate="true" class="form-search px-2">
                    <div class="row">
                      <div class="col-md-4 pt-2">
                        <label for="searchNameRegisters">{{$t('lk.Search_by_name')}}:</label>
                        <input type="text" v-model="searchNameRegisters" class="form-control" id="searchNameRegisters"
                          :placeholder="$t('lk.Temperature_1')">
                      </div>
                      <div class="col-md-3 pt-2">
                        <label for="searchAddressRegisters">{{$t('lk.Search_by_address')}}:</label>
                        <input type="text" v-model="searchAddressRegisters" class="form-control"
                          id="searchAddressRegisters" placeholder="0x0002">
                      </div>
                      <div class="col pt-3">
                        <router-link :to="`/panel/registers/${idObjects}`" class="btn btn-light btn-mob mt-4 w-100"
                          v-on:click.native="searchRegisters()">{{$t('lk.Search')}}</router-link>
                      </div>
                      <div class="col pt-3">
                        <router-link :to="`/panel/registers/${idObjects}`" class="btn btn-secondary btn-mob mt-4 w-100"
                          v-on:click.native="clearSearchRegisters()">{{$t('lk.Clear')}}</router-link>
                      </div>
                    </div>

                  </form>
                  <div class="table-responsive px-2">

                    <table class="table table-striped table-sm">
                      <thead>
                        <tr>
                          <th>{{$t('lk.Title')}}</th>
                          <th class="d-none d-md-table-cell">{{$t('lk.Physical_address')}}</th>
                          <th class="d-none d-md-table-cell">{{$t('lk.Function')}}</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="(ar, i) in allRegisters" :key="ar.id_modbus_registers">
                          <td>
                            <router-link v-on:click.native="pageRegister" class="text-dark"
                              :to="`/panel/editregister/${ar.id_modbus_registers}`">
                              {{ar.channel_name}}
                              <i class="far fa-angle-right"></i>
                            </router-link>
                          </td>
                          <td class="d-none d-md-table-cell">{{ar.physical_address}}
                          </td>
                          <td class="d-none d-md-table-cell">
                            {{ar.function_code}}
                          </td>
                          <td>
                            <input type="checkbox" v-on:change="visibleDelRegisters" :value="ar.id_modbus_registers"
                              v-model="checkedRegisters">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="row">
                    <div class="col">
                      <nav class="mx-2">
                        <ul class="pagination">
                          <li class="page-item" v-bind:class="{ disabled: !prevNavRegisters }">
                            <router-link v-on:click.native="pageRegisters" class="page-link"
                              :to="`/panel/registers/${idObjects}/${prevRegisterLink}`">
                              {{$t('lk.Prev')}}</router-link>
                          </li>
                          <li class="page-item" v-bind:class="{ disabled: !nextNavRegisters }">
                            <router-link v-on:click.native="pageRegisters" class="page-link"
                              :to="`/panel/registers/${idObjects}/${nextRegisterLink}`">
                              {{$t('lk.Next')}}</router-link>
                          </li>
                        </ul>
                      </nav>
                    </div>
                    <div class="col text-right">
                      <button type="button" class="btn btn-primary btn-mob mr-2" :disabled="!deleteRegisterVis"
                        @click="deleteRegisters()">{{$t('lk.Delete_selected')}}</button>
                    </div>
                  </div>
                </div>
              </section>
            </div>
            <div v-if="currentPage == 'devices'">
              <section class="py-4">
                <div class="row">
                  <div class="col-md-2">
                    <router-link to="/panel/adddevice" class="btn btn-primary btn-mob w-100"
                      v-on:click.native="pageAddDevice()">
                      {{$t('lk.Add')}}</router-link>
                  </div>
                  <div class="col-md-2">
                    <router-link to="/panel/devices" class="btn btn-secondary btn-mob w-100"
                      v-on:click.native="searchDevices()">{{$t('lk.Refresh')}}</router-link>
                  </div>
                </div>
              </section>
              <section class="panel panel-default">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center px-2">
                    <h1 class="h3">{{$t('lk.Devices')}}
                    </h1>
                  </div>
                  <form novalidate="true" class="form-search px-2">
                    <div class="row">
                      <div class="col-md-4 pt-2">
                        <label for="searchNameDevice">{{$t('lk.Search_by_name')}}:</label>
                        <input type="text" v-model="searchNameDevice" class="form-control" id="searchNameDevice"
                          :placeholder="$t('lk.Device_1')">
                      </div>
                      <div class="col-md-3 pt-2">
                        <label for="searchStatusDevice">{{$t('lk.Search_by_status')}}:</label>
                        <select class="form-control" v-model="searchStatusDevice" id="searchStatusDevice">
                          <option value="">{{$t('lk.Click_to_select')}}</option>
                          <option value="1">{{$t('lk.Enabled')}}</option>
                          <option value="0">{{$t('lk.Disabled')}}</option>
                        </select>
                      </div>
                      <div class="col pt-3">
                        <router-link to="/panel/devices" class="btn btn-light btn-mob mt-4 w-100"
                          v-on:click.native="searchDevices()">{{$t('lk.Search')}}</router-link>
                      </div>
                      <div class="col pt-3">
                        <router-link to="/panel/devices" class="btn btn-secondary btn-mob mt-4 w-100"
                          v-on:click.native="clearSearchDevices()">{{$t('lk.Clear')}}</router-link>
                      </div>
                    </div>

                  </form>
                  <div class="table-responsive px-2">

                    <table class="table table-striped table-sm">
                      <thead>
                        <tr>
                          <th>{{$t('lk.Title')}}</th>
                          <th class="d-none d-md-table-cell">{{$t('lk.Object')}}</th>
                          <th class="d-none d-md-table-cell">{{$t('lk.Status')}}</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="(ad, i) in allDevices" :key="ad.id_devices">
                          <td>
                            <router-link v-on:click.native="pageDevices" class="text-dark"
                              :to="`/panel/device/${ad.id_devices}`">
                              {{ad.device_name}}
                              <span data-feather="chevron-right"></span>
                            </router-link>
                          </td>
                          <td class="d-none d-md-table-cell">{{ad.object_name}}
                          </td>
                          <td class="d-none d-md-table-cell">
                            <span v-if="ad.status === true">{{$t('lk.Enabled')}}</span>
                            <span v-else>{{$t('lk.Disabled')}}</span>
                          </td>
                          <td>
                            <input type="checkbox" v-on:change="visibleDelDevices" :value="ad.id_devices"
                              v-model="checkedDevices">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="row">
                    <div class="col">
                      <nav class="mx-2" aria-label="...">
                        <ul class="pagination">
                          <li class="page-item" v-bind:class="{ disabled: !prevNavDevices }">
                            <router-link v-on:click.native="pageDevices" class="page-link"
                              :to="`/panel/${prevDeviceLink}`">
                              {{$t('lk.Prev')}}</router-link>
                          </li>
                          <li class="page-item" v-bind:class="{ disabled: !nextNavDevices }">
                            <router-link v-on:click.native="pageDevices" class="page-link"
                              :to="`/panel/${nextDeviceLink}`">
                              {{$t('lk.Next')}}</router-link>
                          </li>
                        </ul>
                      </nav>
                    </div>
                    <div class="col text-right">
                      <button type="button" class="btn btn-primary btn-mob mr-2" :disabled="!deleteDeviceVis"
                        @click="deleteDevices()">{{$t('lk.Delete_selected')}}</button>
                    </div>
                  </div>
                </div>
              </section>
            </div>
            <div v-if="currentPage == 'clients'">
              <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-3">
              </div>
              <section class="panel panel-default">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h1 class="h3">{{$t('lk.Search_option')}}</h1>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 col-lg-offset-2">
                      <div layout="column" class="md-inline-form inputdemoBasicUsage layout-column">
                        <div>
                          <form novalidate="true" class="form-search">
                            <div class="row">
                              <div class="col-md-3 pt-2">
                                <label for="searchFioClient">{{$t('lk.Search_by_fio')}}:</label>
                                <input type="text" v-model="searchFioClient" class="form-control" id="searchFioClient"
                                  :placeholder="$t('lk.FIO')"></div>
                              <div class="col-md-3 pt-2">
                                <label for="searchPhoneClient">{{$t('lk.Search_by_phone')}}:</label>
                                <input type="phone" v-model="searchPhoneClient" class="form-control"
                                  id="searchPhoneClient" placeholder="+79999999999"></div>
                              <div class="col-md-3 pt-2">
                                <label for="searchEmailClient">{{$t('lk.Search_by_email')}}:</label>
                                <input type="text" v-model="searchEmailClient" class="form-control"
                                  id="searchEmailClient" placeholder="example@mail.ru"></div>
                              <div class="col-md-3 pt-2">
                                <label for="searchCompanyClient">{{$t('lk.Search_by_all_data')}}:</label>
                                <input type="text" v-model="searchCompanyClient" id="searchCompanyClient"
                                  class="form-control" :placeholder="$t('lk.INN_KPP_OGRN_ADDRESS')"></div>
                              <div class="col-md-3 pt-2">
                                <label for="searchOGRNClient">{{$t('lk.Search_by_OGRN')}}:</label>
                                <input type="text" v-model="searchOGRNClient" id="searchOGRNClient" class="form-control"
                                  :placeholder="$t('lk.OGRN')"></div>
                              <div class="col-md-2 pt-3">
                                <router-link to="/panel/clients" class="btn btn-primary btn-mob mt-4 w-100"
                                  v-on:click.native="searchClients()">{{$t('lk.Search')}}</router-link>
                              </div>
                              <div class="col-md-2 pt-3">
                                <router-link to="/panel/clients" class="btn btn-secondary btn-mob mt-4 w-100"
                                  v-on:click.native="clearSearchClients()">{{$t('lk.Clear')}}</router-link>
                              </div>
                            </div>

                          </form>
                        </div>
                        <p>{{$t('lk.Select_type_of_search_and_click_search')}}
                          <!--{{$t('lk.The_cost_of_one_file_is')}} {{price}} {{currency}}-->
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <section class="panel panel-default">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h1 class="h3">{{$t('lk.Clients')}} <router-link to="/panel/clients/"
                        v-on:click.native="searchClients()"><span data-feather="refresh-cw"></span></router-link>
                    </h1>
                  </div>
                  <div class="table-responsive layout-padding">
                    <table class="table table-striped table-sm">
                      <thead>
                        <tr>
                          <th>{{$t('lk.Client_name')}}</th>
                          <th class="d-none d-md-table-cell">{{$t('lk.Email_address')}}</th>
                          <th>{{$t('lk.Phone')}}</th>
                          <th>{{$t('lk.INN_KPP_OGRN_ADDRESS')}}</th>
                          <th>{{$t('lk.OGRN')}}</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="(ac, i) in allClients" :key="ac.id">
                          <td>
                            <router-link v-on:click.native="pageClient" class="text-dark"
                              :to="`/panel/client/${ac.id_customers}`">
                              {{ac.fio}}
                              <span data-feather="chevron-right"></span>
                            </router-link>
                          </td>
                          <td class="d-none d-md-table-cell">{{ac.email}}</td>
                          <td class="d-md-table-cell">{{ac.phone}}</td>
                          <td class="d-md-table-cell">{{ac.inn_kpp}}</td>
                          <td class="d-md-table-cell">{{ac.ogrn}}</td>
                          <td>
                            <input type="checkbox" v-on:change="visibleDelClients" :value="ac.id_customers"
                              v-model="checkedClients">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="row">
                    <div class="col">
                      <nav class="mx-2">
                        <ul class="pagination">
                          <li class="page-item" v-bind:class="{ disabled: !prevNavClients }">
                            <router-link v-on:click.native="pageClients" class="page-link"
                              :to="`/panel/clients/${prevClientLink}`">
                              {{$t('lk.Prev')}}</router-link>
                          </li>
                          <li class="page-item" v-bind:class="{ disabled: !nextNavClients }">
                            <router-link v-on:click.native="pageClients" class="page-link"
                              :to="`/panel/clients/${nextClientLink}`">
                              {{$t('lk.Next')}}</router-link>
                          </li>
                        </ul>
                      </nav>
                    </div>
                    <div class="col text-right">
                      <button type="button" class="btn btn-primary btn-mob mr-2" :disabled="!deleteClientVis"
                        @click="deleteClients()">{{$t('lk.Delete_selected')}}</button>
                    </div>
                  </div>
                </div>
              </section>
            </div>

            <div v-if="currentPage == 'editobject'">

              <section class="panel panel-default mt-4">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center px-2">
                    <h1 class="h3">{{editObjectTitle}}</h1>
                  </div>
                  <form novalidate="true" class="form-search px-2">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>{{$t('lk.Name_of_object')}}*:</label>
                          <input type="text" v-model="editobject.object_name" class="form-control" id="nameObject"
                            :placeholder="$t('lk.House_1')">
                        </div>
                        <div class="form-group">
                          <label>{{$t('lk.Remark')}}:</label>
                          <textarea type="text" v-model="editobject.remark" class="form-control"></textarea>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                          <label>{{$t('lk.Country')}}:</label>
                          <select class="form-control" v-model="editobject.id_countries_countries">
                            <option disabled value="0">{{$t('lk.Click_to_select')}}</option>
                            <option v-for="ar in allCountries" v-bind:value="ar.id_countries">
                              {{ ar.country }}
                            </option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>{{$t('lk.Region')}}:</label>
                          <select class="form-control" v-model="editobject.id_regions_regions">
                            <option disabled value="0">{{$t('lk.Click_to_select')}}</option>
                            <option v-for="ar in allRegions" v-bind:value="ar.id_regions">
                              {{ ar.region }}
                            </option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>{{$t('lk.Locality')}}:</label>
                          <input type="text" v-model="editobject.locality" class="form-control"
                            :placeholder="$t('lk.Vyatskoe')">
                        </div>
                        <div class="form-group">
                          <label>{{$t('lk.Street')}}:</label>
                          <input type="text" v-model="editobject.street" class="form-control" id="street"
                            :placeholder="$t('lk.Lomonosov')">
                        </div>
                        <div class="form-group">
                          <label>{{$t('lk.House')}}:</label>
                          <input type="text" v-model="editobject.house" class="form-control" id="house"
                            placeholder="54">
                        </div>

                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="nameSearchForm">{{$t('lk.Building')}}:</label>
                          <input type="text" v-model="editobject.building" class="form-control" placeholder="1">
                        </div>
                        <div class="form-group">
                          <label for="nameSearchForm">{{$t('lk.Flat')}}:</label>
                          <input type="text" v-model="editobject.flat" class="form-control" placeholder="21">
                        </div>
                        <div class="form-group">
                          <label for="nameSearchForm">{{$t('lk.Office')}}:</label>
                          <input type="text" v-model="editobject.office" class="form-control" placeholder="14">
                        </div>
                        <div class="form-group">
                          <label for="nameSearchForm">{{$t('lk.Additional')}}:</label>
                          <input type="text" v-model="editobject.additional" class="form-control">
                        </div>

                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          {{errors}}
                        </div>
                        <div class="form-group mb-0">
                          <button type="button" @click="submitObject()"
                            class="btn btn-primary mt-2">{{$t('lk.Save')}}</button>
                        </div>
                      </div>

                    </div>
                  </form>

                </div>
              </section>
            </div>
            <div v-if="currentPage == 'editregister'">

              <section class="panel panel-default mt-4">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center px-2">
                    <h1 class="h3">{{editRegisterTitle}}</h1>
                  </div>
                  <form novalidate="true" class="form-search px-2">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>{{$t('lk.Channel_name')}}*:</label>
                          <input type="text" v-model="modbus_registers.channel_name" class="form-control" id="ChannelNameRegister"
                            :placeholder="$t('lk.Temperature_1')">
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="PhysicalAddressRegister">{{$t('lk.Physical_address')}}*:</label>
                          <input type="text" v-model="modbus_registers.physical_address" class="form-control" id="PhysicalAddressRegister"
                            placeholder="0x0002">
                        </div>
                        <div class="form-group">
                          <label>{{$t('lk.Function')}}*:</label>
                          <select class="form-control" v-model="modbus_registers.id_modbus_functions_modbus_functions">
                            <option disabled value="0">{{$t('lk.Click_to_select')}}</option>
                            <option v-for="af in allModbusFunctions" v-bind:value="af.id_modbus_functions">
                              {{ af.function_name }} ({{ af.function_code }})
                            </option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>{{$t('lk.Formula')}}:</label>
                          <input type="text" v-model="modbus_registers.formula" class="form-control"
                            placeholder="0.07143*x">
                        </div>
                        <div class="form-group">
                          <label for="nameSearchForm">{{$t('lk.Rounding')}}:</label>
                          <input type="number" v-model="modbus_registers.round" class="form-control" placeholder="1">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          {{errors}}
                        </div>
                        <div class="form-group mb-0">
                          <button type="button" @click="submitRegister()"
                            class="btn btn-primary mt-2">{{$t('lk.Save')}}</button>
                        </div>
                      </div>

                    </div>
                  </form>

                </div>
              </section>
            </div>

            <div v-if="currentPage == 'object'">
              <section class="py-4">
                <div class="row">
                  <div class="col-md-3">
                    <router-link :to="`/panel/editobject/${objectData.id_objects}`"
                      class="btn btn-primary btn-mob w-100" v-on:click.native="pageEditObject()">
                      {{$t('lk.General_information')}}</router-link>
                  </div>
                  <div class="col-md-3 px-2">
                    <router-link to="/panel/" class="btn btn-secondary btn-mob w-100"
                      v-on:click.native="accessObject()">{{$t('lk.Access_control')}}</router-link>
                  </div>
                </div>
              </section>
              <section class="panel panel-default">
                <div class="panel-body">
                  <form novalidate="true" class="form-search px-2">
                    <div class="row">
                      <div class="col-sm-3 pt-2">
                        <label for="nameSearchForm">{{$t('lk.Type_of_resource')}}:</label>
                        <select class="form-control" v-model="typeResource">
                          <option selected value="general">{{$t('lk.General')}}</option>
                          <option value="hot_water">{{$t('lk.Hot_water')}}</option>
                          <option value="cold_water">{{$t('lk.Cold_water')}}</option>
                          <option value="electricity">{{$t('lk.Electricity')}}</option>
                          <option value="thrmal">{{$t('lk.Thermal_energy')}}</option>
                        </select>
                      </div>
                      <div class="col-sm-3 pt-3">
                        <router-link :to="`#`" class="btn btn-light mt-4 btn-mob w-100"
                          v-on:click.native="pageEditObject()">
                          {{$t('lk.Create_report')}}: {{$t('lk.Today')}}</router-link>
                      </div>
                      <div class="col-sm-3 pt-3">
                        <router-link :to="`#`" class="btn btn-light btn-mob mt-4 w-100"
                          v-on:click.native="pageEditObject()">{{$t('lk.Create_report')}}: {{$t('lk.Month')}}
                        </router-link>
                      </div>
                      <div class="col-sm-3 pt-3">
                        <router-link :to="`#`" class="btn btn-light btn-mob w-100 mt-4"
                          v-on:click.native="pageEditObject()">{{$t('lk.Create_report')}}: {{$t('lk.Year')}}
                        </router-link>
                      </div>
                    </div>

                  </form>
                </div>
              </section>


              <section class="panel panel-default">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h1 class="h3">Среднемесячное потребление э/энергии, кВт</h1>
                  </div>
                  <bar-chart></bar-chart>

                </div>
              </section>
              <section v-if="typeResource == 'hot_water'" class="panel panel-default">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h1 class="h3">Потребление электроэнергии по дням, кВт</h1>
                  </div>
                  <div class="graph w-100">
                    <line-chart></line-chart>
                  </div>
                </div>
              </section>
            </div>
            <div v-if="currentPage == 'client'">
              <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-3">
              </div>
              <section class="panel panel-default">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h1 class="h3">{{$t('lk.Client_information')}}</h1>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 col-lg-offset-2">
                      <div layout="column" class="md-inline-form inputdemoBasicUsage layout-column">
                        <h3 class="thin">{{clientInfo.fio}}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <section class="panel panel-default">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-6">

                      <p class="mb-1"><span class="text-dark"><b>{{$t('lk.Company')}}:</b></span> <br>
                        {{clientInfo.legal_name}}
                      </p>
                      <p class="mb-1"><span class="text-dark"><b>{{$t('lk.Contact_person')}}:</b></span> <br>
                        {{clientInfo.fio}}
                      </p>
                      <p class="mb-1"><span class="text-dark"><b>{{$t('lk.Phone')}}:</b></span> <br>
                        {{clientInfo.phone}}
                      </p>


                    </div>
                    <div class="col-md-6">
                      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                      </div>
                      <p class="mb-1"><span class="text-dark"><b>{{$t('lk.Legal_address')}}:</b></span> <br>
                        {{clientInfo.legal_address}}
                      </p>
                      <p class="mb-1"><span class="text-dark"><b>{{$t('lk.Inn_kpp')}}:</b></span> <br>
                        {{clientInfo.inn_kpp}}
                      </p>
                      <p class="mb-1"><span class="text-dark"><b>{{$t('lk.OGRN')}}:</b></span> <br>
                        {{clientInfo.ogrn}}
                      </p>
                      <p class="mb-1"><span class="text-dark"><b>{{$t('lk.Email_address')}}:</b></span> <br>
                        {{clientInfo.email}}
                      </p>
                    </div>
                  </div>
                </div>
              </section>
            </div>
            <div v-if="currentPage == 'lk'">
              <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-3">
              </div>
              <section class="panel panel-default">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h1 class="h3">{{$t('lk.Personal_account')}}</h1>
                  </div>
                  <div class="row">
                    <div class="col-lg-12 col-lg-offset-2">
                      <div layout="column" class="md-inline-form inputdemoBasicUsage layout-column">
                        <h3 class="thin">{{inMailLk}}</h3>
                        <p>{{$t('lk.In_this_section_you_can_manage_your_personal_data')}}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <section class="panel panel-default">
                <div class="panel-body">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h1 class="h3">{{$t('lk.Data_editing')}}</h1>
                  </div>
                  <form name="formLk">
                    <div class="form-group">
                      <label for="exampleInputEmail1">{{$t('lk.Email_address')}}</label>
                      <input type="email" v-model="inMailLk" class="form-control" aria-describedby="emailHelp"
                        placeholder="example@example.com">
                      <small class="form-text text-muted">{{$t('lk.Email_after')}}</small>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">{{$t('lk.New_password')}}</label>
                      <input type="password" v-model="inNewPasswordLk" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">{{$t('lk.Repeat_password')}}</label>
                      <input type="password" v-model="inRepeatPasswordLk" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group">
                      {{errors}}
                    </div>
                    <button type="button" @click="submitLk()" class="btn btn-primary">{{$t('lk.Submit')}}</button>
                  </form>
                </div>
              </section>
            </div>
          </main>

        </div>
      </div>
    </div>
    <footer class="footer">
      <div class="container-fluid">
        <div class="offset-2">
          <span class="text-muted">{{ $t("lk.Web_version_created_by") }} <a target="_blank"
              href="https://seosp.ru/">{{ $t("lk.seosp_ru") }}</a></span>
        </div>
      </div>
    </footer>

    <script type="text/x-template" id="modal-template">
      <transition name="modal">
        <div class="modal-mask" id="modal">
          <div class="modal-wrapper">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <slot name="header">
                    default header
                  </slot>
                </div>
                <div class="tab-content">
                  <slot name="body">
                  </slot>
              </div>
              </div>
            </div>
          </div>
        </div>
      </transition>

    </script>

    <!-- app -->
    <!-- use the modal component, pass in the prop -->
    <modal v-if="confirmModal" @close="confirmModal = false">
      <h3 slot="header">{{ $t("lk.Confirmation") }}</h3>
      <template slot="body">
        <div class="modal-body">
          {{ $t("lk.Delete_selected") }}?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" v-on:click="deleteYes()">{{ $t("lk.Yes") }}</button>
          <button type="button" class="btn btn-secondary" v-on:click="confirmModal = false">{{ $t("lk.No") }}</button>
        </div>
      </template>
    </modal>
    <modal v-if="loadModal" @close="loadModal = false">
      <h3 slot="header">{{ $t("lk.Information") }}</h3>
      <template slot="body">
        <div class="modal-body">
          <div v-html="errors"></div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary"
            v-on:click="clearPage()">{{ $t("lk.Close_the_window") }}</button>
        </div>
      </template>
    </modal>
  </div>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/vue-i18n/dist/vue-i18n.js"></script>
  <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
  <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
  <script src="https://unpkg.com/vue-chartjs/dist/vue-chartjs.min.js"></script>
  <script src="/vue/public/assets/js/script.js"></script>
  <script>
    let ctx = document.getElementById('myChart');
    let myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        datasets: [{
          label: 'Суммарное потребление воды',
          data: [12, 19, 3, 5, 2, 3, 8],
          backgroundColor: [
            'rgba(0, 98, 204, 0.2)'
          ],
          borderColor: [

            'rgba(0, 98, 204, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
    let ctxd = document.getElementById('circle-demo');
    let circleDemo = new Chart(ctxd, {
      type: 'pie',
      data: {
        labels: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        datasets: [{
          label: 'Суммарное потребление воды',
          data: [12, 19, 3, 5, 2, 3, 8],
          "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)", "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)", "rgba(154, 162, 235, 0.2)"],
          "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)", "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"],
          borderWidth: 1
        }]
      }
    });
  </script>
  <!-- Icons -->
  <script>
    window.onload = function () {
      feather.replace()
    };
  </script>

</body>

</html>