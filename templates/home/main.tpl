<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- stylesheet  -->
  <link rel="stylesheet" href="/templates/home/etc/css/tether.min.css">
  <link rel="stylesheet" href="/templates/home/etc/css/bootstrap.min.css">
  <link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css" />
  <link rel="stylesheet" href="/templates/home/dist/style/app.css">
  <link rel="icon" href="/templates/home/dist/images/fav.png">
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/vue-i18n/dist/vue-i18n.js"></script>

  <title>Service</title>
</head>

<body>

  <div id="homeApp">
    <div id="page" class="page">
      <!-- sart navbar -->
      <div id="navbar1">
        <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
          <div class="container">
            <!-- start toggle button -->
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
              data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
              aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
              data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
              aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <!-- end toggle button -->
            <!-- start brand -->
            <a class="navbar-brand" href="#">
                <span class="green">Ion</span>-PO 
            </a>
            <!-- end brand -->
            <!-- start menue -->
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="#about2">{{ $t("register.About") }}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#price2">{{ $t("register.Pricing") }}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" @click="registerModal = true" href="#">{{ $t("register.Registration") }}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/panel/">{{ $t("register.Panel") }}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" @click="langToggle" href="#">{{otherLang}}</a>
                </li>
              </ul>
            </div>
            <!-- end menue -->
          </div>
        </nav>
      </div>
      <!-- end navbar -->
      <!-- start hero 2 -->
      <div id="hero2">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="hero-text">
                <h1 class="animated fadeInDown">{{ $t("register.Name_service_1") }}</h1>
                <p class="animated fadeInDown">{{ $t("register.Name_service_2") }}</p>
                <button id="show-modal" @click="registerModal = true"
                  class="btn btn-primary btn-sh-primary animated fadeInDown">{{ $t("register.Start_using") }}</button>
                <a href="#about2"
                  class="btn btn-primary btn-sh-primary animated fadeInDown">{{ $t("register.Learn_more") }}</a>
              </div>
              <div class="hero-img">
                <div class="b-img animated fadeInUp"><img src="/templates/home/dist/images/hero-lk.png" alt="hero img2">
                </div>
                <div class="t-img animated fadeInUp"><img src="/templates/home/dist/images/hero-lk.png" alt="hero img">
                </div>
              </div>
            </div>
          </div>
          <div class="row"></div>
        </div>
      </div>
      <!-- end hero 2 -->
      <!-- start baner -->
      <div id="baner2">
        <div class="baner-shadow">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <p v-if="$i18n.locale === 'en'">{{text1_en}}</p>
                <p v-if="$i18n.locale === 'ru'">{{text1_ru}}</p>
              </div>
              <div class="col-lg-4">
                <button @click="registerModal = true" class="btn btn-primary">{{ $t("register.Try") }}</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end baner -->
      <!-- start about section -->
      <div id="about2">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <div class="img">
                <img src="/templates/home/dist/images/graph.jpg" alt="about image">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="text">
                <h1 class="mt-0">Формирование эффективных портфелей и программ</h1>
                <p class="paragraph">
                  Веб-сервис предлагает услуги по формированию экономически эффективных инвестиционных программ. На основе заданного бюджета инвестирования и критериев формирования, система определяет наиболее выгодные наборы (портфель) для инвестирования.
                </p>
                <p class="paragraph">
                  Всё что вам нужно - задать бюджет, ряд критериев в виде файла определенного формата и получить оптимальный набор для инвестирования.
                </p>
                <button @click="moreModal = true" class="btn btn-primary">{{ $t("register.More") }}</button>
              </div>
            </div>
          </div>

        </div>
      </div>
      <!-- END about SECTION -->
      <!-- start price -->
      <div id="price2">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="head">
                <h1>{{ $t("register.Our_price_offer") }}</h1>
                <p>{{ $t("register.price_text") }}</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 offset-lg-3">
              <div class="pricetable">
                <div class="head">
                  <p>{{ $t("register.just") }}</p>
                  <h1>$40</h1>
                  <p>{{ $t("register.every_year") }}</p>
                </div>
                <div class="text d-none">
                  <p>1 GB Photos</p>
                  <p>Secure Online Transfer</p>
                  <p>Unlimited Styles</p>
                  <p>Customer Service</p>
                  <p>Manual Backup</p>
                </div>
                <div class="bottom">
                    <button @click="registerModal = true" class="btn btn-primary">{{ $t("register.Try") }}</button>
                  </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <!-- end price -->
      <!-- start footer -->
      <footer id="footer2">
        <div class="container">
          <div class="row">
            <div class="col-lg-3">
              <div class="navbar-brand">
                  <span class="green">Ion</span>-PO 
                </div>
            </div>
            <div class="col-lg-6">
              <div class="copy">
                <p>&copy; 2019 <a href="http://it-progression.com/">Progression</a></p>
              </div>
            </div>
            <div class="col-lg-3">
              <!--
              <div class="social">
                <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                <a href="#"><i class="zmdi zmdi-google-plus"></i></a>
                <a href="#"><i class="zmdi zmdi-youtube"></i></a>
              </div>-->
            </div>
          </div>
        </div>
      </footer>

      <!-- end footer -->
      <script type="text/x-template" id="modal-template">
      <transition name="modal">
        <div class="modal-mask" id="modal">
          <div class="modal-wrapper">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <slot name="header">
                    default header
                  </slot>
                </div>
                <div class="tab-content">
                  <slot name="body">
                  </slot>
              </div>
              </div>
            </div>
          </div>
        </div>
      </transition>

    </script>

      <!-- app -->
      <modal class="modal-more" v-if="moreModal" @close="moreModal = false">
        <div slot="header">
            
                <h5 class="modal-title">{{ $t("register.More") }}</h5>
                <button type="button" class="close" @click="moreModal = false">
                  <span aria-hidden="true">&times;</span>
                </button>
        </div>
       
        <template slot="body">
          <div class="modal-body">
            <div>
              <p>
                  <b>Задачи формирования Портфелей и Программ</b> возникают практически во все сферах человеческой деятельности всякий раз, когда надо принять решение выбора из исходного Пула потенциальных объектов инвестирования некоторого предпочтительного набора этих объектов инвестирования, который обеспечит достижение желаемых целей. 
                </p>
                <p><b>Под объектом инвестирования</b> понимается любой материальный или нематериальный объект вложения средств инвестора, способствующий достижению его целей. При этом принято называть набор однородных объектов инвестирования – Программой, а набор разнородных объектов инвестирования – Портфелем. </p>
                <p>
                  <strong>
                      Примеры Портфелей и Программ:
                  </strong>
                </p>
                <ul class="pli">
                  <li>Портфели активов</li>
                  <li>Портфели и Программы инвестиционных проектов</li>
                  <li>Портфели кредитных продуктов финансово-кредитных учреждений</li>
                  <li>Портфели страховых продуктов страховых компаний</li>
                  <li>Портфели ценных бумаг инвестиционных компаний</li>
                  <li>Портфели заказов производственных компаний</li>
                  <li>Программы производства продукции</li>
                  <li>Программы геолого-технических мероприятий нефтегазовых компаний</li>
                  <li>Портфели и программы социальных проектов органов государственного и муниципального управления</li>
                </ul>
                <p></p>
                <p><strong>Эффективным Портфелем/Программой</strong> является такой набор объектов инвестирования выбранных из рассматриваемого Пула потенциальных объектов инвестирования, который для заданного уровня инвестиций обеспечивает максимально возможное достижение целей инвестора. </p>
                <p>Класс таких задач сводится к решению классческой задачи о ранце (рюкзаке).</p>
                <p>В силу того, что из рассматриваемого Пула, включающего N потенциальных объектов инвестирования можно сформировать 2N-1 Портфелей/Программ, то поиск эффективных Портфелей и Программ при увеличении N&gt;40 является довольно трудоемкой задачей при решении задачи необходимо выбирать между точными алгоритмами, которые неприменимы для «больших» рюкзаков, и приближенными, которые работают быстро, но не гарантируют нахождение эффективных(оптимальных) решений. </p>
                <p>Если будет сформирован не эффективный Портфель/Программа, то инвестор в общем случае понесет прямые финансовые потери: либо в форме перерасхода инвестиций, либо  в форме недополученного дохода, либо в форме их комбинации.</p>
                <img style="margin: 10px auto" src="http://it-progression.com/images/n/p4.png" alt=" " class="img-responsive">
                <p>Мы предлагаем сервис для решения задач с количеством кандидатов до полутора тысяч и более. Причем мы осуществляем поиск множества решений на всем диапазоне инвестиций (или другого управляемого параметра)</p>
                <p>
                </p>
                <h4>Почему лучше искать и рассматривать множество эффективных решений, а не одно решение? </h4>
                <p></p>
                <p>
                    Имея только одно решение, лицо, принимающее решение (инвестор) не видит всей «картины» и для него является полной неопределенностью все, что находится за пределами этой «точки». В то же время множество эффективных решений содержит информацию о потенциально достижимых значениях всех показателей Портфелей и Программ на всем диапазоне их инвестирования, что позволяет инвестору увидеть всю «картину» в целом.  
                </p>
                <img style="margin: 10px auto" width="60%" src="http://it-progression.com/images/n/p2.png" alt=" " class="img-responsive">
                <p>
                  <b>Анализ эффективного множества решений снижает неопределенность и повышает информированность</b> лица принимающее решение по формированию Портфелей и Программ, <b>что обеспечивает его поддержку в принятии наилучшего решения</b>.
                </p>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary"
              @click="moreModal = false">{{ $t("register.Close") }}</button>
          </div>
        </template>
      </modal>
      <!-- use the modal component, pass in the prop -->
      <modal v-if="registerModal" @close="registerModal = false">
        <h3 slot="header">{{ $t("register.Registration") }}</h3>
        <template slot="body">
          <div class="modal-body">
            <form novalidate="true" @submit="regCh" class="form-signin">
              <div v-show="currentTab == 'registerOne'">
                <div class="form-group">
                  <small class="form-text text-muted">{{ $t("register.All_fields_must_be_filled") }}</small>
                </div>
                <div class="form-group">

                  <input type="email" class="form-control" v-model="regMail"
                    :placeholder="$t('register.Email_address') ">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" v-model="regPassword"
                    :placeholder="$t('register.Password')">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" v-model="regRepeatPassword"
                    :placeholder="$t('register.Repeat_password')">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" v-model="regName" :placeholder="$t('register.Name')">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" v-model="regCountry"
                    :placeholder="$t('register.Your_country')">
                </div>

              </div>
              <div v-show="currentTab == 'registerTwo'">
                <div class="form-group">
                  <input type="text" class="form-control" v-model="regIndustry" :placeholder="$t('register.Industry')">
                  <small class="form-text text-muted ml">{{ $t("register.This_field_is_required") }}</small>
                </div>
                <div class="form-group">
                  <textarea v-model="regClassTasks" class="form-control"
                    :placeholder="$t('register.Class_of_tasks')"></textarea>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" v-model="regPhone" :placeholder="$t('register.Phone')">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" v-model="regCompany" :placeholder="$t('register.Company')">
                </div>
              </div>
              <div class="form-group">
                {{errors}}
              </div>
            </form>

          </div>
          <div class="modal-footer">
            <button type="button" v-if="currentTab == 'registerOne'" class="btn btn-secondary"
              @click="registerModal = false">{{ $t("register.Cancel") }}</button>
            <button type="button" v-if="currentTab == 'registerTwo'" class="btn btn-secondary"
              @click="currentTab = 'registerOne'">{{ $t("register.Prev") }}</button>
            <button type="button" v-if="currentTab == 'registerOne'" role="presentation" @click="regOne()"
              class="btn btn-primary">{{ $t("register.Next") }}</button>
            <a v-if="currentTab == 'registerTwo'" class="btn btn-primary" href="#" v-on:click="regCh()">Регистрация</a>

          </div>
        </template>
      </modal>
    </div>
  </div>
  <!-- script -->
  <!--<script src="etc/js/jquery.min.js"></script>-->
  <!--<script src="etc/js/tether.min.js"></script>-->
  <!--<script src="etc/js/bootstrap.min.js"></script>-->

  <script src="//unpkg.com/babel-polyfill@latest/dist/polyfill.min.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script>
    Vue.component('modal', {
      template: '#modal-template'
    })

    // start app

    const messages = {
      en: {
        register: {
          Email_address: 'Email_address',
          Registration: 'Registration',
          Cancel: 'Cancel',
          Learn_more: 'Learn more',
          Start_using: 'Start using',
          Try: 'Try',
          Name: 'Name',
          Your_country: 'Your country',
          Industry: 'Industry',
          Password: 'Password',
          Repeat_password: 'Repeat password',
          Next: 'Next step',
          Prev: 'Previous step',
          All_fields_must_be_filled: 'All fields must be filled!',
          Class_of_tasks: 'Class of tasks',
          Phone: 'Phone',
          Company: 'Company',
          Please_enter_valid_email: 'Please enter valid email',
          This_field_is_required: 'This field is required',
          Please_enter_your_name: 'Please enter your name',
          Please_enter_email: 'Please enter valid Email',
          Please_enter_password: 'Пожалуйста, укажите пароль',
          Passwords_do_not_match: 'Passwords_do_not_match',
          Please_enter_country: 'Please enter country',
          Please_enter_industry: 'Please enter industry',
          Pricing: 'Pricing',
          Home: 'Home',
          About: 'About',
          Login: 'Login',
          More: 'More',
          Close: 'Close',
          just: 'just',
          Panel: 'Panel',
          Registration: 'Registration',
          price_text: 'Our price offer can not fail to please you',
          every_year: 'every year',
          Please_wait: 'Please wait',
          Our_price_offer: 'Our price offer',
          Such_user_is_already_registered: 'Such user (e-mail) is already registered',
          Registration_was_successful: 'Registration was successful. A confirmation email has been sent to your email address.',
          Name_service_1: 'SERVICE SUPPORT INVESTMENT DECISIONS',
          Name_service_2: 'formation of effective Portfolios and Programs'
        }
      },
      ru: {
        register: {
          Email_address: 'Адрес email',
          Registration: 'Регистрация',
          Registration2: 'Регистрация5',
          Cancel: 'Отмена',
          Learn_more: 'Узнать больше',
          Start_using: 'Начать пользоваться',
          Try: 'Попробовать',
          Name: 'Имя',
          Your_country: 'Ваша страна',
          Industry: 'Отрасль',
          Password: 'Пароль',
          Repeat_password: 'Пароль ещё раз',
          Next: 'Далее',
          Prev: 'Назад',
          All_fields_must_be_filled: 'Все поля должны быть обязательно заполнены!',
          Class_of_tasks: 'Описание  класса/классов задач которые интересны для решения',
          Phone: 'Телефон',
          Company: 'Компания',
          This_field_is_required: 'Это поле обязательно к заполнению',
          Please_enter_valid_email: 'Пожалуйста, укажите коректный Email',
          Please_enter_your_name: 'Пожалуйста, укажите имя',
          Please_enter_password: 'Пожалуйста, укажите пароль',
          Passwords_do_not_match: 'Пароли не совпадают',
          Please_enter_country: 'Пожалуйста, укажите страну',
          Please_enter_industry: 'Пожалуйста, укажите отрасль',
          Pricing: 'Цены',
          Home: 'Главная',
          About: 'О сервсие',
          Login: 'Войти',
          More: 'Подробнее',
          Close: 'Закрыть',
          every_year: 'В год',
          just: 'Всего',
          Panel: 'Личный кабинет',
          Registration: 'Регистрация',
          Please_wait: 'Пожалуйста подождите',
          price_text: 'Наше ценовое предложение не может не порадовать Вас',
          Our_price_offer: 'Наше ценовое предложение',
          Such_user_is_already_registered: 'Такой пользователь (e-mail) уже зарегистрирован',
          Registration_was_successful: 'Регистрация прошла успешно. На почту выслано письмо с подтверждением регистрации.',
          Name_service_2: 'по формированию эффективных Портфелей и Программ',
          Name_service_1: 'Сервис поддержки принятия инвестиционных решений'
        }
      }
    }

    // Create VueI18n instance with options
    const i18n = new VueI18n({
      locale: 'en', // set locale
      fallbackLocale: 'en',
      messages, // set locale messages
    })


    // Create a Vue instance with `i18n` option
    var homeApp = new Vue({
      el: '#homeApp',
      data: {
        registerModal: false,
        moreModal: false,
        currentTab: 'registerOne',
        message: 'Hi',
        regMail: '',
        regPassword: '',
        regRepeatPassword: '',
        regName: '',
        regCountry: '',
        regPhone: '',
        regCompany: '',
        regIndustry: '',
        regClassTasks: '',
        errors: '',
        otherLang: 'Русский',
      },
      created: function () {
        var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage).substr(0, 2).toLowerCase();
        if (lang === 'ru') {
          this.$i18n.locale = 'ru';
          this.otherLang = 'English'
        }
      },
      methods: {
        langToggle() {
          if (this.$i18n.locale === 'en') {
            this.$i18n.locale = 'ru';
            this.otherLang = 'English'
          }
          else {
            this.$i18n.locale = 'en';
            this.otherLang = 'Русский'
          }
        },
        regOne: function () {
          if (!this.regMail) {
            this.errors = this.$t("register.Please_enter_valid_email");
            return;
          } else if (!this.validEmail(this.regMail)) {
            this.errors = this.$t("register.Please_enter_valid_email");
            return;
          }
          if (!this.regPassword) {
            this.errors = this.$t("register.Please_enter_password");
            return;
          }
          if (!this.regRepeatPassword) {
            this.errors = this.$t("register.Passwords_do_not_match");
            return;
          } else if (this.regRepeatPassword != this.regPassword) {
            this.errors = this.$t("register.Passwords_do_not_match");
            return;
          }
          if (!this.regName) {
            this.errors = this.$t("register.Please_enter_your_name");
            return;
          }
          if (!this.regCountry) {
            this.errors = this.$t("register.Please_enter_country");
            return;
          }
          this.errors = '';
          this.currentTab = 'registerTwo';
        },
        validEmail: function (email) {
          var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
        },
        regCh: function () {
          if (!this.regIndustry) {
            this.errors = this.$t("register.Please_enter_industry");
            return;
          }
            this.errors = this.$t("register.Please_wait");
          var formData = new FormData();
          formData.append('regMail', this.regMail);
          formData.append('regName', this.regName);
          formData.append('regCountry', this.regCountry);
          formData.append('regPhone', this.regPhone);
          formData.append('regCompany', this.regCompany);
          formData.append('regIndustry', this.regIndustry);
          formData.append('regPassword', this.regPassword);
          formData.append('regRepeatPassword', this.regRepeatPassword);
          formData.append('regClassTasks', this.regClassTasks);
          formData.append('postJson', 'yes');
          formData.append('lang', this.$i18n.locale);
          var xhr = new XMLHttpRequest();
          xhr.open("POST", "/sendregister");
          xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
              if (xhr.status >= 200 && xhr.status < 400) {
                //homeApp.errors = homeApp.$t("register.Registration_was_successful");
                homeApp.errors = xhr.responseText;
                switch (xhr.responseText) {
                  case 'validMail':
                    homeApp.errors = homeApp.$t("register.Please_enter_valid_email");
                    break;
                  case 'validName':
                    homeApp.errors = homeApp.$t("register.Please_enter_your_name");
                    break;
                  case 'hasMail':
                    homeApp.errors = homeApp.$t("register.Such_user_is_already_registered");
                    break;
                  case 'notPas':
                    homeApp.errors = homeApp.$t("register.Please_enter_password");
                    break;
                  case 'notRepeatPas':
                    homeApp.errors = homeApp.$t("register.Passwords_do_not_match");
                    break;
                  case 'notPasMatch':
                    homeApp.errors = homeApp.$t("register.Passwords_do_not_match");
                    break;
                  case 'successful': 
                  
                    homeApp.errors = homeApp.$t("register.Registration_was_successful");
                    setTimeout(function(){homeApp.registerModal = false;}, 2000); 
               
                   break;
                  default:
                }
                homeApp.submitVis = false;
                homeApp.repeatVis = true;
              } else {
                alert('SubmitFeedback error 1!');
                return;
              }
            }
          };
          xhr.send(formData);
          // make ajax request and pass the data. I'm not certain how to do it with axios but something along the lines of this
          /*axios.post('/my-url/', {
            regMail: this.regMail,
            regName: this.regName,
            regCountry: this.regCountry,
            regPhone: this.regPhone,
            regCompany: this.regCompany,
            regIndustry: this.regIndustry,
            regClassTasks: this.regClassTasks,
            regPassword: this.regPassword
          })
          .catch(error => {
      console.log(error);
      this.errored = true;
    });*/

        }
      },
      i18n
    })
  </script>
  <!-- <script src="etc/js/jquery.nicescroll.min.js"></script> -->
  <!--<script src="/templates/home/dist/js/app.js"></script>-->

</body>

</html>