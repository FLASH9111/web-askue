<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title></title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<style type="text/css">
body {
	line-height: 1;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}

body {
	font: 12px/18px Arial, sans-serif;
	background-color: #BEBEBE;
}
.wrapper {
    width: 700px;

margin:   auto;
}

.content {
padding: 50px;
margin: 250px auto;
background-color: #FFFFFF;
display: block;

}
.copy {
float: right;
color: #464451;
margin-right: -10px;
margin-top: 10px;
}
a {
text-decoration: none;
}
</style>
</head>

<body>

<div class="wrapper">
	<div class="content">
	<h2>404... Итак, вы попали не туда, куда следует...</h2>
	<h3>Почему вы сюда попали? Возможные причины этого:</h3>
		<ul type="square">
			<h4><li>Страница удалена. </li></h4>
			<h4><li>Вы получили неверную ссылку.</li></h4>
			<h4><li>Вы ошиблись в вводе адреса страницы нашего интернет-ресурса.</li></h4>
		</ul>
	<h3>Если желаете, Вы можете пройти по одной из данных ссылок:</h3>
	<p><a href="/" title="Главная страница">Главная страница</a></p>
	<p><a href="javascript:history.go(-1)" title="Назад">Туда, откуда вы сюда попали</a></p>
<div class="copy"><a href="https://xn--80aakebzkk3bgn.xn--p1ai/"><span>СДЕЛАЕМСАЙТ.РФ</span></a></div>	
	</div><!-- .content-->

</div><!-- .wrapper -->

</body>
</html>